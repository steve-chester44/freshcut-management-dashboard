export const ACCESS_LEVELS = {
	free: -1,
	low: 1,
	medium: 2,
	high: 3,
	admin: 99
}

export const getAccessLevel = level => ACCESS_LEVELS[level]

export default company => {
	if (!company || !company.subscription || !company.subscription.plan) return -1

	return getAccessLevel(company.subscription.plan.accessLevel)
}
