import gql from "graphql-tag"

export const companyLocationsQuery = gql`
	query {
		activeLocation @client {
			id
			name
		}
		
		locations {
			id
			name
		}
	}
`
