import gql from 'graphql-tag'

export const userInfoQuery = gql`
	query User($skip: Boolean!) {
		me @skip(if: $skip) {
			id
			firstName
			lastName
			email
			permissionLevel
			createdAt
			company {
				id
				features
				maxStaff
				subscription {
					end
					plan {
						# Should only be used inconjunction with getAccessLevel to know whether the company is downgrading or upgrading
						accessLevel
						name
					}
				}
				locations {
					id
					uuid
					name
				}
			}
		}
	}
`
