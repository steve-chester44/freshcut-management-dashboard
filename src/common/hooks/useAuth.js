import gql from 'graphql-tag'
import { useMutation, useQuery } from '@apollo/react-hooks'
import useRouter from './useRouter'
import { userInfoQuery } from '../../common/queries/User'
import { getCookieValue } from '../../graphql/utils'

const registerMutation = gql`
	mutation($email: String!, $password: String!, $firstName: String!, $lastName: String, $companyName: String) {
		register(email: $email, password: $password, firstName: $firstName, lastName: $lastName, companyName: $companyName)
	}
`

const loginMutation = gql`
	mutation($email: String!, $password: String!) {
		login(email: $email, password: $password) {
			token
		}
	}
`

const logoutMutation = gql`
	mutation {
		logout
	}
`

export const useUser = ({ canSkip } = {}) => {
	const { loading, data: { me } = {} } = useQuery(userInfoQuery, {
		variables: { skip: !canSkip ? false : !getCookieValue('access-token') && !getCookieValue('refresh-token') }
	})

	return { loading, user: me }
}

const useAuth = () => {
	const [login, { loading: loginLoading }] = useMutation(loginMutation)
	const [register, { loading: registerLoading }] = useMutation(registerMutation)
	const [logout, { loading: logoutLoading }] = useMutation(logoutMutation)
	const { history } = useRouter()

	const loading = loginLoading || registerLoading || logoutLoading

	const handleLogin = async variables => {
		const { data } = await login({ variables })

		if (data.login) {
			window.location.href = '/'
			return
		}
	}

	const handleRegister = async variables => {
		const { data } = await register({ variables })

		if (data.register) {
			return handleLogin({ email: variables.email, password: variables.password })
		}
	}

	const handleLogout = async () => {
		await logout({
			update: cache => {
				cache.writeQuery({
					query: userInfoQuery,
					variables: { skip: false },
					data: { me: null }
				})
			}
		})

		history.push('/')
	}

	return { register: handleRegister, login: handleLogin, logout: handleLogout, loading }
}

export default useAuth
