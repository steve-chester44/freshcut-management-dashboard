import React from "react"

import { Alert } from "antd"

const VerifyEmailNotification = () => {
	const lastViewed = localStorage.getItem("VerifyEmailNotification")

	const handleClose = () => {
		localStorage.setItem("VerifyEmailNotification", +new Date())
	}

	let show = true

	if (lastViewed) {
		const now = +new Date()

		if (parseInt(now, 10) - parseInt(lastViewed, 10) < 259200) {
			show = false
		}
	}

	return !show ? null : (
		<Alert
			type="warning"
			message="Account Not Verified"
			description="You haven't verified your email yet. Check your email or click here to resend a confirmation code."
			banner
			closable
			onClose={handleClose}
		/>
	)
}

export default VerifyEmailNotification
