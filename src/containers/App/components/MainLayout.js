import React from 'react'
import { Redirect } from 'react-router-dom'

import { Layout } from 'antd'

import MainMenu from './MainMenu'
import { AUTH_LOGIN } from '../../../modules/authentication/utils/urls'

const useWindowDimensions = () => {
	const [state, setState] = React.useState({ width: window.innerWidth, height: window.innerHeight })

	React.useEffect(() => {
		var resizeTimeout

		const listener = () => {
			window.clearTimeout(resizeTimeout)

			resizeTimeout = window.setTimeout(() => {
				setState({
					width: window.innerWidth,
					height: window.innerHeight
				})
			}, 250)
		}
		window.addEventListener('resize', listener)

		return () => window.removeEventListener('resize', listener)
	}, [])

	return state
}

const MainLayout = ({ user, children, ...props }) => {
	const { width } = useWindowDimensions()
	if (!user) {
		return <Redirect to={AUTH_LOGIN} />
	}

	const collapsed = width < 900

	return (
		<Layout>
			<Layout.Sider collapsed={collapsed} width="250" theme="dark">
				<MainMenu collapsed={collapsed} user={user} pathname={props.location.pathname} />
			</Layout.Sider>
			<Layout.Content>{children}</Layout.Content>
		</Layout>
	)
}

export default MainLayout
