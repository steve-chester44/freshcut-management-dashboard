import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Menu, Icon, Avatar } from 'antd'

import useAuth from '../../../../common/hooks/useAuth'

import { CALENDAR_ROOT } from '../../../../modules/calendar/utils/urls'
import { SERVICES_ROOT } from '../../../../modules/services/utils/urls'
import {
	EMPLOYEES_ROOT,
	EMPLOYEES_SCHEDULING_ROUTE,
	LOCATIONS_SCHEDULING_ROUTE
} from '../../../../modules/employees/utils/urls'
import { CUSTOMERS_ROOT } from '../../../../modules/customers/utils/urls'

const Container = styled('div')`
	position: relative;
	padding-top: 24px;
	padding-bottom: 24px;
	display: flex;
	flex-direction: column;
	height: 100%;
	justify-content: space-between;

	.ant-menu-submenu-title > a {
		color: inherit;
	}

	h1.title {
		color: #fff;
		font-family: jaf-domus;
		font-weight: 700;
		font-size: 28px;
		line-height: 1;
		padding: 0;
		margin: 0;
		text-align: center;
		margin-bottom: 24px;
		text-shadow: 2px 2px 0px rgba(32, 32, 32, 0.5);
		text-transform: uppercase;
	}
`

const Header = ({ pathname, collapsed, user }) => {
	const { logout } = useAuth()

	return (
		<Container>
			<div>
				<Link to="/" title={process.env.REACT_APP_COMPANY_NAME}>
					<h1 className="title">{collapsed ? 'N' : process.env.REACT_APP_COMPANY_NAME}</h1>
				</Link>

				<Menu
					mode="inline"
					theme="dark"
					style={{ borderRight: 0 }}
					defaultSelectedKeys={[pathname]}
					selectedKeys={[pathname]}
				>
					<Menu.Item key={CALENDAR_ROOT}>
						<Link to={CALENDAR_ROOT}>
							<Icon type="calendar" />
							<span>Calendar</span>
						</Link>
					</Menu.Item>

					<Menu.Item key={SERVICES_ROOT}>
						<Link to={SERVICES_ROOT}>
							<Icon type="shopping-cart" />
							<span>Services</span>
						</Link>
					</Menu.Item>

					<Menu.SubMenu
						key={EMPLOYEES_ROOT}
						title={
							<Link to={EMPLOYEES_ROOT}>
								<Icon type="solution" />
								<span>Staff</span>
							</Link>
						}
					>
						<Menu.Item key={EMPLOYEES_ROOT}>
							<Link to={EMPLOYEES_ROOT}>
								<Icon type="solution" />
								Staff Members
							</Link>
						</Menu.Item>
						<Menu.Item key={EMPLOYEES_SCHEDULING_ROUTE}>
							<Link to={EMPLOYEES_SCHEDULING_ROUTE}>
								<Icon type="schedule" />
								<span>Staff Schedules</span>
							</Link>
						</Menu.Item>
						<Menu.Item key={LOCATIONS_SCHEDULING_ROUTE}>
							<Link to={LOCATIONS_SCHEDULING_ROUTE}>
								<Icon type="calendar" />
								<span>Closed Dates</span>
							</Link>
						</Menu.Item>
					</Menu.SubMenu>

					<Menu.Item key={CUSTOMERS_ROOT}>
						<Link to={CUSTOMERS_ROOT}>
							<Icon type="team" />
							<span>Clients</span>
						</Link>
					</Menu.Item>
				</Menu>
			</div>

			<div>
				<Menu theme="dark" style={{ borderRight: 0 }}>
					<Menu.Item>
						<Link to="/settings">
							<Icon type="setting" />
							<span>Settings</span>
						</Link>
					</Menu.Item>
					<Menu.SubMenu
						theme="light"
						title={
							<div>
								<Avatar size="small" style={{ marginRight: 7 }}>
									{user.firstName.charAt(0)}
								</Avatar>
								{!collapsed && (
									<span>
										{user.firstName} {user.lastName && user.lastName}
									</span>
								)}
							</div>
						}
					>
						<Menu.Item>
							<Link to="/settings/account" style={{ paddingRight: 50 }}>
								<Icon type="idcard" />
								<span> Account</span>
							</Link>
						</Menu.Item>
						<Menu.Divider />

						{user.permissionLevel === 'owner' && (
							<Menu.Item>
								<Link to="/settings/billing">
									<Icon type="wallet" />
									<span> Billing</span>
								</Link>
							</Menu.Item>
						)}

						{user.permissionLevel === 'owner' && <Menu.Divider />}
						<Menu.Item key="logout">
							<div onClick={logout}>
								<Icon type="logout" /> Logout
							</div>
						</Menu.Item>
					</Menu.SubMenu>
				</Menu>
			</div>
		</Container>
	)
}

export default Header
