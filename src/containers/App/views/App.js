import React, { lazy } from 'react'
import queryString from 'query-string'
import { Switch, Route, Redirect } from 'react-router-dom'

import { useRouter, useUser } from '../../../common/hooks'

import MainLayout from '../components/MainLayout'
import GuestRoute from '../../../components/routes/GuestRoute'

import {
	AUTH_LOGIN,
	AUTH_REGISTER,
	AUTH_CREATE_PASSWORD,
	AUTH_RESET_PASSWORD
} from '../../../modules/authentication/utils/urls'
import {
	EMPLOYEES_ROOT,
	EMPLOYEES_SCHEDULING_ROUTE,
	LOCATIONS_SCHEDULING_ROUTE
} from '../../../modules/employees/utils/urls'
import { CALENDAR_ROOT } from '../../../modules/calendar/utils/urls'
import { SERVICES_ROOT } from '../../../modules/services/utils/urls'

import LoadingPage from '../../../components/routes/LoadingPage'
import { CUSTOMERS_ROOT, EDIT_CUSTOMER_ROUTE } from '../../../modules/customers/utils/urls'

const CreatePassword = lazy(() => import('../../../modules/authentication/views/CreatePassword'))

const Employees = lazy(() => import('../../../modules/employees/views/EmployeesRoot'))
const EmployeeScheduling = lazy(() => import('../../../modules/employees/views/EmployeeScheduling'))
const LocationsScheduling = lazy(() => import('../../../modules/employees/views/LocationsScheduling'))

const Services = lazy(() => import('../../../modules/services/views/ServicesRoot'))
const Settings = lazy(() => import('../../../modules/settings/views/RootContainer'))

const Calendar = lazy(() => import('../../../modules/calendar/views/CalendarRoot'))

const Register = lazy(() => import('../../../modules/authentication/views/Register'))
const Login = lazy(() => import('../../../modules/authentication/views/Login'))
const ResetPassword = lazy(() => import('../../../modules/authentication/views/ResetPassword'))

const CustomerModal = lazy(() => import('../../../modules/customers/views/CustomerModal'))
const CustomerList = lazy(() => import('../../../modules/customers/views/ListContainer'))

const App = () => {
	const { history, location } = useRouter()
	const { loading, user } = useUser({ canSkip: true })

	if (loading) return <LoadingPage />

	return (
		<Switch>
			<GuestRoute user={user} path={AUTH_REGISTER} component={Register} />
			<GuestRoute user={user} path={AUTH_LOGIN} component={Login} />
			<GuestRoute
				user={user}
				path={AUTH_RESET_PASSWORD}
				render={props => {
					const { token } = queryString.parse(props.location.search)

					if (!token) return <Redirect to={AUTH_LOGIN} />

					return <ResetPassword history={props.history} token={token} />
				}}
			/>

			<GuestRoute
				user={user}
				path={AUTH_CREATE_PASSWORD}
				render={props => {
					const { token } = queryString.parse(props.location.search)

					if (!token) return <Redirect to={AUTH_LOGIN} />

					return <CreatePassword history={props.history} token={token} />
				}}
			/>

			<MainLayout location={location} history={history} user={user}>
				<React.Suspense fallback={<LoadingPage />}>
					<Switch>
						<Route path={CALENDAR_ROOT} render={props => <Calendar locations={user.company.locations} {...props} />} />
						<Route path={SERVICES_ROOT} render={props => <Services {...props} />} />

						<Route
							path={EMPLOYEES_SCHEDULING_ROUTE}
							render={props => <EmployeeScheduling locations={user.company.locations} {...props} />}
						/>

						<Route
							path={LOCATIONS_SCHEDULING_ROUTE}
							render={props => <LocationsScheduling locations={user.company.locations} {...props} />}
						/>

						<Route
							path={EMPLOYEES_ROOT}
							render={props => <Employees {...props} totalAccountsAllowed={user.company.maxStaff} />}
						/>

						<Route
							path={EDIT_CUSTOMER_ROUTE}
							render={props => <CustomerModal customerId={props.match.params.id} {...props} />}
						/>

						<Route path={CUSTOMERS_ROOT} render={props => <CustomerList {...props} />} />
						<Route path="/settings" render={props => <Settings {...props} user={user} />} />
						<Redirect to="/calendar" />
					</Switch>
				</React.Suspense>
			</MainLayout>
		</Switch>
	)
}

export default App
