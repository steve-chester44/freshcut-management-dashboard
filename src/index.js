import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'

import { ApolloProvider } from 'react-apollo'
import { ApolloProvider as ApolloHooksProvider } from '@apollo/react-hooks'

import client from './graphql/createClient'
import App from './containers/App/views/App'
import './index.css'

import 'react-big-calendar/lib/css/react-big-calendar.css'
import './modules/calendar/style-overrides.css'

ReactDOM.render(
	<ApolloProvider client={client}>
		<ApolloHooksProvider client={client}>
			<Router>
				<App />
			</Router>
		</ApolloHooksProvider>
	</ApolloProvider>,
	document.getElementById('root')
)
