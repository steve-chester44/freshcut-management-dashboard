import React from "react"
import styled, { keyframes } from "styled-components"

const rotate360 = keyframes`
  from {
	opacity: 0;
	transform: translate(20px);
  }

  to {
	opacity: 1;
	transform: translate(0px);
	
  }
`
const Wrapper = styled("div")`
	display: flex;
	justify-content: flex-end;
	align-items: center;
    cursor: pointer;
`
const Tooltip = styled("div")`
	margin-right: 10px;
	border-radius: 5px;
	background: rgba(32, 32, 32, 1);
	color: white;
	padding: 7px 13px;
	display: block;
	text-align: right;
    font-weight: 600;
	font-size: 12px;
	opacity: 0;

	animation: ${rotate360} .2s linear forwards;
`

const MenuButton = styled("div")`
	background: rgba(103, 120, 208, 1.0);
	font-size: 32px;
	color: white;
	border-radius: 50%;
	display: flex;
	align-items: center;
	justify-content: center;
	width: 60px;
	height: 60px;
	margin-bottom: 10px;
	box-shadow: 0px 2px 5px rgba(32, 32, 32, 0.1);

	&:hover {
		transition: transform 0.5s ease;
		transform: rotate(360deg);
	}
`

const FloatingMenuButton = ({ children, tooltip, ...props }) => {
	return (
        <Wrapper { ...props}>
			{tooltip && <Tooltip>{tooltip}</Tooltip>}
			<MenuButton>{children}</MenuButton>
		</Wrapper>
	)
}

export default FloatingMenuButton
