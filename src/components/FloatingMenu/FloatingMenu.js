import React, { PureComponent } from "react"
import { Query } from "react-apollo"

import FloatingMenuButton from "./FloatingMenuButton"
import styled from "styled-components"
import { insertModeQuery } from "../../modules/calendar/queries"

const Wrapper = styled("div")`
	position: fixed;
	bottom: 25px;
	right: 25px;
	z-index: 20;
`

class FloatingMenu extends PureComponent {
	state = { isOpen: false }

	onMouseEnter = () => {
		this.setState({ isOpen: true })
	}

	onMouseLeave = () => {
		this.setState({ isOpen: false })
	}


	hideActions() {
		this.setState({ isOpen: false })
	}

	renderActions() {
		return this.props.actions.map((action, key) => (
			<FloatingMenuButton
				tooltip={action.tooltip}
				onClick={e => {
					this.onMouseLeave()
					action.onClick(e)
				}}
				key={key}
			>
				{action.content}
			</FloatingMenuButton>
		))
	}

	renderRootAction() {
		return (
			<FloatingMenuButton onClick={this.props.rootAction.onClick} tooltip={this.props.rootAction.tooltip}>
				{this.props.rootAction.content}
			</FloatingMenuButton>
		)
	}

	render() {
		return (
			<Query query={insertModeQuery}>
				{({ loading, data }) => {
					if (loading || data.calendarInsertMode !== "appointment") return null

					return (
						<Wrapper onMouseLeave={this.onMouseLeave} onMouseEnter={this.onMouseEnter}>
							{this.state.isOpen ? this.renderActions() : this.renderRootAction()}
						</Wrapper>
					)
				}}
			</Query>
		)
	}
}

export default FloatingMenu
