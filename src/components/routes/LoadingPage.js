import React from "react"
import { Spin } from "antd"

const loadingstyles = {
	width: "100%",
	height: "100%",
	display: "flex",
	justifyContent: "center",
	alignItems: "center"
}

export default ({ background = "rgba(240, 242, 245, 1.0)" }) => {
	let styles = { ...loadingstyles, background }
	
	return (
		<div style={styles}>
			<Spin size="large" />
		</div>
	)
}
