import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import LoadingPage from './LoadingPage'

const GuestRoute = ({ user, component: Component, render, ...routeProps }) => {
	if (user) {
		return <Redirect to="/" />
	}

	return (
		<Route
			{...routeProps}
			render={props => (
				<React.Suspense fallback={<LoadingPage />}>{render ? render(props) : <Component {...props} />}</React.Suspense>
			)}
		/>
	)
}

export default GuestRoute
