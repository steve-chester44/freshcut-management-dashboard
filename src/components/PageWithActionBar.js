/**
 * TODO: Remove me for something more adapatable
 */
import React from 'react'
import styled from 'styled-components'
import { Layout } from 'antd'

const Container = styled('div')`
	flex: 1;
	height: 100vh;
	overflow-y: auto;
`

const Heading = styled('div')`
	position: sticky;
	top: 0;
	right: 0;
	z-index: 9;
	display: flex;
	align-items: center;
	justify-content: space-between;
	height: 64px;
	background: white;
	padding: 0px 20px;
	box-shadow: 5px 5px 10px rgba(32, 32, 32, 0.02);

	h1,
	h2,
	h3 {
		margin: 0;
	}

	@media (min-width: 768px) {
		.ant-input-search {
			width: 50%;
			max-width: 300px;
		}
	}
`

const Page = ({ leftContent, rightContent, children, padding, extra, ...props }) => {
	return (
		<Container {...props}>
			{(leftContent || rightContent) && (
				<Heading>
					{leftContent || <div />}
					{rightContent || <div />}
				</Heading>
			)}

			{extra && extra}

			<Layout.Content style={{ padding: padding || 20 }}>{children}</Layout.Content>
		</Container>
	)
}

export default Page
