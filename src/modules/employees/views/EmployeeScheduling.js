import React from 'react'
import styled from 'styled-components'

import { addDays, subDays } from 'date-fns'
import { DatePicker as Picker, Button } from 'antd'

import moment from 'moment'
import PageWithActionBar from '../../../components/PageWithActionBar'
import SchedulingTable from '../components/SchedulingTable'

const DatePicker = styled('div')`
	display: flex;

	.picker {
		padding: 0 8px;
	}
`

const today = new Date()

const generateDaysOfWeek = day => {
	return [day, ...Array.from({ length: 6 }).map((_, i) => addDays(day, i + 1))]
}

const EmployeeScheduling = ({ locations }) => {
	// TODO: Use the activeLocation or allow the user to change the location
	const [locationId, setLocation] = React.useState(locations[0].id)

	const [state, setState] = React.useState({
		startOfWeek: today,
		daysOfWeek: generateDaysOfWeek(today)
	})

	const setStartOfWeek = startOfWeek => {
		setState(prev => ({
			...prev,
			startOfWeek,
			daysOfWeek: generateDaysOfWeek(startOfWeek)
		}))
	}

	return (
		<PageWithActionBar
			leftContent={<h1>Staff Scheduling</h1>}
			rightContent={
				<DatePicker>
					<Button icon="left" onClick={() => setStartOfWeek(subDays(state.startOfWeek, 7))} />
					<div className="picker">
						<Picker
							format="MMMM Do, YYYY"
							value={moment(state.startOfWeek)}
							onChange={momentObject => setStartOfWeek(momentObject.toDate())}
						/>
					</div>
					<Button icon="right" onClick={() => setStartOfWeek(addDays(state.startOfWeek, 7))} />
				</DatePicker>
			}
		>
			<SchedulingTable days={state.daysOfWeek} locationId={locationId} />
		</PageWithActionBar>
	)
}

export default EmployeeScheduling
