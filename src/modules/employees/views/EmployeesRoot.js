import React from 'react'

import { Button, Icon, Alert } from 'antd'
import EmployeesTable from '../components/EmployeesTable/EmployeesTable'
import { useQuery } from '@apollo/react-hooks'
import { employeesQuery } from '../queries'

import PageWithActionBar from '../../../components/PageWithActionBar'

const EmployeeModal = React.lazy(() => import('../components/EmployeeModal/EmployeeModal'))

const accountLimitReached = (numberOfThings, limit) => numberOfThings >= limit

const EmployeesRoot = ({ totalAccountsAllowed }) => {
	const { loading, data } = useQuery(employeesQuery)

	const overLimit = accountLimitReached(data?.employees?.length || 1, totalAccountsAllowed)

	return (
		<PageWithActionBar
			leftContent={<h1>Staff Members</h1>}
			extra={
				overLimit && (
					<Alert
						banner
						type="error"
						message="You have reached the total number of  accounts allowed for your current subscription. Upgrade your account to add more staff members."
					/>
				)
			}
			rightContent={
				!loading &&
				data?.employees?.length < totalAccountsAllowed && (
					<EmployeeModal
						trigger={({ open }) => {
							return (
								<Button type="primary" onClick={open} size="large">
									<Icon type="plus" /> Add Staff
								</Button>
							)
						}}
					/>
				)
			}
		>
			<EmployeesTable loading={loading} employees={data?.employees ? data.employees : []} />
		</PageWithActionBar>
	)
}

export default EmployeesRoot
