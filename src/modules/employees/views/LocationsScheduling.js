import React from 'react'
import { Layout } from 'antd'
import { useQuery } from '@apollo/react-hooks'
import styled from 'styled-components'

import ClosedDatesModal from '../components/ClosedDatesModal/ClosedDatesModal'
import ClosedDatesCalendar from '../components/ClosedDatesCalendar'

import { closedDatesQuery } from '../queries'
import startOfYear from 'date-fns/start_of_year'
import endOfYear from 'date-fns/end_of_year'
import format from 'date-fns/format'
import startOfDay from 'date-fns/start_of_day'
import endOfDay from 'date-fns/end_of_day'

const queryVariables = {
	input: {
		where: {
			start_date: format(startOfYear(new Date()), 'YYYY-MM-DD'),
			end_date: format(endOfYear(new Date()), 'YYYY-MM-DD')
		}
	}
}

const Wrapper = styled('div')`
	height: calc(100vh - 78px);
	padding: 20px 20px 0 20px;
`

const mapClosedDatesToCalendarEvent = closedDates => {
	return closedDates.map(event => {
		return {
			...event,
			title: event.description || 'Closed',
			start: startOfDay(event.start_date),
			end: endOfDay(event.end_date),
			allDay: true
		}
	})
}

const defaultClosedDatesValue = {
	start_date: new Date(),
	end_date: new Date(),
	description: '',
	locations: []
}
/**
 *
 * TODO: Don't show the calendar if the user is on mobile.. show the table instead
 */
const LocationsScheduling = ({ locations }) => {
	const {
		data: { closedDates = [] }
	} = useQuery(closedDatesQuery, {
		variables: {
			...queryVariables,
			input: {
				// FIXME: Assumes only 1 location
				locationId: locations[0].id,
				...queryVariables.input
			}
		}
	})

	const [state, setState] = React.useState({
		activeClosedDates: defaultClosedDatesValue,
		modalVisible: false
	})

	const toggleModal = () =>
		setState(prev => ({
			...prev,
			modalVisible: !prev.modalVisible,
			activeClosedDates: prev.modalVisible ? defaultClosedDatesValue : prev.activeClosedDates
		}))

	const datesClosed = React.useMemo(() => mapClosedDatesToCalendarEvent(closedDates), [closedDates])

	const onSelectEvent = activeClosedDates => {
		setState(prev => ({
			...prev,
			activeClosedDates,
			modalVisible: true
		}))
	}

	const onSelect = date => {
		setState(prev => ({
			...prev,
			modalVisible: true,
			activeClosedDates: {
				...prev.activeClosedDates,
				start_date: date.start,
				end_date: date.end
			}
		}))
	}

	return (
		<>
			{state.modalVisible && (
				<ClosedDatesModal
					closedDate={state.activeClosedDates}
					refetchQueryVariables={queryVariables}
					locations={locations}
					onClose={() => toggleModal()}
				/>
			)}

			<Layout.Content className="page-load-fade">
				<Wrapper>
					<ClosedDatesCalendar onSelectEvent={onSelectEvent} onSelect={onSelect} datesClosed={datesClosed} />
				</Wrapper>
			</Layout.Content>
		</>
	)
}

export default LocationsScheduling
