import gql from "graphql-tag"

export const deleteClosedDatesMutation = gql`
	mutation deleteClosedDates($input: DeleteClosedDatesInput!) {
		deleteClosedDates(input: $input)
	}
`

export const upsertClosedDatesMutation = gql`
	mutation upsertClosedDates($input: UpsertClosedDatesInput!) {
		upsertClosedDates(input: $input) {
			closedDates {
				id
				start_date
				end_date
				description
				locations {
					id
					name
				}
			}
		}
	}
`

export const updateScheduleMutation = gql`
	mutation updateSchedule($input: UpdateScheduleInput!) {
		updateSchedule(input: $input)
	}
`

export const deleteScheduleMutation = gql`
	mutation deleteSchedule($input: DeleteScheduleInput!) {
		deleteSchedule(input: $input)
	}
`

export const upsertEmployeeMutation = gql`
	mutation upsertEmployee($input: UpsertEmployeeInput!) {
		upsertEmployee(input: $input) {
			employee {
				id
				firstName
				lastName
				email
				locations {
					id
				}
				services {
					id
				}
				bookingEnabled
				permissionLevel
			}
		}
	}
`

export const deleteEmployeeMutation = gql`
	mutation deleteEmployee($id: ID!) {
		deleteEmployee(id: $id)
	}
`
