import setHours from 'date-fns/set_hours'
import setMinutes from 'date-fns/set_minutes'

export function timeStringToAMPM(string) {
	let [hours, minutes] = string.split(':')

	let meridian = 'am'

	hours = parseInt(hours, 10)
	minutes = parseInt(minutes, 10)

	if (hours >= 12) {
		meridian = 'pm'

		if (hours > 12) {
			hours = hours - 12
		}
	}

	if (minutes < 10) {
		minutes = `0${minutes}`
	}

	return `${hours}:${minutes}${meridian}`
}

export function timeStringToMinutes(string) {
	const [hours, minutes] = string.split(':')

	return parseInt(hours, 10) * 60 + parseInt(minutes, 10)
}

export function shiftStartInMinutes(shift) {
	return timeStringToMinutes(shift.start_time)
}

export function dateFromTimeString(time, date) {
	const [hours, minutes] = time.split(':')

	return setHours(setMinutes(date || new Date(), parseInt(minutes, 10)), parseInt(hours, 10))
}
