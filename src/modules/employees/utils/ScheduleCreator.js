import { parse, isWithinRange, isAfter, format, isSameDay } from "date-fns"
import sortBy from "lodash/sortBy"

import { shiftStartInMinutes } from "./time"

export default class SchedulerCreator {
	sortShifts(shifts) {
		return sortBy(shifts, shiftStartInMinutes)
	}

	get(scheduleRanges, date) {
		return scheduleRanges.find(range => {
			let withinRange

			const start = range.start_date

			if (range.end_date) {
				const end = range.end_date

				if (range.day_of_week === format(date, "dddd").toLowerCase()) {
					withinRange = isWithinRange(date, start, end) || isSameDay(start, date) || isSameDay(end, date)
				}
			} else {
				if (range.day_of_week === format(date, "dddd").toLowerCase()) {
					withinRange = isSameDay(date, start) || isAfter(date, start)
				}
			}

			return withinRange
		})
	}

	filter(dates, needle) {
		return dates.filter(date => {
			return isWithinRange(needle, parse(date.start_date), parse(date.end_date))
		})
	}
}
