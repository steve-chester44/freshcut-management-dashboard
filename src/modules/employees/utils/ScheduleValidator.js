import { timeStringToMinutes } from "./time"
import isBefore from "date-fns/is_before"

export default class ScheduleValidator {
	datesInverted(dates) {
		return isBefore(dates.end_date, dates.start_date)
	}

	shiftsInvalid(shifts) {
		return this.shiftTimesInverted(shifts).length > 0 || this.shiftsOverlapping(shifts).length > 0
	}

	shiftsOverlapping(shifts) {
		return shifts.filter(shift => {
			const start1 = timeStringToMinutes(shift.start_time)
			const end1 = timeStringToMinutes(shift.end_time)

			return shifts.some(shift2 => {
				// skip same dates
				if (shift === shift2) return false

				const start2 = timeStringToMinutes(shift2.start_time)
				const end2 = timeStringToMinutes(shift2.end_time)

				return start1 <= end2 && start2 <= end1
			})
		})
	}

	shiftTimesInverted(shifts = []) {
		return shifts.filter(shift => {
			const start = timeStringToMinutes(shift.start_time)
			const end = timeStringToMinutes(shift.end_time)

			return end < start
		})
	}
}
