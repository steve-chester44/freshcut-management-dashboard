import React from "react"
import { useQuery } from "@apollo/react-hooks"
import format from "date-fns/format"

import { Table, Spin } from "antd"
import { employeeSchedulesByLocation } from "../../queries"
import ScheduleCreator from "../../utils/ScheduleCreator"
import DayCell from "./DayCell"
import ScheduleHoursModal from "../ScheduleHoursModal"

const scheduler = new ScheduleCreator()

const components = { body: { cell: DayCell } }

const SchedulingTable = ({ days, locationId }) => {
	const [state, setState] = React.useState({
		editHoursModalVisible: false,
		// its possible for there to be an activeDate without an activeRange
		activeDate: undefined,
		activeEmployee: undefined
	})

	const queryVariables = {
		employees: {
			where: {
				locationId,
				bookingEnabled: true
			}
		},
		ranges: {
			where: {
				start_date: format(days[0], "YYYY-MM-DD"),
				end_date: format(days[6], "YYYY-MM-DD")
			}
		}
	}

	const { loading, data } = useQuery(employeeSchedulesByLocation, {
		variables: queryVariables
	})

	/**
	 * click a date.. create schedule_shifts.
	 * dont-repeat === 1 day only, so find schedule_range or create one
	 * repeat-weekly, ongoing = find schedule_range with same
	 */
	const toggleEditHoursModal = (activeEmployee, activeDate, activeRange) =>
		setState(prev => ({
			...prev,
			activeDate,
			activeEmployee,
			activeRange,
			editHoursModalVisible: !!activeDate
		}))

	const closeScheduleModal = () =>
		setState(prev => ({
			...prev,
			editHoursModalVisible: false,
			activeDate: undefined,
			activeEmployee: undefined
		}))

	const dayColumns = React.useMemo(() => {
		return days.map(day => {
			const title = format(day, "ddd, MMM Do")

			return {
				key: title,
				title,
				width: "10%",
				onCell: employee => {
					const range = scheduler.get(employee.schedule_ranges, day)

					return {
						openEditHoursModal: ({ activeRange }) => toggleEditHoursModal(employee, day, activeRange),
						range,
						isDate: true
					}
				}
			}
		})
	}, [days])

	if (loading || !data.employees) return <Spin />

	const columns = [
		{
			title: "Name",
			key: "id",
			render: employee => `${employee.firstName} ${employee.lastName || ""}`
		},
		...dayColumns
	]

	return (
		<div className="page-load-fade">
			{state.editHoursModalVisible && (
				<ScheduleHoursModal
					locationId={locationId}
					visible={state.editHoursModalVisible}
					employee={state.activeEmployee}
					range={state.activeRange}
					activeDate={state.activeDate}
					onClose={closeScheduleModal}
					refetchQueryVariables={queryVariables}
				/>
			)}

			<Table components={components} bordered rowKey="id" columns={columns} dataSource={data.employees || []} />
		</div>
	)
}

export default SchedulingTable
