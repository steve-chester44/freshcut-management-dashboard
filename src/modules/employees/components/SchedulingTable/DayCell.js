import React from "react"
import styled from "styled-components"

import { Icon, Tag } from "antd"

import { timeStringToAMPM } from "../../utils/time"

const emptyStyles = ({ empty }) =>
	empty &&
	`
    .inner {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        
        .anticon {
            opacity: 0;
            font-size: 22px;
            transition: opacity .2s linear;
        }
        
        &:hover {
            cursor: pointer;
            .anticon {
                opacity: 1;
            }
        }
    }
`

const Cell = styled("td")`
	${emptyStyles}
`

const Default = props => <td>{props.children}</td>

const DayCell = ({ children, isDate, range, openEditHoursModal, ...rest }) => {
	if (!isDate) return <Default>{children}</Default>

	if (!range || range.schedule_shifts.length === 0) {
		return (
			<Cell empty onClick={openEditHoursModal}>
				<div className="inner">
					<Icon type="plus" />
				</div>
			</Cell>
		)
	}

	return (
		<Cell onClick={() => openEditHoursModal({ activeRange: range })}>
			{range.schedule_shifts.map((shift, index) => (
				<div key={index}>
					<Tag color="rgba(54, 70, 141, 1.0)" style={{ width: "100%" }}>
						{timeStringToAMPM(shift.start_time)} - {timeStringToAMPM(shift.end_time)}
					</Tag>
				</div>
			))}
		</Cell>
	)
}

export default DayCell
