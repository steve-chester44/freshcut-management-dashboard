import React from 'react'
import { Switch } from 'antd'
import styled from 'styled-components'

const Container = styled('div')`
	width: 100%;

	.item {
		margin: 10px 0;
		padding: 10px 0;
		border-bottom: 1px solid rgba(249, 249, 249, 1);

		span {
			font-size: 16px;
			padding-left: 8px;
		}
	}
`

const LocationListItem = ({ onChange, locations, selectedLocations }) => {
	return (
		<Container>
			<p style={{ paddingTop: 20, opacity: 0.5, textAlign: 'center' }}>
				Staff members can only work and receive appointments at locations that they're assigned to. Assign this person
				to appropriate locations.
			</p>

			{locations.map(location => (
				<div className="item" key={`location-${location.id}`}>
					<Switch onChange={checked => onChange(checked, location)} checked={!!selectedLocations[location.id]} />
					<span>{location.name}</span>
				</div>
			))}
		</Container>
	)
}

export default LocationListItem
