import React, { Fragment, PureComponent } from 'react'
import styled from 'styled-components'

import { Query } from 'react-apollo'
import { Tabs, Tooltip, Icon, Switch, Modal, message, Form, Input, Select } from 'antd'

import { companyLocationsQuery } from '../../../../common/queries/Location'
import { employeesQuery } from '../../queries'
import { serviceGroupQuery } from '../../../services/queries'

import ServicesSelector from './ServicesSelector'
import LocationsSelector from './LocationsSelector'
import EmployeeModalFooter from './EmployeeModalFooter'

import validateEmail from '../../../../common/utils/validate-email'
import { displayErrors } from '../../../../graphql/utils'

const OWNER_PERMISSION_LEVEL = 'owner'

const FormRow = styled('div')`
	display: flex;
	align-items: center;
	justify-content: space-between;

	.ant-form-item {
		flex: 1;

		&:first-child {
			margin-right: 20px;
		}
	}
`

const StyledModal = styled(Modal)`
	.ant-modal-body {
		padding-top: 10px;
	}
`

const initialState = {
	isEditing: false,
	isSubmitting: false,
	isModalVisible: false,
	showEmailedUserAlert: false,

	fields: {
		firstName: '',
		lastName: '',
		permissionLevel: 'full',
		bookingEnabled: true,
		email: '',
		locations: [],
		services: []
	}
}

class EmployeeModal extends PureComponent {
	constructor(props) {
		super(props)

		this.state = {
			...initialState,
			isModalVisible: props.visible,
			isEditing: !!props.employee,
			selectedLocations: !props.employee
				? {}
				: props.employee.locations.reduce((acc, curr) => {
						acc[curr.id] = true
						return acc
				  }, {}),
			selectedServices: !props.employee
				? {}
				: props.employee.services.reduce((acc, curr) => {
						acc[curr.id] = true
						return acc
				  }, {}),
			fields: {
				...initialState.fields,
				...props.employee
			}
		}
	}

	handleOpen = () => {
		this.setState({
			isModalVisible: true,
			isEditing: this.props.employee && this.props.employee.id
		})
	}

	handleClose = () => {
		if (this.props.onClose) {
			this.props.onClose()
		} else {
			this.setState({ ...initialState })
		}
	}

	handleInputChange = ({ target: { name, value } }) => {
		this.setState({ fields: { ...this.state.fields, [name]: value } })
	}

	handleServicesChange = (checked, service) => {
		const selectedServices = { ...this.state.selectedServices }

		if (checked) {
			selectedServices[service.id] = true
		} else {
			delete selectedServices[service.id]
		}

		const services = checked
			? this.state.fields.services.concat([service])
			: this.state.fields.services.filter(s => s.id !== service.id)

		this.setState({
			...this.state,
			selectedServices,
			fields: {
				...this.state.fields,
				services
			}
		})
	}

	handleLocationsChange = (checked, location) => {
		const selectedLocations = { ...this.state.selectedLocations }

		if (checked) {
			selectedLocations[location.id] = true
		} else {
			delete selectedLocations[location.id]
		}

		const locations = checked
			? this.state.fields.locations.concat([location])
			: this.state.fields.locations.filter(loc => loc.id !== location.id)

		this.setState({
			...this.state,
			selectedLocations,
			fields: {
				...this.state.fields,
				locations
			}
		})
	}

	handleUpsert = (
		cache,
		{
			data: {
				upsertEmployee: { employee }
			}
		}
	) => {
		const { employees } = cache.readQuery({ query: employeesQuery })

		// TODO: Can we use readFragment/writeFragment to updaet this employee?
		cache.writeQuery({
			query: employeesQuery,
			data: {
				employees: this.state.isEditing
					? employees.map(emp => (emp.id === employee.id ? employee : emp))
					: employees.concat([employee])
			}
		})
	}

	handleDelete = (cache, { data: { deleteEmployee } }) => {
		if (deleteEmployee) {
			const { employees } = cache.readQuery({ query: employeesQuery })

			cache.writeQuery({
				query: employeesQuery,
				data: {
					employees: employees.filter(employee => +employee.id !== +this.state.fields.id)
				}
			})
		} else {
			message.error('Failed to delete the employee.')
		}
	}

	handleSubmission = async upsertEmployee => {
		this.setState({ isSubmitting: true })
		const { __typename, ...input } = this.state.fields

		try {
			await upsertEmployee({
				variables: {
					input: {
						...input,
						services: input.services.map(i => i.id),
						locations: input.locations.map(i => i.id)
					}
				}
			})

			message.success(this.state.isEditing ? 'Employee updated' : 'Employee created')

			if (!this.state.isEditing && input.permissionLevel !== 'none') {
				return this.setState({ showEmailedUserAlert: true, isSubmitting: false })
			} else {
				return this.handleClose()
			}
		} catch (error) {
			this.setState({ isSubmitting: false })
		}
	}

	renderTabs() {
		const { fields } = this.state

		return (
			<div>
				<Tabs defaultActiveKey="details">
					<Tabs.TabPane tab="Details" key="details">
						<FormRow>
							<Form.Item label="First Name" colon={false}>
								<Input type="text" value={fields.firstName} name="firstName" onChange={this.handleInputChange} />
							</Form.Item>

							<Form.Item label="Last Name" colon={false}>
								<Input type="text" value={fields.lastName} name="lastName" onChange={this.handleInputChange} />
							</Form.Item>
						</FormRow>

						<Form.Item label="Email" colon={false}>
							<Input type="text" value={fields.email} name="email" onChange={this.handleInputChange} />
						</Form.Item>

						<Form.Item label="Permission Level">
							{fields.permissionLevel !== OWNER_PERMISSION_LEVEL ? (
								<Select
									style={{ width: '100%' }}
									value={fields.permissionLevel}
									onChange={permissionLevel => this.setState({ fields: { ...this.state.fields, permissionLevel } })}
								>
									<Select.Option value="none">No Access</Select.Option>
									<Select.Option value="limited">Limited</Select.Option>
									<Select.Option value="regular">Regular</Select.Option>
									<Select.Option value="full">Full Access</Select.Option>
								</Select>
							) : (
								<Select disabled={true} value={fields.permissionLevel} style={{ width: '100%' }}>
									<Select.Option value={OWNER_PERMISSION_LEVEL}>Owner</Select.Option>
								</Select>
							)}
						</Form.Item>

						<div style={{ display: 'flex' }}>
							<Switch
								checked={fields.bookingEnabled}
								onChange={bookingEnabled => {
									this.setState({ fields: { ...this.state.fields, bookingEnabled } })
								}}
							/>

							<span style={{ paddingLeft: 10 }}>
								<span>Booking Enabled </span>
								<Tooltip size="small" title="Employees with this enabled will be visible in the calendar">
									<Icon type="question-circle-o" />
								</Tooltip>
							</span>
						</div>
					</Tabs.TabPane>
					<Tabs.TabPane tab="Locations" key="locations">
						<Query query={companyLocationsQuery}>
							{({ loading, data }) => {
								if (loading) return <div>Loading locations...</div>
								if (!data || !data.locations) return <div>An error has occurred.</div>

								return (
									<LocationsSelector
										locations={data.locations}
										selectedLocations={this.state.selectedLocations}
										onChange={this.handleLocationsChange}
									/>
								)
							}}
						</Query>
					</Tabs.TabPane>
					<Tabs.TabPane tab="Services" key="services" disabled={!fields.bookingEnabled}>
						<Query query={serviceGroupQuery}>
							{({ loading, data, errors }) => {
								if (loading) return <div>Loading Services...</div>
								if (!data || !data.serviceGroups) return <div>An error has occurred</div>

								return (
									<ServicesSelector
										onChange={this.handleServicesChange}
										groups={data.serviceGroups}
										selectedServices={this.state.selectedServices}
									/>
								)
							}}
						</Query>
					</Tabs.TabPane>
				</Tabs>
			</div>
		)
	}

	renderEmailedUserMessage() {
		const { fields } = this.state

		return (
			<div style={{ paddingTop: 10 }}>
				<p>
					Nice! We created an account for <b>{fields.firstName}</b> with <b>{fields.permissionLevel}</b> permissions.
				</p>

				<p>
					A getting started email has been sent to this person with details on how to enable and configure their
					account.
				</p>
			</div>
		)
	}

	render() {
		const { fields, isEditing, isSubmitting, isModalVisible, showEmailedUserAlert } = this.state

		return (
			<Fragment>
				{isModalVisible && (
					<StyledModal
						isEditing={isEditing}
						title={isEditing ? 'Edit Employee' : 'Create Employee'}
						visible={isModalVisible}
						onCancel={this.handleClose}
						footer={
							<EmployeeModalFooter
								isEditing={isEditing}
								isSubmitting={isSubmitting}
								isDisabled={
									fields.firstName.length === 0 ||
									(fields.email && !validateEmail(fields.email)) ||
									(fields.permissionLevel !== 'none' && !validateEmail(fields.email))
								}
								showEmailedUserAlert={showEmailedUserAlert}
								canDelete={fields.permissionLevel !== OWNER_PERMISSION_LEVEL}
								employeeId={fields.id}
								onDelete={this.handleDelete}
								onUpsert={this.handleUpsert}
								onSubmit={this.handleSubmission}
								onClose={this.handleClose}
							/>
						}
					>
						{showEmailedUserAlert ? this.renderEmailedUserMessage() : this.renderTabs()}
					</StyledModal>
				)}

				{this.props.trigger && this.props.trigger({ open: this.handleOpen })}
			</Fragment>
		)
	}
}

export default EmployeeModal
