/**
 * TODO: This thing sucks.. I hate the way i'm passing around the `on` methods. Rewrite the EmployeeModal 
 */
import React from 'react'

import { Popconfirm, Button } from 'antd'

import { upsertEmployeeMutation, deleteEmployeeMutation } from '../../mutations'
import { useMutation } from '@apollo/react-hooks'

const EmployeeModalFooter = ({
	isEditing,
	isSubmitting,
	isDisabled,
	canDelete,
	employeeId,
	onDelete,
	onClose,
	onUpsert,
	onSubmit,
	showEmailedUserAlert
}) => {
	// TODO: REWRITE THIS
	const [deleteUser] = useMutation(deleteEmployeeMutation, { update: onDelete })
	const [upsertUser] = useMutation(upsertEmployeeMutation, { update: onUpsert })

	if (showEmailedUserAlert) {
		return (
			<div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
				<Button onClick={onClose}>Close</Button>
			</div>
		)
	}

	return (
		<div style={{ display: 'flex', justifyContent: isEditing ? 'space-between' : 'flex-end', width: '100%' }}>
			{isEditing && canDelete ? (
				<Popconfirm
					placement="topLeft"
					okText="Delete"
					cancelText="Cancel"
					title="Are you sure you want to delete this employee?"
					onConfirm={async () => {
						await deleteUser({ variables: { id: employeeId } })

						if (onClose) {
							onClose()
						}
					}}
				>
					<Button type="danger">Delete</Button>
				</Popconfirm>
			) : (
				<span />
			)}

			<div>
				<Button onClick={onClose}>Cancel</Button>

				<Button type="primary" disabled={isDisabled} loading={isSubmitting} onClick={() => onSubmit(upsertUser)}>
					{isEditing ? 'Update' : 'Create'}
				</Button>
			</div>
		</div>
	)
}

export default EmployeeModalFooter
