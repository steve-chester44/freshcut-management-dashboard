import React from 'react'
import { Row, Col, Switch } from 'antd'
import styled from 'styled-components'

const CheckboxTable = styled('div')`
	width: 100%;

	.item {
		margin: 10px 0;
		padding: 10px 0;
		border-bottom: 1px solid rgba(249, 249, 249, 1);

		span {
			font-size: 16px;
			padding-left: 8px;
		}
	}
`

const ServiceGroupListItem = ({ groups, onChange, selectedServices }) => {
	if (groups.length === 0)
		return (
			<h3 style={{ paddingTop: 20, opacity: 0.5, textAlign: 'center' }}>
				You need to create service groups before you can assign them to a staff member.
			</h3>
		)

	return (
		<CheckboxTable>
			<p style={{ paddingTop: 20, opacity: 0.6, textAlign: 'center' }}>
				Staff members can only be booked for services that they can perform. Select the appropriate services for this
				person below.
			</p>

			<Row>
				{groups.map(group => {
					if (group.services.length === 0) {
						return groups.length === 1 ? (
							<p style={{ opacity: 0.5, textAlign: 'center' }}>You haven't created any services yet.</p>
						) : (
							<span key="empty" />
						)
					}

					return (
						<Col xs={24} lg={12} key={`group-${group.id}`}>
							<div className="group">
								<h3>{group.name}</h3>
								{group.services.map(service => (
									<div className="item" key={`service-${service.id}`}>
										<Switch onChange={checked => onChange(checked, service)} checked={!!selectedServices[service.id]} />
										<span>{service.name}</span>
									</div>
								))}
							</div>
						</Col>
					)
				})}
			</Row>
		</CheckboxTable>
	)
}

export default ServiceGroupListItem
