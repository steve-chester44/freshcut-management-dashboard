import React from 'react'

import { Modal, Button, Row, TimePicker, Col, message, Select, Calendar, Popconfirm } from 'antd'
import moment from 'moment'
import { useMutation } from '@apollo/react-hooks'
import { setHours, setMinutes, addMinutes, format, isBefore, addHours, isAfter, parse } from 'date-fns'
import { displayErrors } from '../../../../graphql/utils'
import { updateScheduleMutation, deleteScheduleMutation } from '../../mutations'
import ShiftValidator from '../../utils/ScheduleValidator'
import { dateFromTimeString } from '../../utils/time'
import { employeeSchedulesByLocation } from '../../queries'

const validator = new ShiftValidator()

const REPEAT_STATES = {
	dont: 'DONT',
	weekly: 'WEEKLY',
	ongoing: 'ONGOING',
	specific: 'SPECIFIC'
}

const ScheduleHoursModal = ({ visible, activeDate, employee, range, locationId, refetchQueryVariables, onClose }) => {
	const [updateSchedule] = useMutation(updateScheduleMutation)
	const [deleteSchedule] = useMutation(deleteScheduleMutation)

	const [state, setState] = React.useState({
		submitting: false,
		values: {
			repeat: !range || range.start_date === range.end_date ? REPEAT_STATES.dont : REPEAT_STATES.weekly,
			endRepeat: !range || !range.end_date ? REPEAT_STATES.ongoing : REPEAT_STATES.specific,

			endDate: range && range.end_date ? parse(range.end_date) : activeDate,
			schedule_shifts:
				range && range.schedule_shifts.length > 0
					? range.schedule_shifts.map(shift => {
							const start_time = dateFromTimeString(shift.start_time, activeDate)
							const end_time = dateFromTimeString(shift.end_time, activeDate)

							return { start_time, end_time }
					  })
					: [
							{
								start_time: setMinutes(setHours(activeDate, 9), 0),
								end_time: setMinutes(setHours(activeDate, 17), 0)
							}
					  ]
		}
	})

	if (!activeDate || !employee) return null

	const setSubmitting = submitting => setState(prev => ({ ...prev, submitting }))

	const insertEmptyShift = () => {
		const lastShift = state.values.schedule_shifts[0]

		// Start 30 minutes after the last shift
		const start_time = addMinutes(lastShift.end_time, 30)
		const end_time = addMinutes(start_time, 60)

		setState(prev => ({
			...prev,
			values: {
				...state.values,
				schedule_shifts: prev.values.schedule_shifts.concat([
					{
						start_time,
						end_time
					}
				])
			}
		}))
	}

	const setEndDate = momentObj => {
		const endDate = momentObj.toDate()
		if (isBefore(endDate, new Date())) {
			message.warn('Dates must be in the future.')
			return
		}

		setState(prev => ({
			...prev,
			values: {
				...prev.values,
				endDate
			}
		}))
	}

	const handleDelete = async () => {
		try {
			setSubmitting(true)

			await deleteSchedule({
				refetchQueries: [{ query: employeeSchedulesByLocation, variables: refetchQueryVariables }],
				variables: {
					input: {
						scheduleRangeId: range.id
					}
				}
			})
			
			onClose()

		} catch (error) {
			setSubmitting(false)
			displayErrors(error)
		}
	}

	const handleSave = async () => {
		const shifts = state.values.schedule_shifts.map(shift => ({
			start_time: format(shift.start_time, 'HH:mm'),
			end_time: format(shift.end_time, 'HH:mm')
		}))

		const isInvalid = validator.shiftsInvalid(shifts)
		setSubmitting(true)

		if (isInvalid) {
			return message.error('Shift hours are invalid.')
		}

		const variables = {
			input: {
				employeeId: employee.id,
				locationId: locationId,
				start_date: activeDate,
				schedule_shifts: state.values.schedule_shifts.map(shift => ({
					start_time: format(shift.start_time, 'HH:mm'),
					end_time: format(shift.end_time, 'HH:mm')
				}))
			}
		}

		// There won't be an endDate if the user is setting an on-going weekly repeating schedule
		if (state.values.endDate) {
			variables.input.end_date = state.values.endDate
		}

		try {
			await updateSchedule({
				variables,
				refetchQueries: [{ query: employeeSchedulesByLocation, variables: refetchQueryVariables }]
			})

			onClose()
		} catch (error) {
			displayErrors(error)
			setSubmitting(false)
		}
	}

	return (
		<Modal
			onCancel={onClose}
			footer={
				<div style={{ display: 'flex', justifyContent: 'space-between' }}>
					{!!range ? (
						<Popconfirm
							title="Are you sure you want to delete this?"
							onConfirm={handleDelete}
							okText="Yes"
							cancelText="No"
						>
							<Button type="danger">Delete</Button>
						</Popconfirm>
					) : (
						<div />
					)}

					<div>
						<Button onClick={onClose}>Cancel</Button>
						<Button loading={state.submitting} type="primary" onClick={handleSave}>
							Update
						</Button>
					</div>
				</div>
			}
			visible={visible}
			title={`${employee.firstName}'s Hours`}
		>
			{state.values.schedule_shifts.map((shift, index) => {
				return (
					<Row style={{ marginBottom: 14 }} gutter={16} key={index}>
						<Col xs={12}>
							{index === 0 && <h3>Shift Start</h3>}
							<TimePicker
								style={{ width: '100%' }}
								use12Hours
								minuteStep={5}
								value={moment(shift.start_time)}
								secondStep={60}
								format="hh:mm a"
								onChange={momentObj => {
									setState(prev => {
										const schedule_shifts = prev.values.schedule_shifts
										const date = momentObj.toDate()
										schedule_shifts[index].start_time = date

										if (isAfter(date, schedule_shifts[index].end_time)) {
											schedule_shifts[index].end_time = addHours(date, 1)
										}

										return {
											...prev,
											values: {
												...prev.values,
												schedule_shifts
											}
										}
									})
								}}
							/>
						</Col>
						<Col xs={12}>
							{index === 0 && <h3>Shift End</h3>}
							<TimePicker
								style={{ width: '100%' }}
								use12Hours
								minuteStep={5}
								value={moment(shift.end_time)}
								secondStep={60}
								format="hh:mm a"
								onChange={momentObj => {
									setState(prev => {
										const schedule_shifts = prev.values.schedule_shifts
										schedule_shifts[index].end_time = momentObj.toDate()

										return {
											...prev,
											values: {
												...prev.values,
												schedule_shifts
											}
										}
									})
								}}
							/>
						</Col>
					</Row>
				)
			})}
			<Row>
				<Col xs={24}>
					{state.values.schedule_shifts.length < 2 && (
						<Button icon="plus" onClick={insertEmptyShift}>
							Add Another Shift
						</Button>
					)}
				</Col>
			</Row>

			<Row style={{ marginTop: 24 }} gutter={16}>
				<Col xs={state.values.repeat === REPEAT_STATES.dont ? 24 : 12}>
					<h3>Repeat</h3>
					<Select
						style={{ width: '100%' }}
						onChange={repeat =>
							setState(prev => ({
								...prev,
								values: {
									...prev.values,
									repeat,
									endRepeat: repeat !== REPEAT_STATES.weekly ? REPEAT_STATES.ongoing : prev.values.endRepeat,
									endDate: repeat === REPEAT_STATES.weekly ? undefined : prev.values.endDate
								}
							}))
						}
						value={state.values.repeat}
					>
						<Select.Option value={REPEAT_STATES.dont}>Don't Repeat</Select.Option>
						<Select.Option value={REPEAT_STATES.weekly}>Weekly</Select.Option>
					</Select>
				</Col>

				{state.values.repeat !== REPEAT_STATES.dont && (
					<Col xs={12}>
						<h3>End Repeat</h3>
						<Select
							style={{ width: '100%' }}
							onChange={endRepeat =>
								setState(prev => ({
									...prev,
									values: {
										...prev.values,
										endRepeat,
										endDate: endRepeat === REPEAT_STATES.ongoing ? undefined : activeDate
									}
								}))
							}
							value={state.values.endRepeat}
						>
							<Select.Option value={REPEAT_STATES.ongoing}>Forever</Select.Option>
							<Select.Option value={REPEAT_STATES.specific}>Specific Date</Select.Option>
						</Select>
					</Col>
				)}
			</Row>

			{state.values.endRepeat === REPEAT_STATES.specific && (
				<Row style={{ marginTop: 24 }}>
					<Col xs={24}>
						<Calendar mode="month" value={moment(state.values.endDate)} onSelect={setEndDate} fullscreen={false} />
					</Col>
				</Row>
			)}
		</Modal>
	)
}

export default ScheduleHoursModal
