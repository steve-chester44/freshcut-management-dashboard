import React from "react"

import { Table, Button } from "antd"

import EmployeeModal from "../EmployeeModal/EmployeeModal"

const columns = [
	{
		title: "Name",
		dataIndex: "firstName",
		key: "firstName",
		sorter: (a, b) => a.firstName.localeCompare(b.firstName),
		render: (_, employee) => (
			<span>
				{employee.firstName} {employee.lastName}
			</span>
		)
	},
	{
		title: "Permissions",
		dataIndex: "permissionLevel",
		key: "permissionLevel",
		render: level => {
			return level === "none" ? "No Access" : <span style={{ textTransform: "capitalize" }}>{level}</span>
		}
	},
	{
		title: "Booking",
		dataIndex: "bookingEnabled",
		key: "bookingEnabled",
		render: enabled => {
			return enabled ? "Enabled" : "Disabled"
		}
	},
	{
		align: "right",
		render: employee => {
			return <Button>Manage</Button>
		}
	}
]

const EmployeesTable = ({ employees, loading }) => {
	const [state, setState] = React.useState({
		isEmployeeModalVisible: false,
		activeEmployee: null
	})

	const toggleEmployeeModal = () => {
		return setState(prev => ({ ...prev, isEmployeeModalVisible: !prev.isEmployeeModalVisible }))
	}

	const handleEmployeeSelection = activeEmployee => {
		return setState(prev => ({ ...prev, isEmployeeModalVisible: true, activeEmployee }))
	}

	if (!loading && employees.length === 0) {
		return <div style={{ textAlign: "center" }}>You haven't created any employees yet.</div>
	}

	return (
		<div className="page-load-fade">
			{state.isEmployeeModalVisible && (
				<EmployeeModal employee={state.activeEmployee} visible={true} onClose={toggleEmployeeModal} />
			)}

			<Table
				size="small"
				loading={loading}
				rowKey="id"
				onRow={employee => ({
					onClick: () => handleEmployeeSelection(employee)
				})}
				pagination={employees.length >= 10}
				columns={columns}
				dataSource={employees}
			/>
		</div>
	)
}

export default EmployeesTable
