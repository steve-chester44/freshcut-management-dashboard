import React from "react"
import { Calendar, Views, momentLocalizer } from "react-big-calendar"
import moment from "moment"

import "./style-overrides.css"
import ClosedDatesToolbar from "./ClosedDatesCalendarToolbar"
import isBefore from "date-fns/is_before"
import startOfDay from "date-fns/start_of_day"
import DateCell from "./DateCell"
import DateHeader from "./DateHeader"

const localizer = momentLocalizer(moment)

const allViews = [Views.MONTH]

const defaultDate = new Date()

const today = startOfDay(new Date())

const eventProps = event => {
	const isInPast = isBefore(event.end, today)
	return {
		style: {
			border: 0,
			color: "#fff",
			background: isInPast ? "rgba(69, 69, 69, 0.4)" : "rgba(69,69,69,1)",
			outlineColor: "rgba(100, 118, 214, 1.0)",
			borderRadius: "25px",
			paddingLeft: 10,
			userSelect: "none"
		}
	}
}

const components = {
	dateCellWrapper: DateCell,
	toolbar: ClosedDatesToolbar,
	month: {
		dateHeader: DateHeader
	}
}

const index = ({ datesClosed, onSelect, onSelectEvent }) => {
	const handleSelect = date => {
		if (!isBefore(date.start, today)) {
			onSelect(date)
		}
	}

	return (
		<Calendar
			popup
			components={components}
			className="closed-dates-calendar"
			selectable
			localizer={localizer}
			events={datesClosed}
			views={allViews}
			defaultDate={defaultDate}
			eventPropGetter={eventProps}
			onSelectEvent={onSelectEvent}
			onSelectSlot={handleSelect}
		/>
	)
}

export default index
