import React from "react"

import styled from "styled-components"
import isBefore from "date-fns/is_before"
import startOfDay from "date-fns/start_of_day"
import { isToday } from "date-fns"

const today = startOfDay(new Date())

const Label = styled("span")`
	opacity: 1;
	position: relative;
	color: rgba(54, 70, 141, 1);
	border-radius: 50%;
	font-size: 24px;
`

const DisabledLabel = styled(Label)`
	color: rgba(227, 227, 227, 1);
`

const DefaultLabel = styled(Label)`
	${({ offRange, disabled }) => !disabled && offRange && `color: rgba(49,49,49,0.4);`}
	${({ isToday }) =>
		isToday &&
		`
        background: rgba(255, 198, 0, 0.3);
    `}
`

const DateCellWrapper = ({ label, date, isOffRange }) => {
	const isInPast = isBefore(date, today)
	const dateToday = isToday(date)

	const Wrapper = isInPast ? DisabledLabel : DefaultLabel
	return (
		<Wrapper disabled={isInPast} isToday={dateToday} offRange={isOffRange}>
			{label}
		</Wrapper>
	)
}

export default DateCellWrapper
