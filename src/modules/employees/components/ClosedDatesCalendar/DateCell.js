import React from "react"
import isBefore from "date-fns/is_before"
import startOfDay from "date-fns/start_of_day"

const disabledStyles = {
	backgroundImage: `linear-gradient(
    45deg,
    rgba(249, 249, 249, 1) 25%,
   rgba(235, 235, 235, 0.6) 25%,
   rgba(235, 235, 235, 0.6) 50%,
    rgba(249, 249, 249, 1) 50%,
    rgba(249, 249, 249, 1) 75%,
   rgba(235, 235, 235, 0.6) 75%,
   rgba(235, 235, 235, 0.6) 100%
)`,
	backgroundSize: "8.49px 8.49px"
}

const defaultStyles = {
	cursor: "pointer"
}

const today = startOfDay(new Date())

const DateCellWrapper = ({ children, value }) => {
	const isInPast = isBefore(value, today)
	return React.cloneElement(React.Children.only(children), {
		style: {
			...children.style,
			...(isInPast ? disabledStyles : defaultStyles)
		}
	})
}

export default DateCellWrapper
