import React from 'react'
import styled from 'styled-components'

import { Row, Col } from 'antd'
import DateSelector from '../../../calendar/components/Toolbar/DateSelector'

const StyledRow = styled(Row)`
	padding-bottom: 20px;
`

const ClosedDatesToolbar = ({ onNavigate, date }) => {
	return (
		<StyledRow type="flex" justify="space-between" align="middle">
			<Col xs={24} md={12}>
				<h1 style={{ margin: 0 }}>Closed Dates</h1>
			</Col>
			<Col xs={24} md={12} style={{ textAlign: 'right' }}>
				<DateSelector format="MMMM YYYY" onNavigate={onNavigate} date={date} />
			</Col>
		</StyledRow>
	)
}

export default ClosedDatesToolbar
