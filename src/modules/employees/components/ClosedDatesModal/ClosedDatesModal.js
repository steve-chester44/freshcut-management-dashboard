import React from 'react'
import { Modal, Button, Popconfirm, DatePicker, Input, Row, Col, message } from 'antd'
import { useMutation } from '@apollo/react-hooks'
import { upsertClosedDatesMutation, deleteClosedDatesMutation } from '../../mutations'
import moment from 'moment'
import ScheduleValidator from '../../utils/ScheduleValidator'
import { displayErrors } from '../../../../graphql/utils'
import { closedDatesQuery } from '../../queries'
import startOfDay from 'date-fns/start_of_day'
import endOfDay from 'date-fns/end_of_day'

const validator = new ScheduleValidator()

const ClosedDatesModal = ({ locations, closedDate, refetchQueryVariables, onClose }) => {
	const [upsertDate] = useMutation(upsertClosedDatesMutation)
	const [deleteDate] = useMutation(deleteClosedDatesMutation)
	const isCreating = !closedDate.id

	const [state, setState] = React.useState({
		submitting: false,
		fields: closedDate
	})
	const setFieldValue = (key, value) => setState(prev => ({ ...prev, fields: { ...prev.fields, [key]: value } }))

	const handleDelete = async () => {
		try {
			const input = {
				variables: {
					input: {
						id: closedDate.id
					}
				}
			}

			if (refetchQueryVariables) {
				input.refetchQueries = [{ query: closedDatesQuery, variables: refetchQueryVariables }]
			}

			const { data } = await deleteDate(input)

			if (data.deleteClosedDates) {
				onClose()
			}
		} catch (error) {
			displayErrors(error)
		}
	}
	const handleSave = async () => {
		if (validator.datesInverted(state.fields)) {
			return message.error('Start and end dates are invalid')
		}

		const input = {
			variables: {
				input: {
					id: closedDate.id,
					description: state.fields.description,
					start_date: startOfDay(state.fields.start_date),
					end_date: endOfDay(state.fields.end_date),
					locations: locations.length === 1 ? [{ id: locations[0].id }] : state.fields.locations
				}
			}
		}

		// FIXME: Smelly.. why must we pass query variables around
		if (refetchQueryVariables) {
			input.refetchQueries = [{ query: closedDatesQuery, variables: refetchQueryVariables }]
		}

		try {
			const { data } = await upsertDate(input)

			if (data.upsertClosedDates) {
				onClose()
			}
		} catch (error) {
			displayErrors(error)
		}
	}

	return (
		<Modal
			onCancel={onClose}
			title={isCreating ? 'Add Closed Date' : 'Edit Closed Date'}
			visible={true}
			footer={
				<div style={{ display: 'flex', justifyContent: 'space-between' }}>
					{isCreating ? (
						<div />
					) : (
						<Popconfirm
							title="Are you sure you want to delete this?"
							onConfirm={handleDelete}
							okText="Yes"
							cancelText="No"
						>
							<Button type="danger">Delete</Button>
						</Popconfirm>
					)}

					<div>
						<Button onClick={onClose}>Cancel</Button>
						<Button loading={state.submitting} type="primary" onClick={handleSave}>
							{isCreating ? 'Add' : 'Update'}
						</Button>
					</div>
				</div>
			}
		>
			<Row gutter={16} style={{ marginBottom: 14 }}>
				<Col xs={24} md={12}>
					<h3>Start Date</h3>
					<DatePicker
						value={moment(state.fields.start_date)}
						allowClear={false}
						onChange={momentObj => setFieldValue('start_date', momentObj.toDate())}
					/>
				</Col>
				<Col xs={24} md={12}>
					<h3>End Date</h3>

					<DatePicker
						value={moment(state.fields.end_date)}
						allowClear={false}
						onChange={momentObj => setFieldValue('end_date', momentObj.toDate())}
					/>
				</Col>
			</Row>

			<h3>Description</h3>
			<Input.TextArea
				value={state.fields.description}
				onChange={({ target: { value } }) => setFieldValue('description', value)}
			/>

			{locations.length > 1 && (
				<Row style={{ marginTop: 14 }}>
					<Col>
						<h3>Locations</h3>
					</Col>
				</Row>
			)}
		</Modal>
	)
}

export default ClosedDatesModal
