import gql from "graphql-tag"

export const baseEmployeeFragment = gql`
	fragment BaseEmployeeParts on User {
		id
		firstName
		lastName
		bookingEnabled
	}
`

export const employeesQuery = gql`
	query {
		employees {
			...BaseEmployeeParts
			permissionLevel
			email
			locations {
				id
			}
			services {
				id
			}
		}
	}

	${baseEmployeeFragment}
`

export const employeeSchedulesByLocation = gql`
	query schedules($employees: EmployeeQueryInput, $ranges: SchedulesInput!) {
		employees(input: $employees) {
			...BaseEmployeeParts
			schedule_ranges(input: $ranges) {
				id
				start_date
				end_date
				day_of_week
				schedule_shifts {
					start_time
					end_time
				}
			}
		}
	}

	${baseEmployeeFragment}
`

export const closedDatesQuery = gql`
	query closedDates($input: ClosedDatesInput!) {
		closedDates(input: $input) {
			id
			start_date
			end_date
			description
			locations {
				id
				name
			}
		}
	}
`
