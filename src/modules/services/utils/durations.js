import { createSelector } from 'reselect'

const createDurations = createSelector(
	hours => hours,
	hours => {
		const durations = []

		for (let i = 1; i <= 60 * hours; i++) {
			let hourDisplay = Math.floor(i / 60)
			let name = `${i >= 60 ? `${hourDisplay} ${hourDisplay === 1 ? 'hour' : 'hours'}` : ''}`

			let minutes = i % 60

			if (minutes > 0) {
				const min = `${minutes} minute${minutes > 1 ? 's' : ''}`
				name += `${name.length > 0 ? ' ' : ''}${min}`
			}

			durations.push({
				name,
				value: i
			})
		}

		return durations
	}
)

export default createDurations
