export const durations = [
	{ name: "5min", value: 5 },
	{ name: "10min", value: 10 },
	{ name: "15min", value: 15 },
	{ name: "20min", value: 20 },
	{ name: "25min", value: 25 },
	{ name: "30min", value: 30 },
	{ name: "35min", value: 35 },
	{ name: "40min", value: 40 },
	{ name: "45min", value: 45 },
	{ name: "50min", value: 50 },
	{ name: "55min", value: 55 },
	{ name: "1h", value: 60 },
	{ name: "1h 5min", value: 65 },
	{ name: "1h 10min", value: 70 },
	{ name: "1h 15min", value: 75 },
	{ name: "1h 20min", value: 80 },
	{ name: "1h 25min", value: 85 },
	{ name: "1h 30min", value: 90 },
	{ name: "1h 35min", value: 95 },
	{ name: "1h 40min", value: 100 },
	{ name: "1h 45min", value: 105 },
	{ name: "1h 50min", value: 110 },
	{ name: "1h 55min", value: 115 },
	{ name: "2h", value: 120 },
	{ name: "2h 5min", value: 125 },
	{ name: "2h 10min", value: 130 },
	{ name: "2h 15min", value: 135 },
	{ name: "2h 20min", value: 140 },
	{ name: "2h 25min", value: 145 },
	{ name: "2h 30min", value: 150 },
	{ name: "2h 35min", value: 155 },
	{ name: "2h 40min", value: 160 },
	{ name: "2h 45min", value: 165 },
	{ name: "2h 50min", value: 170 },
	{ name: "2h 55min", value: 175 },
	{ name: "3h", value: 180 },
	{ name: "3h 5min", value: 185 },
	{ name: "3h 10min", value: 190 },
	{ name: "3h 15min", value: 195 },
	{ name: "3h 20min", value: 200 },
	{ name: "3h 25min", value: 205 },
	{ name: "3h 30min", value: 210 },
	{ name: "3h 35min", value: 215 },
	{ name: "3h 40min", value: 220 },
	{ name: "3h 45min", value: 225 },
	{ name: "3h 50min", value: 230 },
	{ name: "3h 55min", value: 235 },
	{ name: "4h", value: 240 },
	{ name: "4h 5min", value: 245 },
	{ name: "4h 10min", value: 250 },
	{ name: "4h 15min", value: 255 },
	{ name: "4h 20min", value: 260 },
	{ name: "4h 25min", value: 265 },
	{ name: "4h 30min", value: 270 },
	{ name: "4h 35min", value: 275 },
	{ name: "4h 40min", value: 280 },
	{ name: "4h 45min", value: 285 },
	{ name: "4h 50min", value: 290 },
	{ name: "4h 55min", value: 295 },
	{ name: "5h", value: 300 },
	{ name: "5h 5min", value: 305 },
	{ name: "5h 10min", value: 310 },
	{ name: "5h 15min", value: 315 },
	{ name: "5h 20min", value: 320 },
	{ name: "5h 25min", value: 325 },
	{ name: "5h 30min", value: 330 },
	{ name: "5h 35min", value: 335 },
	{ name: "5h 40min", value: 340 },
	{ name: "5h 45min", value: 345 },
	{ name: "5h 50min", value: 350 },
	{ name: "5h 55min", value: 355 },
	{ name: "6h", value: 360 },
	{ name: "6h 5min", value: 365 },
	{ name: "6h 10min", value: 370 },
	{ name: "6h 15min", value: 375 },
	{ name: "6h 20min", value: 380 },
	{ name: "6h 25min", value: 385 },
	{ name: "6h 30min", value: 390 },
	{ name: "6h 35min", value: 395 },
	{ name: "6h 40min", value: 400 },
	{ name: "6h 45min", value: 405 },
	{ name: "6h 50min", value: 410 },
	{ name: "6h 55min", value: 415 },
	{ name: "7h", value: 420 },
	{ name: "7h 5min", value: 425 },
	{ name: "7h 10min", value: 430 },
	{ name: "7h 15min", value: 435 },
	{ name: "7h 20min", value: 440 },
	{ name: "7h 25min", value: 445 },
	{ name: "7h 30min", value: 450 },
	{ name: "7h 35min", value: 455 },
	{ name: "7h 40min", value: 460 },
	{ name: "7h 45min", value: 465 },
	{ name: "7h 50min", value: 470 },
	{ name: "7h 55min", value: 475 },
	{ name: "8h", value: 480 },
	{ name: "8h 5min", value: 485 },
	{ name: "8h 10min", value: 490 },
	{ name: "8h 15min", value: 495 },
	{ name: "8h 20min", value: 500 },
	{ name: "8h 25min", value: 505 },
	{ name: "8h 30min", value: 510 },
	{ name: "8h 35min", value: 515 },
	{ name: "8h 40min", value: 520 },
	{ name: "8h 45min", value: 525 },
	{ name: "8h 50min", value: 530 },
	{ name: "8h 55min", value: 535 },
	{ name: "9h", value: 540 },
	{ name: "9h 5min", value: 545 },
	{ name: "9h 10min", value: 550 },
	{ name: "9h 15min", value: 555 },
	{ name: "9h 20min", value: 560 },
	{ name: "9h 25min", value: 565 },
	{ name: "9h 30min", value: 570 },
	{ name: "9h 35min", value: 575 },
	{ name: "9h 40min", value: 580 },
	{ name: "9h 45min", value: 585 },
	{ name: "9h 50min", value: 590 },
	{ name: "9h 55min", value: 595 },
	{ name: "10h", value: 600 },
	{ name: "10h 5min", value: 605 },
	{ name: "10h 10min", value: 610 },
	{ name: "10h 15min", value: 615 },
	{ name: "10h 20min", value: 620 },
	{ name: "10h 25min", value: 625 },
	{ name: "10h 30min", value: 630 },
	{ name: "10h 35min", value: 635 },
	{ name: "10h 40min", value: 640 },
	{ name: "10h 45min", value: 645 },
	{ name: "10h 50min", value: 650 },
	{ name: "10h 55min", value: 655 },
	{ name: "11h", value: 660 },
	{ name: "11h 5min", value: 665 },
	{ name: "11h 10min", value: 670 },
	{ name: "11h 15min", value: 675 },
	{ name: "11h 20min", value: 680 },
	{ name: "11h 25min", value: 685 },
	{ name: "11h 30min", value: 690 },
	{ name: "11h 35min", value: 695 },
	{ name: "11h 40min", value: 700 },
	{ name: "11h 45min", value: 705 },
	{ name: "11h 50min", value: 710 },
	{ name: "11h 55min", value: 715 },
	{ name: "12h", value: 720 }
]
