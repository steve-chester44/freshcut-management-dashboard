import React from 'react'
import { useQuery } from '@apollo/react-hooks'
import { Button, Icon, Card } from 'antd'

import PageWithActionBar from '../../../components/PageWithActionBar'

import ServiceGroup from '../components/ServiceGroup/ServiceGroup'
import ServiceGroupModal from '../components/ServiceGroupModal/ServiceGroupModal'

import { serviceGroupQuery } from '../queries'

const ServiceGroupList = () => {
	const { data, loading } = useQuery(serviceGroupQuery)
	if (loading || !data) return null

	if (data.serviceGroups.length === 0) {
		return <div style={{ textAlign: 'center' }}>You haven't created any service groups yet.</div>
	}

	return data.serviceGroups.map(group => (
		<div key={group.id} style={{ paddingBottom: 50 }}>
			<Card bordered={false}>
				<ServiceGroup group={group} />
			</Card>
		</div>
	))
}

const ServiceRoot = () => {
	return (
		<PageWithActionBar
			leftContent={<h1>Services</h1>}
			rightContent={
				<div>
					<ServiceGroupModal
						trigger={({ open }) => {
							return (
								<Button type="primary" onClick={open}>
									<Icon type="plus" /> Add Group
								</Button>
							)
						}}
					/>
				</div>
			}
		>
			<ServiceGroupList />
		</PageWithActionBar>
	)
}

export default ServiceRoot
