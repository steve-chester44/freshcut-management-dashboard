import gql from "graphql-tag"

export const serviceGroupQuery = gql`
	query {
		serviceGroups {
			id
			name
			services {
				id
				name
				sources {
					id
					serviceId,
					name
					type
					price
					duration
				}
				employees {
					id
					firstName
					lastName
				}
			}
		}
	}
`
