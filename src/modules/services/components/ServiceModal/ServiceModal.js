import React from 'react'
import omit from 'lodash/omit'
import * as Yup from 'yup'
import { withFormik } from 'formik'
import { useMutation } from '@apollo/react-hooks'
import { Modal, Tabs, message, Button, Popconfirm, Icon } from 'antd'

import ServiceDetailsForm from './ServiceDetailsForm'
import ServiceEmployeesForm from './ServiceEmployeesForm'
import { upsertServiceMutation, deleteServiceMutation } from '../../mutations'

import { serviceGroupQuery } from '../../queries'

import ServicePricingForm from './ServicePricingForm'

import createDurations from '../../utils/durations'
const durations = createDurations(6)

const initialFormValues = {
	name: '',
	employees: [],
	sources: [
		{
			id: 1,
			price: 0,
			duration: 0
		}
	]
}

const omitTypename = obj => omit(obj, ['__typename'])

const ServiceModal = ({ service, visible = true, serviceGroupId, onClose, ...props }) => {
	const [deleteService, { loading: deleteLoading }] = useMutation(deleteServiceMutation)
	const [upsertService, { loading: upsertLoading }] = useMutation(upsertServiceMutation)
	const isLoading = deleteLoading || upsertLoading

	const handleSubmit = async () => {
		await upsertService({
			variables: {
				// TODO: How to handle `Source`
				input: {
					serviceGroupId,
					...props.values,
					employees: props.values.employees.map(emp => emp.id),
					sources: props.values.sources.map(omitTypename),
					__typename: undefined
				}
			},
			update: (proxy, { data }) => {
				if (service) return

				const { serviceGroups } = proxy.readQuery({ query: serviceGroupQuery })

				proxy.writeQuery({
					query: serviceGroupQuery,
					data: {
						serviceGroups: serviceGroups.map(serviceGroup => {
							if (serviceGroup.id !== serviceGroupId) return serviceGroup

							return {
								...serviceGroup,
								services: serviceGroup.services.concat([data.upsertService])
							}
						})
					}
				})
			}
		})

		onClose()
		message.success(service ? 'Service updated' : 'Service created')
	}

	const handleDelete = async () => {
		await deleteService({
			variables: {
				id: props.values.id
			},
			update: proxy => {
				const { serviceGroups } = proxy.readQuery({ query: serviceGroupQuery })

				proxy.writeQuery({
					query: serviceGroupQuery,
					data: {
						serviceGroups: serviceGroups.map(group => {
							if (group.id !== serviceGroupId) return group

							return {
								...group,
								services: group.services.filter(({ id }) => id !== service.id)
							}
						})
					}
				})
			}
		})

		onClose()
		message.success('Service deleted')
	}

	return (
		<Modal
			width="100%"
			style={{ maxWidth: 650 }}
			onCancel={onClose}
			footer={
				<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
					{props.values.id ? (
						<Popconfirm title="Are you sure you want to delete this service?" onConfirm={handleDelete}>
							<Button disabled={isLoading} type="danger">
								Delete
							</Button>
						</Popconfirm>
					) : (
						<span />
					)}

					<div>
						<Button onClick={onClose}>Cancel</Button>
						<Button loading={isLoading} type="primary" disabled={!props.isValid} onClick={handleSubmit}>
							{service ? 'Update' : 'Save'}
						</Button>
					</div>
				</div>
			}
			visible={visible}
			title={service ? 'Edit Service' : 'Create Service'}
		>
			<Tabs style={{ marginTop: 0, paddingTop: 0 }} defaultActiveKey="details">
				<Tabs.TabPane
					tab={
						<span>
							<Icon type="idcard" />
							<span>Details</span>
						</span>
					}
					key="details"
				>
					<ServiceDetailsForm durations={durations} />
				</Tabs.TabPane>

				<Tabs.TabPane
					tab={
						<span>
							<Icon type="user" />
							<span>Staff</span>
						</span>
					}
					key="staff"
				>
					<ServiceEmployeesForm />
				</Tabs.TabPane>

				<Tabs.TabPane
					tab={
						<span>
							<Icon type="dollar" />
							<span>Additional Pricing</span>
						</span>
					}
					key="pricing"
				>
					<ServicePricingForm durations={durations} />
				</Tabs.TabPane>
			</Tabs>
		</Modal>
	)
}

const validationSchema = Yup.object().shape({
	name: Yup.string()
		.min(1)
		.required('Name is a required field'),

	sources: Yup.array().of(
		Yup.object().shape({
			name: Yup.string(),
			type: Yup.mixed().oneOf(['default', 'onlineappointment', 'onlinecheckin']),
			duration: Yup.number()
				.positive()
				.min(0)
				.required('A duration is required.'),
			price: Yup.number()
				.positive()
				.min(0)
				.required('A price is required')
		})
	)
})

export default withFormik({
	enableReinitialize: true,
	mapPropsToValues: props => {
		if (props.service && props.service.sources.length === 0) {
			return {
				...props.service,
				sources: initialFormValues.sources
			}
		}

		return props.service || initialFormValues
	},
	validationSchema
})(ServiceModal)
