import React from 'react'
import { connect } from 'formik'
import { Form, Input, Select, Row, Col } from 'antd'

const ServiceForm = ({ formik, durations }) => {
	const { values, touched, errors, handleBlur, handleChange } = formik

	const handleDuration = value => {
		formik.setFieldValue('sources[0].duration', value)
	}

	console.log(values)

	return (
		<div>
			<Form.Item
				label="Service Name"
				colon={false}
				hasFeedback={touched?.name && !!errors?.name}
				validateStatus={!touched?.name ? '' : !errors?.name ? 'success' : 'error'}
				help={touched?.name && !!errors?.name && 'A valid name is required.'}
			>
				<Input
					size="large"
					placeholder="Hair"
					type="text"
					value={values.name}
					name="name"
					onChange={handleChange}
					onBlur={handleBlur}
				/>
			</Form.Item>

			<Row gutter={16}>
				<Col xs={24} md={12}>
					<Form.Item
						label="Price"
						colon={false}
						hasFeedback={touched?.sources?.[0]?.price && !!errors?.sources?.[0]?.price}
						validateStatus={!touched?.sources?.[0]?.price ? '' : !errors?.sources?.[0]?.price ? 'success' : 'error'}
						help={touched?.sources?.[0]?.price && !!errors?.sources?.[0]?.price && 'A valid price is required.'}
					>
						<Input
							addonBefore="$"
							size="large"
							placeholder="25.00"
							type="number"
							name="sources[0].price"
							value={values.sources?.[0]?.price}
							onBlur={handleBlur}
							onChange={handleChange}
						/>
					</Form.Item>
				</Col>

				<Col xs={24} md={12}>
					<Form.Item label="Duration" colon={false}>
						<Select
							style={{ width: '100%' }}
							size="large"
							name="sources[0].duration"
							value={values.sources?.[0]?.duration}
							onChange={handleDuration}
						>
							{durations.map(duration => {
								return (
									<Select.Option value={duration.value} key={duration.value}>
										{duration.name}
									</Select.Option>
								)
							})}
						</Select>
					</Form.Item>
				</Col>
			</Row>
		</div>
	)
}

export default connect(ServiceForm)
