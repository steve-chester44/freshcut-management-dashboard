import React from 'react'
import { connect } from 'formik'
import styled from 'styled-components'

import { employeesQuery } from '../../../employees/queries'
import { useQuery } from 'react-apollo'
import { Spin, Switch } from 'antd'

const Employee = styled('div')`
	margin: 10px 0;
	padding: 10px 0;
	border-bottom: 1px solid rgba(249, 249, 249, 1);

	span {
		font-size: 20px;
		padding-left: 8px;
	}
`

const ServiceEmployeesForm = ({ formik }) => {
	const checked = React.useMemo(() => {
		return formik.values.employees.reduce((acc, employee) => {
			acc[employee.id] = true
			return acc
		}, {})
	}, [formik.values.employees])

	const { data, loading } = useQuery(employeesQuery)

	return (
		<div>
			<p style={{ paddingTop: 20, opacity: 0.6, textAlign: 'center' }}>Select staff who can perform this service.</p>

			{loading && <Spin />}
			{data.employees &&
				data.employees.map(employee => (
					<Employee key={employee.id}>
						<Switch
							onChange={checked => {
								formik.setFieldValue(
									'employees',
									checked
										? formik.values.employees.concat([employee])
										: formik.values.employees.filter(emp => emp.id !== employee.id)
								)
							}}
							checked={!!checked[employee.id]}
						/>
						<span>
							{employee.firstName} {employee.lastName}
						</span>
					</Employee>
				))}
			{}
		</div>
	)
}

export default connect(ServiceEmployeesForm)
