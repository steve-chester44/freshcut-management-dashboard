import React from 'react'
import { connect } from 'formik'
import styled from 'styled-components'
import { Form, Input, Row, Col, Select } from 'antd'

const Source = styled('div')`
	border-bottom: 1px solid rgba(232, 232, 232, 1);
	margin-bottom: 24px;
	padding-bottom: 7px;
`

const SOURCES = [{ id: 2, name: 'Online Appointments' }, { id: 3, name: 'Online Checkins' }]

const ServicePricingForm = ({ formik, durations }) => {
	const { touched, errors } = formik

	return (
		<div>
			<p style={{ opacity: 0.6, textAlign: 'center' }}>
				Optionally override the default price and duration for online services.
			</p>

			{SOURCES.map((source, index) => {
				// fake the values as the Default values if none set

				const touchedVals = touched?.sources?.[index + 1]
				const errorVals = errors?.sources?.[index + 1]

				const price = formik.values.sources?.[index + 1]?.price
				const duration = formik.values.sources?.[index + 1]?.duration

				return (
					<Source key={index}>
						<h3>{source.name}</h3>
						<Row gutter={8}>
							<Col xs={24} md={12}>
								<Form.Item
									label="Price"
									colon={false}
									hasFeedback={touchedVals?.price && !!errorVals?.price}
									validateStatus={!touchedVals?.price ? '' : !errorVals?.price ? 'success' : 'error'}
									help={touchedVals?.price && !!errorVals?.price && 'A valid price is required.'}
								>
									<Input
										addonBefore="$"
										type="number"
										size="large"
										name={`sources[${index + 1}].price`}
										value={price}
										onBlur={formik.handleBlur}
										onChange={({ target: { name, value } }) => {
											formik.setFieldValue(name, value)
											formik.setFieldValue(`sources[${index + 1}].id`, index + 2)
											formik.setFieldTouched(name)
										}}
									/>
								</Form.Item>
							</Col>

							<Col xs={24} md={12}>
								<Form.Item label="Duration" colon={false}>
									<Select
										style={{ width: '100%' }}
										size="large"
										name={`sources[${index + 1}].duration`}
										value={duration}
										onChange={value => {
											formik.setFieldValue(`sources[${index + 1}].duration`, value)
											formik.setFieldValue(`sources[${index + 1}].id`, index + 2)
											formik.setFieldTouched(`sources[${index + 1}].duration`)
										}}
									>
										{durations.map(duration => {
											return (
												<Select.Option value={duration.value} key={duration.value}>
													{duration.name}
												</Select.Option>
											)
										})}
									</Select>
								</Form.Item>
							</Col>
						</Row>
					</Source>
				)
			})}
		</div>
	)
}

export default connect(ServicePricingForm)
