import React, { useState } from 'react'
import styled from 'styled-components'

import { Button, Table } from 'antd'
import GroupDropdown from './GroupDropdown'
import ServiceModal from '../ServiceModal/ServiceModal'
import ServiceGroupModal from '../ServiceGroupModal/ServiceGroupModal'

const Header = styled('div')`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding-bottom: 7px;

	h3 {
		margin: 0;
	}
`

const formatDuration = duration => {
	const hours = Math.floor(duration / 60)
	const minuteFragment = duration % 60

	if (duration === 0) return `No Duration`

	return `${hours >= 1 ? `${hours}h` : ''} ${minuteFragment !== 0 ? `${minuteFragment} minutes` : ''}`
}

const ServiceGroup = ({ group }) => {
	const [serviceModal, setServiceModalState] = useState({ visible: false, service: undefined })
	const [groupModal, setGroupModalState] = useState({ visible: false })

	const columns = [
		{
			title: 'Name',
			key: 'name',
			dataIndex: 'name'
		},
		{
			title: 'Duration',
			dataIndex: 'sources[0].duration',
			render: duration => formatDuration(duration)
		},
		{
			title: 'Price',
			dataIndex: 'sources[0].price',
			render: price => {
				return `$${price}`
			}
		},
		{
			key: 'action',
			align: 'right',
			render: service => <Button onClick={() => setServiceModalState({ visible: true, service })}>Edit</Button>
		}
	]

	return (
		<div className="page-load-fade">
			{serviceModal.visible && (
				<ServiceModal
					service={serviceModal.service}
					serviceGroupId={group.id}
					onClose={() => setServiceModalState({ visible: false, service: undefined })}
				/>
			)}

			{groupModal.visible && <ServiceGroupModal group={group} visible={true} />}
			<Header>
				<h3>{group.name}</h3>
				<div style={{ display: 'flex' }}>
					<Button
						style={{ marginRight: 7 }}
						onClick={() => setServiceModalState({ visible: true, service: undefined })}
					>
						Add Service
					</Button>

					<GroupDropdown onEdit={() => setGroupModalState({ visible: true })} group={group} />
				</div>
			</Header>

			{group.services.length > 0 ? (
				<Table rowKey="id" pagination={group.services.length > 10} dataSource={group.services} columns={columns} />
			) : (
				<p style={{ opacity: 0.7, textAlign: 'center', paddingTop: 20 }}>
					This group does not have any services assigned to it.
				</p>
			)}
		</div>
	)
}

export default ServiceGroup
