import React from 'react'

import { Button, Icon, Popconfirm, Menu, Dropdown } from 'antd'

import { serviceGroupQuery } from '../../queries'
import { deleteServiceGroupMutation } from '../../mutations'
import { useMutation } from '@apollo/react-hooks'

const GroupDropdown = ({ group, onEdit }) => {
	const [deleteServiceGroup] = useMutation(deleteServiceGroupMutation)
	return (
		<Dropdown
			overlay={
				<Menu>
					<Menu.Item onClick={onEdit}>
						<Icon style={{ marginRight: 5, opacity: '0.3' }} type="edit" /> Edit Group
					</Menu.Item>
					<Menu.Divider />

					<Menu.Item>
						<Popconfirm
							title="Are you sure delete this group?"
							placement="bottomRight"
							onConfirm={async () => {
								deleteServiceGroup({
									variables: { id: group.id },
									update: (cache, { data }) => {
										if (!data || !data.deleteServiceGroup) return

										const { serviceGroups } = cache.readQuery({ query: serviceGroupQuery })

										cache.writeQuery({
											query: serviceGroupQuery,
											data: { serviceGroups: serviceGroups.filter(sg => sg.id !== group.id) }
										})
									}
								})
							}}
							onCancel={() => null}
							okText="Delete"
							cancelText="Cancel"
						>
							<span>
								<Icon style={{ marginRight: 5, opacity: '0.3' }} type="delete" /> Delete Group
							</span>
						</Popconfirm>
					</Menu.Item>
				</Menu>
			}
			placement="bottomRight"
		>
			<Button>
				<Icon type="ellipsis" />
			</Button>
		</Dropdown>
	)
}

export default GroupDropdown
