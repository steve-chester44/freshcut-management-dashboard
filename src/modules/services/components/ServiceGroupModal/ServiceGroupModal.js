import React, { Fragment, PureComponent } from 'react'
import { Modal, Form, Button, Input, message } from 'antd'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

import { serviceGroupQuery } from '../../queries'
import { displayErrors } from '../../../../graphql/utils'

const initialState = {
	isSubmitting: false,
	isModalVisible: false,
	fields: {
		name: ''
	}
}

const UPDATE_OR_CREATE = gql`
	mutation upsertServiceGroup($input: ServiceGroupInput!) {
		upsertServiceGroup(input: $input) {
			group {
				id
				name
				services {
					id
					name
					sources {
						id
						name
						type
						price
						duration
						serviceId
					}
				}
			}
		}
	}
`

class ServiceGroupModal extends PureComponent {
	state = { ...initialState }

	constructor(props) {
		super(props)

		this.state = {
			...initialState,
			isModalVisible: props.visible,
			isEditing: !!props.group,
			fields: props.group ? { ...props.group } : initialState.fields
		}
	}

	handleOpen = () => {
		this.setState({
			isModalVisible: true,
			isEditing: !!this.props.group,
			fields: this.props.group ? { ...this.props.group } : initialState.fields
		})
	}

	handleClose = () => {
		if (this.props.onClose) {
			this.props.onClose()
		} else {
			this.setState({ ...initialState })
		}
	}

	handleInputChange = ({ target: { name, value } }) =>
		this.setState({ fields: { ...this.state.fields, [name]: value } })

	onUpsert = (
		cache,
		{
			data: {
				upsertServiceGroup: { group }
			}
		}
	) => {
		const { serviceGroups } = cache.readQuery({ query: serviceGroupQuery })

		cache.writeQuery({
			query: serviceGroupQuery,
			data: {
				serviceGroups: this.state.isEditing
					? serviceGroups.map(sg => (sg.id === group.id ? group : sg))
					: serviceGroups.concat([group])
			}
		})
	}

	_renderFooter = () => {
		const { isSubmitting, fields, isEditing } = this.state

		return (
			<div style={{ padding: 10 }}>
				<Button onClick={this.handleClose}>Cancel</Button>
				<Mutation update={this.onUpsert} mutation={UPDATE_OR_CREATE}>
					{(upsertServiceGroup, { data }) => {
						return (
							<Button
								disabled={fields.name.length === 0}
								loading={isSubmitting}
								onClick={async () => {
									this.setState({ isSubmitting: true })

									try {
										await upsertServiceGroup({
											variables: {
												input: {
													id: fields.id,
													name: fields.name
												}
											}
										})

										message.success(isEditing ? 'Group updated' : 'Group created')
										this.handleClose()
									} catch (error) {
										this.setState({ isSubmitting: false })
										displayErrors(error)
									}
								}}
								type="primary"
							>
								{isEditing ? 'Update' : 'Create'}
							</Button>
						)
					}}
				</Mutation>
			</div>
		)
	}

	render() {
		const { fields, isModalVisible } = this.state

		return (
			<Fragment>
				{isModalVisible && (
					<Modal
						maskClosable={false}
						onCancel={() => this.setState({ ...initialState })}
						visible={isModalVisible}
						footer={this._renderFooter()}
					>
						<div style={{ paddingTop: 10 }}>
							<Form.Item label="Group Name" colon={false}>
								<Input type="text" name="name" size="large" value={fields.name} onChange={this.handleInputChange} />
							</Form.Item>
						</div>
					</Modal>
				)}

				{this.props.trigger && this.props.trigger({ open: this.handleOpen })}
			</Fragment>
		)
	}
}

export default ServiceGroupModal
