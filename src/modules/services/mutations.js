import gql from 'graphql-tag'

export const upsertServiceMutation = gql`
	mutation upsertService($input: UpsertServiceInput!) {
		upsertService(input: $input) {
			id
			name
			sources {
				id
				name
				type
				price
				duration
				serviceId
			}
			employees {
				id
				firstName
				lastName
			}
		}
	}
`

export const deleteServiceMutation = gql`
	mutation deleteService($id: ID!) {
		deleteService(id: $id)
	}
`

export const deleteServiceGroupMutation = gql`
	mutation DeleteGroup($id: ID!) {
		deleteServiceGroup(id: $id)
	}
`
