import gql from 'graphql-tag'

export const setAccountPasswordMutation = gql`
	mutation resetPassword($token: String!, $password: String!) {
		resetPassword(token: $token, password: $password)
	}
`
export const changeUserPasswordMutation = gql`
	mutation changePassword($currentPassword: String!, $newPassword: String!) {
		changePassword(currentPassword: $currentPassword, newPassword: $newPassword)
	}
`

export const forgotPasswordMutation = gql`
	mutation forgotPassword($email: String!) {
		forgotPassword(email: $email)
	}
`