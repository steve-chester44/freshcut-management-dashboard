export const AUTH_LOGIN = "/login"
export const AUTH_REGISTER = "/register"
export const AUTH_RESET_PASSWORD = "/forgot-password"
export const AUTH_CREATE_PASSWORD  = "/confirm-account"