import React from 'react'
import { Modal, Form, Input, Button, message } from 'antd'
import { useMutation } from 'react-apollo'

import { changeUserPasswordMutation } from '../../mutations'

const ChangePasswordModal = ({ onClose }) => {
	const [updatePassword, { loading }] = useMutation(changeUserPasswordMutation)

	const [fields, setFields] = React.useState({
		currentPassword: '',
		password: '',
		confirmPassword: ''
	})

	const handleChange = ({ target: { name, value } }) => setFields(prev => ({ ...prev, [name]: value }))

	const handleSubmit = async () => {
		try {
			const { data } = await updatePassword({
				variables: {
					currentPassword: fields.currentPassword,
					newPassword: fields.password
				}
			})

			if (data.changePassword) {
				message.success('Password changed')
				onClose()
			}
		} catch (e) {
			console.log(e)
		}
	}

	return (
		<Modal
			onCancel={onClose}
			footer={
				<div>
					<Button onClick={onClose}>Cancel</Button>
					<Button
						type="primary"
						onClick={handleSubmit}
						loading={loading}
						disabled={
							fields.currentPassword.trim().length < 3 ||
							fields.password.trim().length < 3 ||
							fields.password !== fields.confirmPassword
						}
					>
						Update
					</Button>
				</div>
			}
			visible={true}
			width="500px"
			title="Change Password"
		>
			<Form.Item label="Current Password" colon={false} required={true}>
				<Input type="password" onChange={handleChange} name="currentPassword" />
			</Form.Item>

			<Form.Item label="New Password" colon={false} required={true}>
				<Input type="password" onChange={handleChange} name="password" />
			</Form.Item>

			<Form.Item label="Confirm New Password" colon={false} required={true}>
				<Input type="password" onChange={handleChange} name="confirmPassword" />
			</Form.Item>
		</Modal>
	)
}

export default ChangePasswordModal
