import React from 'react'
import { Modal, Form, Input, Button, message } from 'antd'
import { useMutation } from 'react-apollo'

import { forgotPasswordMutation } from '../../mutations'

const ForgotPasswordModal = ({ email, onClose }) => {
	const [resetPassword, { loading }] = useMutation(forgotPasswordMutation)

	const [fields, setFields] = React.useState({ email })

	const handleChange = ({ target: { name, value } }) => setFields(prev => ({ ...prev, [name]: value }))

	const handleSubmit = async () => {
		try {
			const { data } = await resetPassword({
				variables: {
					email: fields.email
				}
			})

			if (data.forgotPassword) {
				onClose({ showAlert: true })
			}
		} catch (err) {
			console.log(err)
		}
	}

	return (
		<Modal
			onCancel={onClose}
			footer={
				<div>
					<Button onClick={onClose}>Cancel</Button>
					<Button type="primary" onClick={handleSubmit} loading={loading} disabled={fields.email.trim().length < 3}>
						Reset Password
					</Button>
				</div>
			}
			visible={true}
			width="500px"
			title="Reset Password"
		>
			<p>Enter the email address tied to your account and we will email you instructions on how to reset your password.</p>
			<Form.Item label="Email Address" colon={false} required={true}>
				<Input type="email" onChange={handleChange} name="email" />
			</Form.Item>
		</Modal>
	)
}

export default ForgotPasswordModal
