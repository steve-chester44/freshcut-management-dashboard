import React from 'react'
import { Link } from 'react-router-dom'

import useAuth from '../../../common/hooks/useAuth'
import { AUTH_LOGIN } from '../utils/urls'

import { Col, Card, Form, Icon, Input, Button, Row } from 'antd'
import styled from 'styled-components'

const FormItem = Form.Item

const styles = {
	icon: {
		color: 'rgba(32,32,32,.5)',
		marginRight: '5px'
	},
	card: { width: '100%' }
}

const Wrapper = styled('div')`
	width: 100%;
	height: 100%;

	.heading {
		text-transform: uppercase;
		text-align: center;
		font-size: 22px;
	}

	.footer {
		text-align: center;
		margin: 20px auto;
		color: rgba(50, 66, 151, 1);

		a {
			color: rgba(50, 66, 151, 1);
			font-weight: 700;
			text-decoration: underline;
		}
	}

	.logo {
		color: rgba(50, 66, 151, 1);
		font-family: jaf-domus;
		font-weight: 700;
		font-size: 40px;
		line-height: 1;
		padding: 0;
		text-align: center;
		text-shadow: 1px 2px 3px rgba(32, 32, 32, 0.1);
		margin-top: 20px;
		text-transform: uppercase;
	}

	.main-button {
		width: 100%;
		margin: 15px 0;
		background: rgba(88, 192, 131, 1);
		border: 0;

		&:disabled {
			background: rgba(240, 240, 240, 1);
		}

		&:not(:disabled):hover {
			background: #36a360;
		}
	}

	@media (min-width: 768px) {
		.login-row {
			height: 100%;
		}

		.footer {
			text-shadow: 1px 1px rgba(32, 32, 32, 0.2);
			color: white;

			a {
				color: white;
			}
		}

		.logo {
			color: white;
			text-shadow: 1px 2px 3px rgba(32, 32, 32, 0.2);
			margin-top: 0;
		}

		background-color: rgba(50, 66, 151, 1);
	}
`

const Register = () => {
	const { register, loading } = useAuth()

	const [state, setState] = React.useState({
		fields: {
			firstName: '',
			email: '',
			password: '',
			companyName: ''
		}
	})

	const { firstName, email, password, companyName } = state.fields

	const handleInputChange = ({ target: { name, value } }) => {
		setState(prev => ({ ...prev, fields: { ...prev.fields, [name]: value } }))
	}

	return (
		<Wrapper>
			<Row type="flex" justify="center" align="middle" className="login-row">
				<Col xs={24} md={12} lg={10} xl={7}>
					<h1 className="logo">{process.env.REACT_APP_COMPANY_NAME}</h1>
					<Card style={styles.card} bordered={false}>
						<h1 className="heading">Sign Up</h1>

						<Form>
							<FormItem
								style={styles.formItem}
								label={
									<span>
										<Icon type="mail" style={styles.icon} />
										Email
									</span>
								}
								colon={false}
							>
								<Input name="email" size="large" onChange={handleInputChange} />
							</FormItem>
							<FormItem
								style={styles.formItem}
								label={
									<span>
										<Icon type="lock" style={styles.icon} />
										Password
									</span>
								}
								colon={false}
							>
								<Input name="password" size="large" onChange={handleInputChange} type="password" />
							</FormItem>

							<Row gutter={16}>
								<Col span={12}>
									<FormItem
										style={styles.formItem}
										colon={false}
										label={
											<span>
												<Icon type="user" style={styles.icon} />
												First Name
											</span>
										}
									>
										<Input name="firstName" size="large" onChange={handleInputChange} />
									</FormItem>
								</Col>

								<Col span={12}>
									<FormItem
										style={styles.formItem}
										colon={false}
										label={
											<span>
												<Icon type="user" style={styles.icon} />
												Last Name
											</span>
										}
									>
										<Input name="lastName" size="large" onChange={handleInputChange} />
									</FormItem>
								</Col>
							</Row>
							<Row>
								<Col span={24}>
									<FormItem
										style={styles.formItem}
										colon={false}
										label={
											<span>
												<Icon type="home" style={styles.icon} />
												Company Name
											</span>
										}
									>
										<Input name="companyName" size="large" onChange={handleInputChange} />
									</FormItem>
								</Col>
							</Row>

							<Button
								type="primary"
								size="large"
								className="main-button"
								disabled={email.length < 3 || password.length < 3 || firstName.length < 2 || companyName.length < 2}
								onClick={() => register(state.fields)}
								loading={loading}
							>
								Create Account
							</Button>
						</Form>
					</Card>

					<div className="footer">
						Already have an account? <Link to={AUTH_LOGIN}>Login</Link>
					</div>
				</Col>
			</Row>
		</Wrapper>
	)
}

export default Register
