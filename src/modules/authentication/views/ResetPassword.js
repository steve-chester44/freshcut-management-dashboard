import React from 'react'
import { Form, Input, Button, Row, Col, Card, message } from 'antd'

import styled from 'styled-components'
import { useMutation } from 'react-apollo'
import { setAccountPasswordMutation } from '../mutations'

const Wrapper = styled('div')`
	width: 100%;
	height: 100%;

	.forgot-password {
		text-align: center;
		margin: 10px 0 0 0;
	}

	.heading {
		font-size: 22px;
		display: none;
	}

	.footer {
		text-align: center;
		margin: 20px auto;
		color: rgba(51, 67, 149, 1);

		a {
			color: rgba(51, 67, 149, 1);
			font-weight: 700;
			text-decoration: underline;
		}
	}

	.main-button {
		width: 100%;
		margin: 15px 0;
		background: rgba(88, 192, 131, 1);
		line-height: 1;
		border: 0;

		&:disabled {
			background: rgba(240, 240, 240, 1);
		}

		&:not(:disabled):hover {
			background: #36a360;
		}
	}

	.logo {
		color: rgba(51, 67, 149, 1);
		font-family: jaf-domus;
		font-weight: 700;
		font-size: 40px;
		line-height: 1;
		padding: 0;
		text-align: center;
		text-shadow: 1px 2px 3px rgba(32, 32, 32, 0.05);
		margin-top: 20px;
		text-transform: uppercase;
	}

	@media (min-width: 768px) {
		.login-row {
			height: 100%;
		}

		.heading {
			display: block;
		}

		.footer {
			text-shadow: 1px 1px rgba(32, 32, 32, 0.2);
			color: white;

			a {
				color: white;
			}
		}

		.logo {
			color: white;
			text-shadow: 1px 2px 3px rgba(32, 32, 32, 0.05);
			margin-top: 0;
		}

		background-color: rgba(50, 66, 151, 1);
	}
`

const styles = {
	icon: {
		color: 'rgba(32,32,32,.5)',
		marginRight: '5px'
	}
}

const ResetPassword = ({ token, history }) => {
	const [state, setState] = React.useState({
		fields: {
			password: '',
			confirmPassword: ''
		}
	})

	const [resetPassword, { loading }] = useMutation(setAccountPasswordMutation)

	const handleInput = ({ target: { name, value } }) => {
		setState(prev => ({
			...prev,
			fields: { ...prev.fields, [name]: value }
		}))
	}

	const handleSubmit = async () => {
		try {
			const { data } = await resetPassword({
				variables: {
					token,
					password: state.fields.password
				}
			})

			if (data.resetPassword) {
				message.success('Password reset. Please login to continue')
				history.push('/')
            }
		} catch (err) {
			console.log(err)
		}
	}

	return (
		<Wrapper>
			<Row type="flex" justify="center" align="middle" className="login-row">
				<Col xs={24} md={12} lg={10} xl={6}>
					<h1 className="logo">{process.env.REACT_APP_COMPANY_NAME}</h1>
					<Card style={{ width: '100%' }} bordered={false}>
						<h1 className="heading">Set Password</h1>

						<Form>
							<Form.Item colon={false} label="Password">
								<Input name="password" type="password" size="large" onChange={handleInput} />
							</Form.Item>

							<Form.Item colon={false} label="Confirm Password">
								<Input name="confirmPassword" type="password" size="large" onChange={handleInput} />
							</Form.Item>

							<Button
								htmlType="submit"
								type="primary"
								size="large"
								className="main-button"
								onClick={handleSubmit}
								loading={loading}
								disabled={
									state.fields.password.trim().length < 3 || state.fields.password !== state.fields.confirmPassword
								}
							>
								Reset Password
							</Button>
						</Form>
					</Card>
				</Col>
			</Row>
		</Wrapper>
	)
}

export default ResetPassword
