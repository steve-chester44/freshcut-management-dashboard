import React from 'react'
import { Link } from 'react-router-dom'
import { Form, Icon, Input, Button, Row, Col, Card, Alert } from 'antd'

import { AUTH_REGISTER } from '../utils/urls'
import { useAuth } from '../../../common/hooks'
import styled from 'styled-components'

const ForgotPasswordForm = React.lazy(() => import('../components/ForgotPasswordModal'))

const Wrapper = styled('div')`
	width: 100%;
	height: 100%;

	.forgot-password {
		text-align: center;
		margin: 10px 0 0 0;
	}

	.heading {
		text-align: center;
		font-size: 22px;
		text-transform: uppercase;
		display: none;
	}

	.footer {
		text-align: center;
		margin: 20px auto;
		color: rgba(51, 67, 149, 1);

		a {
			color: rgba(51, 67, 149, 1);
			font-weight: 700;
			text-decoration: underline;
		}
	}

	.main-button {
		width: 100%;
		margin: 15px 0;
		background: rgba(88, 192, 131, 1);
		line-height: 1;
		border: 0;

		&:disabled {
			background: rgba(240, 240, 240, 1);
		}

		&:not(:disabled):hover {
			background: #36a360;
		}
	}

	.logo {
		color: rgba(51, 67, 149, 1);
		font-family: jaf-domus;
		font-weight: 700;
		font-size: 40px;
		line-height: 1;
		padding: 0;
		text-align: center;
		text-shadow: 1px 2px 3px rgba(32, 32, 32, 0.05);
		margin-top: 20px;
		text-transform: uppercase;
	}

	@media (min-width: 768px) {
		.login-row {
			height: 100%;
		}

		.heading {
			display: block;
		}

		.footer {
			text-shadow: 1px 1px rgba(32, 32, 32, 0.2);
			color: white;

			a {
				color: white;
			}
		}

		.logo {
			color: white;
			text-shadow: 1px 2px 3px rgba(32, 32, 32, 0.05);
			margin-top: 0;
		}

		background-color: rgba(50, 66, 151, 1);
	}
`

const styles = {
	icon: {
		color: 'rgba(32,32,32,.5)',
		marginRight: '5px'
	}
}

const Login = () => {
	const [forgotPassword, setForgotPassword] = React.useState({ visible: false, showAlert: false })
	const [state, setState] = React.useState({
		fields: {
			email: '',
			password: ''
		}
	})

	const handleInput = ({ target: { name, value } }) => {
		setState(prev => ({
			...prev,
			fields: { ...prev.fields, [name]: value }
		}))
	}

	const { email, password } = state.fields
	const { login, loading } = useAuth()

	return (
		<Wrapper>
			<React.Suspense fallback={null}>
				{forgotPassword.visible && (
					<ForgotPasswordForm
						email={email}
						onClose={cb => {
							setForgotPassword({ visible: false, showAlert: cb && cb.showAlert })
						}}
					/>
				)}
			</React.Suspense>
			<Row type="flex" justify="center" align="middle" className="login-row">
				<Col xs={24} md={12} lg={10} xl={forgotPassword.showAlert ? 8 : 6}>
					<h1 className="logo">{process.env.REACT_APP_COMPANY_NAME}</h1>
					<Card style={{ width: '100%' }} bordered={false}>
						<h1 className="heading">Log In</h1>
						{forgotPassword.showAlert && (
							<Alert
								message={
									'If there is an account associated with this e-mail address, then the password reset link was sent to it.'
								}
								description={'Follow the link in the email we sent to reset your password.'}
							/>
						)}
						<Form>
							<Form.Item
								colon={false}
								label={
									<span>
										<Icon type="mail" style={styles.icon} />
										Email
									</span>
								}
							>
								<Input name="email" size="large" onChange={handleInput} />
							</Form.Item>
							<Form.Item
								colon={false}
								label={
									<span>
										<Icon type="lock" style={styles.icon} />
										Password
									</span>
								}
							>
								<Input name="password" size="large" onChange={handleInput} type="password" />
							</Form.Item>
							<Button
								htmlType="submit"
								type="primary"
								size="large"
								className="main-button"
								onClick={() => login(state.fields)}
								loading={loading}
								disabled={email.length < 3 || password.length < 3}
							>
								Login
							</Button>

							<div className="forgot-password">
								<Button onClick={() => setForgotPassword({ visible: true, showAlert: false })} type="link">
									Forgot password?
								</Button>
							</div>
						</Form>
					</Card>

					<div className="footer">
						Don't have an account? <Link to={AUTH_REGISTER}>Sign Up</Link>
					</div>
				</Col>
			</Row>
		</Wrapper>
	)
}

export default Login
