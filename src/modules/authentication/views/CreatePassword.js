import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { useMutation } from 'react-apollo'

import { AUTH_LOGIN } from '../utils/urls'
import { Col, Card, Form, Icon, Input, Button, Row, message } from 'antd'

import { setAccountPasswordMutation } from '../mutations'

const styles = {
	icon: {
		color: 'rgba(32,32,32,.5)',
		marginRight: '5px'
	},
	card: { width: '100%' }
}

const Wrapper = styled('div')`
	width: 100%;
	height: 100%;

	.heading {
		text-align: center;
	}

	.footer {
		text-align: center;
		margin: 20px auto;
		color: rgba(46, 148, 249, 1);

		a {
			color: rgba(46, 148, 249, 1);
			font-weight: 700;
			text-decoration: underline;
		}
	}

	.logo {
		color: rgba(76, 102, 243, 1);
		font-family: jaf-domus;
		font-weight: 700;
		font-size: 40px;
		line-height: 1;
		padding: 0;
		text-align: center;
		text-shadow: 1px 2px 3px rgba(32, 32, 32, 0.1);
		margin-top: 20px;
		text-transform: capitalize;
	}

	.main-button {
		width: 100%;
		margin: 15px 0;
		background: #36b360;
		border: 0;

		&:disabled {
			background: rgba(240, 240, 240, 1);
		}

		&:not(:disabled):hover {
			background: #36a360;
		}
	}

	@media (min-width: 768px) {
		.login-row {
			height: 100%;
		}

		.footer {
			text-shadow: 1px 1px rgba(32, 32, 32, 0.2);
			color: white;

			a {
				color: white;
			}
		}

		.logo {
			color: white;
			text-shadow: 1px 2px 3px rgba(32, 32, 32, 0.2);
			margin-top: 0;
		}

		background-color: rgba(50, 66, 151, 1);
	}
`

const CreatePassword = ({ token, history }) => {
	const [fields, setFields] = React.useState({
		password: '',
		confirmPassword: ''
	})

	const handleInputChange = ({ target: { name, value } }) => setFields(prev => ({ ...prev, [name]: value }))

	const [createPassword, { loading }] = useMutation(setAccountPasswordMutation)

	const setAccountPassword = async () => {
		try {
			const { data } = await createPassword({
				variables: {
					token,
					password: fields.password
				}
			})

			if (data.resetPassword) {
				message.success('Success! time to log in')
				history.push('/')
			}
		} catch (error) {
			if (error.graphQLErrors) {
				const invalidToken = error.graphQLErrors.some(error => !!error?.data?.isInvalidToken)

				if (invalidToken) {
					history.push('/')
				}
			}
		}
	}

	return (
		<Wrapper>
			<Row type="flex" justify="center" align="middle" className="login-row">
				<Col xs={24} md={12} lg={6}>
					<h1 className="logo">{process.env.REACT_APP_COMPANY_NAME}</h1>
					<Card style={styles.card} bordered={false}>
						<div className="heading">
							<h3>Set a password to finish creating your account.</h3>
						</div>

						<Form>
							<Form.Item
								style={styles.formItem}
								required={true}
								label={
									<span>
										<Icon type="lock" style={styles.icon} />
										Password
									</span>
								}
								colon={false}
							>
								<Input name="password" size="large" onChange={handleInputChange} type="password" />
							</Form.Item>

							<Form.Item
								style={styles.formItem}
								required={true}
								label={
									<span>
										<Icon type="lock" style={styles.icon} />
										Confirm Password
									</span>
								}
								colon={false}
							>
								<Input name="confirmPassword" size="large" onChange={handleInputChange} type="password" />
							</Form.Item>

							<Button
								type="primary"
								size="large"
								className="main-button"
								disabled={fields.password.trim().length < 3 || fields.password !== fields.confirmPassword}
								onClick={() => {
									setAccountPassword({ password: fields.password, token })
								}}
								loading={loading}
							>
								Create Account
							</Button>
						</Form>
					</Card>

					<div className="footer">
						Already have an account? <Link to={AUTH_LOGIN}>Login</Link>
					</div>
				</Col>
			</Row>
		</Wrapper>
	)
}

export default CreatePassword
