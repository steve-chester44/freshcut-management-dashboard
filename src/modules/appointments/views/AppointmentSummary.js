import React from 'react'
import { Link, generatePath } from 'react-router-dom'
import styled from 'styled-components'
import format from 'date-fns/format'

import { message, Popconfirm, Row, Col, Icon, Dropdown, Menu, Button, Modal } from 'antd'

import Customer from '../components/Customer'

import { appointmentQuery, PAYMENT_METHODS } from '../queries'
import { CHECKOUT_MUTATION } from '../mutations'
import { distanceInWordsStrict } from 'date-fns'
import { useMutation, useQuery, useLazyQuery } from '@apollo/react-hooks'
import { EDIT_APPOINTMENT_ROUTE } from '../urls'
import { deleteAppointmentMutation } from '../mutations'

const ServiceSummary = styled('div')`
	display: flex;
	margin-top: 15px;
	padding: 10px 20px;
	background: #f6f8fc;
	border-radius: 10px;

	.left {
		min-height: 48px;
		display: flex;
		align-items: center;
		font-size: 18px;
		font-weight: 600;
		height: 100%;
		padding-right: 10px;

		h3 {
			font-size: 18px;
			line-height: 1;
			margin: 0;
			padding: 0;
		}
	}

	.right {
		h3 {
			font-size: 18px;
			line-height: 1;
			margin: 0;
			padding: 0;
		}

		padding-left: 10px;
		border-left: 1px solid #dedede;
		flex: 1;
		display: flex;
		justify-content: space-between;
		align-items: center;
	}
`

const AppointmentSummary = ({ appointmentId, history, onClose }) => {
	const [paymentMethodsVisible, setPaymentMethodsVisible] = React.useState(false)

	const [doCheckout] = useMutation(CHECKOUT_MUTATION)

	const { loading, data } = useQuery(appointmentQuery, { variables: { id: parseInt(appointmentId, 10) } })

	const [deleteAppointment] = useMutation(deleteAppointmentMutation)

	const [getPaymentMethods, { loading: paymentMethodsLoading, data: { paymentMethods = [] } = {} }] = useLazyQuery(
		PAYMENT_METHODS
	)

	const handlePaymentMethodClick = async (paymentMethodId, appointment) => {
		doCheckout({
			variables: {
				input: {
					paymentMethodId,
					appointmentId: appointment.id,
					paymentAmount: appointment.price
				}
			}
		}).then(() => {
			setPaymentMethodsVisible(false)
		})
	}

	if (loading || !data || !data.appointment) return null

	return (
		<Modal
			width={1000}
			bodyStyle={{ height: 'calc(90vh - 200px)' }}
			visible={true}
			title="Appointment Overview"
			onCancel={onClose}
			footer={
				<div>
					<Dropdown
						placement="topCenter"
						overlay={
							<Menu>
								<Menu.Item>
									<Link
										to={generatePath(EDIT_APPOINTMENT_ROUTE, {
											id: data.appointment.id
										})}
									>
										Edit Appointment
									</Link>
								</Menu.Item>
								<Menu.Divider />
								<Menu.Item>
									<Popconfirm
										title="Are you sure you want to delete this appointment?"
										onConfirm={async () => {
											const deleted = await deleteAppointment({
												variables: {
													id: data.appointment.id
												}
											})

											if (deleted) {
												message.success('Appointment deleted')
												onClose()
											} else {
												message.error('Failed to delete the appointment')
											}
										}}
									>
										<div>Delete Appointment</div>
									</Popconfirm>
								</Menu.Item>
							</Menu>
						}
					>
						<Button size="large" style={{ flex: 1, marginRight: 5 }}>
							More Options <Icon type="down" />
						</Button>
					</Dropdown>

					{!paymentMethodsVisible && data.appointment.status !== 'completed' && (
						<Button
							onClick={() => {
								getPaymentMethods()
								setPaymentMethodsVisible(true)
							}}
							type="primary"
							size="large"
							icon="check"
							loading={paymentMethodsLoading}
						>
							Checkout
						</Button>
					)}
				</div>
			}
		>
			<Row type="flex" gutter={16}>
				<Col className="appointment-form" xs={24} md={14}>
					<Row>
						<Col className="start-date">
							<h2>{format(data.appointment.startTime, 'dddd, MMMM Do, YYYY')}</h2>
						</Col>
					</Row>

					<Row>
						<Col>
							<ServiceSummary>
								<div className="left">
									<h3>{format(data.appointment.startTime, 'h:mma')}</h3>
								</div>
								<div className="right">
									<div>
										<h3>{data.appointment.services[0] && data.appointment.services[0].name}</h3>
										<span>
											{distanceInWordsStrict(data.appointment.startTime, data.appointment.endTime)}
											{data.appointment.employee &&
												` with ${data.appointment.employee.firstName} ${
													data.appointment.employee.lastName ? data.appointment.employee.lastName : ''
												}`}
										</span>
									</div>
									<h3>${data.appointment.price}</h3>
								</div>
							</ServiceSummary>

							<h3 style={{ marginTop: 25 }}>Appointment History</h3>
							<p>Created {format(data.appointment.createdAt, 'dddd, MMMM Do, YYYY - h:mma')}</p>
						</Col>
					</Row>
				</Col>

				<Col xs={24} md={10}>
					<Customer
						actions={[
							{
								title: 'Edit Customer Details',
								onClick: () => {
									history.push(`/customers/${data.appointment.customer.id}`)
								}
							}
						]}
						customer={data.appointment.customer}
					/>

					{paymentMethodsVisible && data.appointment.status !== 'completed' && (
						<div className="main">
							<Row>
								<Col span={24}>
									<div style={{ paddingTop: 20, textAlign: 'center' }}>
										<h3>Total Due: ${data.appointment.price}</h3>
									</div>
								</Col>
							</Row>
							<Row type="flex" justify="center" align="middle">
								{paymentMethods.map(method => (
									<Col span={24} key={`pay-method-${method.id}`} style={{ padding: '10px 20px' }}>
										<Button
											size="large"
											type="primary"
											style={{ width: '100%' }}
											onClick={() => handlePaymentMethodClick(method.id, data.appointment)}
										>
											{method.name}
										</Button>
									</Col>
								))}
							</Row>
						</div>
					)}

					{data.appointment.status === 'completed' && (
						<div className="main">
							<h1 style={{ marginTop: 20, textAlign: 'center', opacity: 0.5, fontSize: 20 }}>Appointment Completed</h1>
						</div>
					)}
				</Col>
			</Row>
		</Modal>
	)
}

export default AppointmentSummary
