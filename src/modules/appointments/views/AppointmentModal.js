import React from 'react'

import differenceInSeconds from 'date-fns/difference_in_seconds'
import { Modal, Row, Col, message } from 'antd'

import AppointmentForm from '../components/AppointmentForm'
import FormButtons from '../components/FormButtons'

import { appointmentQuery } from '../queries'
import { upsertAppointmentMutation } from '../mutations'

import moment from 'moment'
import { useMutation, useQuery, useApolloClient } from '@apollo/react-hooks'
import { calendarDate as getCalendarDate } from '../../calendar/queries'
import parse from 'date-fns/parse'
import { useRouter } from '../../../common/hooks'
import { differenceInMinutes } from 'date-fns'

const CustomerStats = React.lazy(() => import('../components/CustomerStats/CustomerStats'))
const CustomerSelector = React.lazy(() => import('../components/CustomerSelector/CustomerSelector'))

const AppointmentModal = ({
	appointmentId,
	locationId,
	defaultEmployeeId,
	defaultStartTime,
	defaultEndTime,
	onClose
}) => {
	const { location } = useRouter()
	const client = useApolloClient()
	const [upsertAppointment] = useMutation(upsertAppointmentMutation)

	const {
		data: { calendarDate }
	} = useQuery(getCalendarDate)

	const now = moment(parse(calendarDate))
	// remove the seconds, we don't care about seconds.
	const startTime = defaultStartTime ? moment(defaultStartTime) : now.clone().seconds(0)

	const [state, setState] = React.useState({
		loadingFields: false,
		displayDate: startTime,
		selectedTime: startTime,
		fields: {
			locationId,
			startTime,
			endTime: defaultEndTime,
			userId: defaultEmployeeId,
			customerId: undefined,
			duration: defaultStartTime && defaultEndTime ? differenceInMinutes(defaultEndTime, defaultStartTime) : 0,
			price: 0,
			services: []
		}
	})

	const handleSubmit = async () => {
		const { data } = await upsertAppointment({ variables: { input: state.fields } })

		if (data && data.upsertAppointment) {
			message.success(appointmentId ? 'Appointment updated' : 'Appointment created')
			if (onClose) {
				onClose()
			}
		}
	}

	React.useEffect(() => {
		if (!appointmentId) return

		client
			.query({
				variables: {
					id: parseInt(appointmentId, 10)
				},
				query: appointmentQuery
			})
			.then(({ data: { appointment } }) => {
				setState(prevState => ({
					...prevState,
					displayDate: moment(appointment.startTime),
					selectedTime: moment(appointment.startTime),
					loadingFields: false,
					fields: {
						id: appointment.id,
						locationId: appointment.locationId,
						userId: appointment.employee ? appointment.employee.id : undefined,
						customerId: appointment.customer ? appointment.customer.id : undefined,
						startTime: moment(appointment.startTime),
						endTime: moment(appointment.endTime),
						duration: appointment.duration,
						price: appointment.price,
						services: appointment.services.map(service => service.id)
					}
				}))
			})
	}, [appointmentId, client])

	const setFieldValue = (key, value) => setState(prev => ({ ...prev, fields: { ...prev.fields, [key]: value } }))

	const handleServicesSelection = (serviceId, opt) => {
		setState(prevState => ({
			...prevState,
			fields: {
				...prevState.fields,
				price: opt.props['data-price'] || 0,
				duration: opt.props['data-duration'] || 0,
				services: [serviceId],
				endTime: moment(prevState.fields.startTime).add(opt.props['data-duration'] || 0, 'minutes')
			}
		}))
	}

	const handleTimeSelection = time => {
		const diff = differenceInSeconds(moment(time, 'h:mma'), state.fields.startTime)

		const startTime = moment(state.fields.startTime).add(diff + 1, 'seconds')

		setState(prevState => ({
			...prevState,
			selectedTime: startTime,
			fields: {
				...prevState.fields,
				startTime: startTime,
				endTime: startTime.clone().add(prevState.fields.duration, 'minutes')
			}
		}))
	}

	const handleDateSelection = momentObj => {
		const diff = differenceInSeconds(momentObj, state.fields.startTime)

		const startTime = moment(state.fields.startTime).add(diff + 1, 'seconds')

		setState(prevState => ({
			...prevState,
			isDatePickerVisible: false,
			displayDate: momentObj,
			selectedTime: startTime,
			fields: {
				...prevState.fields,
				startTime: startTime,
				endTime: startTime.clone().add(prevState.fields.duration, 'minutes')
			}
		}))
	}

	if (state.loadingFields) return null

	return (
		<Modal
			bodyStyle={{ height: 'calc(90vh - 200px)' }}
			visible={true}
			title={appointmentId ? `Edit Appointment` : `New Appointment`}
			onCancel={onClose}
			width={1000}
			footer={
				<FormButtons
					loading={state.isSubmitting}
					appointmentId={appointmentId}
					locationId={state.fields.locationId}
					disabled={state.fields.services.length === 0 || !state.fields.userId}
					isCreating={!appointmentId}
					onDelete={onClose}
					onSubmit={handleSubmit}
				/>
			}
		>
			<Row type="flex" gutter={16}>
				<Col className="appointment-form" xs={24} md={14}>
					<AppointmentForm
						date={state.displayDate}
						locationId={locationId}
						fields={state.fields}
						time={state.selectedTime}
						onTimeSelect={handleTimeSelection}
						onEmployeeSelect={id => setFieldValue('userId', id)}
						onDateChange={handleDateSelection}
						onServiceSelect={handleServicesSelection}
					/>
				</Col>

				<React.Suspense fallback={null}>
					<Col className="customer-selector" xs={24} md={10}>
						{!state.fields.customerId && <CustomerSelector onCustomerSelect={id => setFieldValue('customerId', id)} />}

						{state.fields.customerId && (
							<CustomerStats
								onRemoveCustomer={() => setFieldValue('customerId', undefined)}
								customerId={state.fields.customerId}
							/>
						)}
					</Col>
				</React.Suspense>
			</Row>
		</Modal>
	)
}

export default AppointmentModal
