import gql from 'graphql-tag'
import { appointmentFragments } from '../calendar/queries';

export const deleteAppointmentMutation = gql`
	mutation DeleteAppointment($id: ID!) {
		deleteAppointment(id: $id)
	}
`

export const CHECKOUT_MUTATION = gql`
	mutation Checkout($input: CheckoutInput!) {
		Checkout(input: $input) {
			sale {
				id
				appointment {
					id
					startTime
					endTime
					createdAt
					price
					duration
					status
					employee {
						id
						firstName
						lastName
					}
					services {
						id
						name
					}
					customer {
						id
						firstName
						lastName
					}
				}
			}
		}
	}
`

export const upsertAppointmentMutation = gql`
	mutation upsertAppointment($input: AppointmentInput!) {
		upsertAppointment(input: $input) {
			...appointmentParts
		}
	}

	${appointmentFragments}
`
