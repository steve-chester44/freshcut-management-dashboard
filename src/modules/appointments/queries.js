import gql from "graphql-tag"

export const PAYMENT_METHODS = gql`
	{
		paymentMethods {
			id
			name
		}
	}
`

export const appointmentQuery = gql`
	query appointment($id: Int!) {
		appointment(id: $id) {
			id
			startTime
			endTime
			createdAt
			price
			duration
			status
			locationId
			employee {
				id
				firstName
				lastName
			}
			services {
				id
				name
			}
			customer {
				id
				firstName
				lastName
				createdAt
				totalBookings
			}
		}
	}
`

export const customerInfoQuery = gql`
	fragment appointmentFragment on Appointment {
		id
		startTime
		endTime
		price
		services {
			id
			name
		}
		employee {
			id
			firstName
			lastName
		}
	}

	query customer($id: Int!) {
		customer(id: $id) {
			id
			firstName
			lastName
			phoneNumber
			totalBookings
			appointments {
				past {
					...appointmentFragment
				}
				upcoming {
					...appointmentFragment
				}
			}
		}
	}
`
