export const NEW_APPOINTMENT = "/calendar/appointment/new"
export const VIEW_APPOINTMENT = "/calendar/appointment/:id"
export const EDIT_APPOINTMENT_ROUTE = "/calendar/appointment/:id/edit"

export const NEW_BLOCKED_TIME_ROUTE = '/calendar/blocked-time'
export const EDIT_BLOCKED_TIME_ROUTE = '/calendar/blocked-time/:id/edit'