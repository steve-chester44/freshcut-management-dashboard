import React from "react"
import get from "lodash/get"
import { useQuery } from "@apollo/react-hooks"
import { Select } from "antd"

import { calendarInfoQuery } from "../../calendar/queries"

const EmployeeSelector = ({ value, onSelect, locationId }) => {
	const { data, loading } = useQuery(calendarInfoQuery, { variables: { locationId } })

	const employees = get(data, 'location.employees') || []

	return (
		<Select
			loading={loading}
			size="large"
			onChange={onSelect}
			placeholder="Select Employee"
			value={value}
			style={{ width: "100%" }}
		>
			{employees.map(employee => {
				return (
					<Select.Option key={`employee-${employee.id}`} value={employee.id}>
						{employee.firstName} {employee.lastName}
					</Select.Option>
				)
			})}
		</Select>
	)
}

export default React.memo(EmployeeSelector)
