import React from 'react'

import { customerInfoQuery } from '../../queries'

import styled from 'styled-components'
import { Tabs } from 'antd'

import Customer from '../Customer'
import AppointmentList from './AppointmentList'
import { useQuery } from '@apollo/react-hooks'
import { useRouter } from '../../../../common/hooks'

const Wrapper = styled('div')`
	flex: 1;
	position: relative;
	display: flex;
	flex-direction: column;
`

const Header = styled('div')`
	width: 100%;
`

const Stats = styled('div')`
	display: flex;
	padding: 10px;
	justify-content: center;
	font-size: 20px;
	font-weight: 100;

	.cell {
		text-align: center;
		max-width: 50%;
	}

	.heading {
		font-size: 14px;
		font-weight: 600;
	}
`

const CustomerStats = ({ customerId, onRemoveCustomer }) => {
	const { history } = useRouter()
	const { data, loading } = useQuery(customerInfoQuery, { variables: { id: customerId } })
	if (loading || !data) return null

	const customer = data.customer

	return (
		<Wrapper>
			<Header>
				<Customer
					customer={customer}
					actions={[
						{
							title: 'Edit Customer Details',
							onClick: () => {
								history.push(`/customers/${customerId}`)
							}
						},
						{ title: 'Remove Customer', onClick: onRemoveCustomer }
					]}
				/>

				<Stats>
					<div className="cell">
						<div className="stat">{customer.totalBookings}</div>
						<div className="heading">Total Bookings</div>
					</div>
				</Stats>
			</Header>

			<Tabs defaultActiveKey="appointments" style={{ flex: 1 }}>
				<Tabs.TabPane key="appointments" tab="Appointments">
					<AppointmentList
						appointments={customer.appointments.past}
						title="Past"
						style={{ overflow: 'auto', maxHeight: '100%' }}
					/>

					<AppointmentList
						appointments={customer.appointments.upcoming}
						title="Upcoming"
						style={{ overflow: 'auto', maxHeight: '100%' }}
					/>
				</Tabs.TabPane>
				<Tabs.TabPane key="info" tab="Info" style={{ padding: '0 20px' }}>
					<div>
						Name: {customer.firstName} {customer.lastName}
					</div>
					<div>Phone: {customer.phoneNumber}</div>
				</Tabs.TabPane>
			</Tabs>
		</Wrapper>
	)
}

export default CustomerStats
