import React from "react"
import distanceInWordsStrict from "date-fns/distance_in_words_strict"
import styled from "styled-components"
import { format } from "date-fns"

const Wrapper = styled("div")`
	width: 100%;
`

const Heading = styled("div")`
	font-size: 24px;
	padding: 20px;
	border-bottom: 1px solid rgba(32, 32, 32, 0.05);
`

const List = styled("div")`
	flex: 1;
`

const Appointment = styled("div")`
	padding: 10px 20px;
	border-bottom: 1px solid rgba(32, 32, 32, 0.05);
	display: flex;
	align-items: center;
	justify-content: space-between;

	.main {
		display: flex;
		align-items: center;
	}

	.price {
		font-size: 16px;
		font-weight: 500;
	}

	.service {
		.service-title {
			font-weight: 500;
			font-size: 18px;
		}

		.duration {
			font-size: 14px;
			color: rgba(32, 32, 32, 0.5);
		}
	}

	.date-time {
		width: 85px;
		height: 65px;
		background: rgba(250, 250, 250, 1);
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
		margin-right: 10px;

		.date {
			font-weight: 500;
		}

		.time {
			font-size: 12px;
		}
	}
`

const AppointmentList = ({ appointments, title }) => {
	return (
		<Wrapper>
			<Heading>
				{title} ({appointments.length})
			</Heading>
			<List>
				{appointments.map(appointment => {
					const start = new Date(appointment.startTime)
					const end = new Date(appointment.endTime)

					return (
						<Appointment key={appointment.id}>
							<div className="main">
								<div className="date-time">
									<div className="date">{format(start, "DD MMM")}</div>
									<div className="time">{format(start, "h:mma")}</div>
								</div>
								<div className="service">
									<div className="service-title">
										{appointment.services[0] && appointment.services[0].name}
									</div>

									<div className="duration">
										{distanceInWordsStrict(start, end)}

										{appointment.employee && (
											<span>
												<span> with </span>
												{appointment.employee.firstName}
											</span>
										)}
									</div>
								</div>
							</div>
							<div className="price">${appointment.price}</div>
						</Appointment>
					)
				})}
			</List>
		</Wrapper>
	)
}

export default AppointmentList
