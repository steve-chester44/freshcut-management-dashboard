import React from "react"
import { differenceInMinutes } from "date-fns"

import { DatePicker, Row, Col, Select, TimePicker, Form } from "antd"
import ServiceSelector from "./ServiceSelector"
import EmployeeSelector from "./EmployeeSelector"

const AppointmentForm = ({
	fields,
	date,
	time,
	locationId,
	onEmployeeSelect,
	onServiceSelect,
	onDateChange,
	onTimeSelect
}) => {
	return (
		<Row gutter={16}>
			<Row gutter={16}>
				<Col xs={16}>
					<Form.Item label="Date" colon={false}>
						<DatePicker
							size="large"
							style={{ width: "100%" }}
							value={date}
							format="MMMM DD, YYYY"
							onChange={onDateChange}
						/>
					</Form.Item>
				</Col>
				<Col xs={8}>
					<Form.Item label="Start Time" colon={false}>
						<TimePicker
							size="large"
							style={{ width: "100%" }}
							allowClear={false}
							defaultValue={time}
							format="h:mm A"
							onChange={onTimeSelect}
							use12Hours={true}
							value={time}
						/>
					</Form.Item>
				</Col>
			</Row>

			<Row>
				<Col xs={24}>
					<Form.Item label="Employee" colon={false} required={true}>
						<EmployeeSelector value={fields.userId} locationId={locationId} onSelect={onEmployeeSelect} />
					</Form.Item>
				</Col>
			</Row>

			<Row gutter={16}>
				<Col xs={18}>
					<Form.Item label="Service" colon={false} required={true}>
						<ServiceSelector
							value={fields.services[0]}
							locationId={locationId}
							onSelect={onServiceSelect}
						/>
					</Form.Item>
				</Col>
				<Col xs={6}>
					<Form.Item label="Duration" colon={false}>
						<Select
							size="large"
							disabled={true}
							value={
								fields.endTime
									? `${differenceInMinutes(fields.endTime, fields.startTime)}min`
									: undefined
							}
							style={{ width: "100%" }}
						/>
					</Form.Item>
				</Col>
			</Row>
		</Row>
	)
}

export default AppointmentForm
