import React from "react"
import styled from "styled-components"

import { Avatar, Icon, Menu, Dropdown } from "antd"
import { differenceInHours } from "date-fns"
import { Button } from "antd"
const Wrapper = styled("div")`
	padding: 13px 20px;
	display: flex;
	align-items: center;
	justify-content: space-between;
	border-bottom: 1px solid rgba(32, 32, 32, 0.05);

	.tags {
		.tag {
			padding: 3px 5px;
			border-radius: 10px;
			font-size: 9px;
			line-height: 1;

			&.new {
				background: rgba(79, 197, 182, 1);
				color: white;
			}
		}
	}

	${props =>
		props.clickable &&
		`
        cursor: pointer;

        &:hover {
            background: rgba(250, 250, 250, 1.0);
        }
    `} .info {
		display: flex;
		align-items: center;
	}

	.options {
		cursor: pointer;
		padding: 0 10px;
		font-size: 18px;

		&:hover {
			background: rgba(250, 250, 250, 1);
		}
	}

	.meta {
		padding-left: 20px;

		h2 {
			margin: 0;
			line-height: 1;
			padding: 0;
			font-size: 16px;
		}

		div {
			font-size: 12px;
		}
	}
`

const Customer = ({ customer, actions, onClick }) => {
	if (!customer) {
		return (
			<Wrapper>
				<div className="info">
					<Avatar style={{ verticalAlign: "middle" }} size="large">
						W
					</Avatar>
					<div className="meta">
						<h2>Walk-In</h2>
					</div>
				</div>
			</Wrapper>
		)
	}

	return (
		<Wrapper clickable={!!onClick} onClick={event => onClick && onClick(event, customer)}>
			<div className="info">
				<Avatar style={{ verticalAlign: "middle", minWidth: 40 }} size="large">
					{customer.firstName.charAt(0)}
				</Avatar>
				<div className="meta">
					<h2>
						{customer.firstName} {customer.lastName}
					</h2>
					<div>{customer.phoneNumber}</div>
					<div className="tags">
						{differenceInHours(new Date(), customer.createdAt) <= 24 && (
							<span className="tag new">New Customer</span>
						)}
					</div>
				</div>
			</div>

			{Array.isArray(actions) && (
				<Dropdown
					overlay={
						<Menu>
							{actions.map((action, idx) => (
								<Menu.Item key={`action-${idx}`} onClick={action.onClick}>
									{action.title}
								</Menu.Item>
							))}
						</Menu>
					}
				>
					<Button size="small" shape="circle">
						<Icon type="ellipsis" />
					</Button>
				</Dropdown>
			)}
		</Wrapper>
	)
}

export default Customer
