import React from 'react'
import styled from 'styled-components'

import { Popconfirm, message, Button, Dropdown, Menu, Icon } from 'antd'
import { useMutation } from '@apollo/react-hooks'

import { deleteAppointmentMutation } from '../mutations'

const Wrapper = styled('div')`
	padding: 5px 10px;
	display: flex;
	justify-content: flex-end;

	.ant-btn {
		margin: 0 2px 5px 0;
	}
`

const AppointmentButtons = ({
	isCreating = false,
	onSubmit,
	disabled = false,
	loading = false,
	appointmentId,
	locationId,
	onDelete
}) => {
	const [deleteAppointment] = useMutation(deleteAppointmentMutation)

	return (
		<Wrapper>
			{!isCreating && (
				<Dropdown
					placement="topCenter"
					overlay={
						<Menu>
							<Menu.Item>
								<Popconfirm
									title="Are you sure you want to delete this appointment?"
									onConfirm={async () => {
										const deleted = await deleteAppointment({
											variables: {
												id: appointmentId
											}
										})

										if (deleted) {
											message.success('Appointment deleted')
											if (onDelete) {
												onDelete()
											}
										} else {
											message.error('Failed to delete the appointment')
										}
									}}
								>
									Delete Appointment
								</Popconfirm>
							</Menu.Item>
						</Menu>
					}
				>
					<Button style={{ marginRight: 7 }} size="large">
						More Options <Icon type="down" />
					</Button>
				</Dropdown>
			)}
			<Button disabled={disabled} loading={loading} onClick={onSubmit} size="large" type="primary">
				{isCreating ? `Save Appointment` : `Update Appointment`}
			</Button>
		</Wrapper>
	)
}

export default AppointmentButtons
