import React from "react"
import { Input } from "antd"

const SearchBar = ({ value, onSearch, onFocus }) => {
	return (
		<Input.Search
			autoComplete="off"
			onFocus={onFocus}
			size="large"
			type="text"
			name="term"
			onChange={onSearch}
			placeholder="Find Client"
		/>
	)
}

export default SearchBar
