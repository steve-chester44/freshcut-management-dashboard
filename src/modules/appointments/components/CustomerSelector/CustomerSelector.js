import React from 'react'
import { useApolloClient } from '@apollo/react-hooks'
import debounce from 'lodash.debounce'
import styled from 'styled-components'

import { Button } from 'antd'

import SearchBar from './Search'
import List from './List'

import { customerSearchQuery } from '../../../customers/queries'

const CustomerModal = React.lazy(() => import('../../../customers/views/CustomerModal'))

const Wrapper = styled('div')`
	width: 100%;
	padding-top: 38px;
	height: 100%;
	display: flex;
	flex-direction: column;

	.search-bar {
		display: flex;
	}
`

const CustomerSelector = ({ onCustomerSelect }) => {
	const client = useApolloClient()

	const [state, setState] = React.useState({ searched: false, results: [] })

	const handleSearch = ({ target: { value } }) => doSearch(value)

	const handleFocus = () => setState(prev => ({ ...prev, searched: true }))
	const handleBlur = () => setState(prev => ({ ...prev, searched: false }))

	const getSearchResults = async term => {
		try {
			const { data } = await client.query({
				query: customerSearchQuery,
				variables: { input: { term: term } }
			})

			setState(prev => ({ ...prev, results: data.searchCustomers }))
		} catch (error) {
			console.log(error)
		}
	}

	const doSearch = debounce(getSearchResults, 100)

	return (
		<Wrapper>
			<div className="search-bar">
				<SearchBar value={state.term} onBlur={handleBlur} onFocus={handleFocus} onSearch={handleSearch} />
				<React.Suspense fallback={null}>
					{state.searched && (
						<CustomerModal onCreate={(customer) => onCustomerSelect(customer.id)}>
							{({ open }) => {
								return (
									<Button style={{ marginLeft: 7 }} size="large" icon="plus" onClick={event => open(null, event)}>
										Create
									</Button>
								)
							}}
						</CustomerModal>
					)}
				</React.Suspense>
			</div>

			<List items={state.results} onSelect={onCustomerSelect} searched={state.searched} />
		</Wrapper>
	)
}

export default CustomerSelector
