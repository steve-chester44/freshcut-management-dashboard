import React from "react"
import styled from "styled-components"
import Customer from "../Customer"

const Wrapper = styled("div")`
	width: 100%;
	height: 100%;
	max-height: calc(90vh - 318px);
	overflow-y: auto;
	margin-top: 14px;
`

const List = ({ items, onSelect }) => {
	return (
		<Wrapper>
			{items.map(item => (
				<Customer customer={item} key={`customer-${item.id}`} onClick={() => onSelect(item.id)} />
			))}
		</Wrapper>
	)
}

export default List
