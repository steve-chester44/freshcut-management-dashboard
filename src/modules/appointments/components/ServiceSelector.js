import React from 'react'
import get from 'lodash/get'
import { Select } from 'antd'
import { useQuery } from '@apollo/react-hooks'
import { calendarInfoQuery } from '../../calendar/queries'

const ServiceSelector = ({ locationId, onSelect, value }) => {
	const { loading, data } = useQuery(calendarInfoQuery, { variables: { locationId } })

	const serviceGroups = get(data, 'location.serviceGroups') || []
	return (
		<Select
			loading={loading}
			value={value}
			size="large"
			style={{ width: '100%' }}
			onChange={onSelect}
			placeholder="Select Service"
		>
			{serviceGroups.map(group => {
				return (
					<Select.OptGroup key={`group-${group.id}`} label={group.name}>
						{group.services.map(service => {
							const price = service.sources.length === 0 ? 0 : service.sources[0].price
							const duration = service.sources.length === 0 ? 0 : service.sources[0].duration

							return (
								<Select.Option
									data-price={price}
									data-duration={duration}
									key={`service-${service.id}`}
									value={service.id}
								>
									{service.name} ({duration} min) - ${price}
								</Select.Option>
							)
						})}
					</Select.OptGroup>
				)
			})}
		</Select>
	)
}

const areEqual = (prevProps, nextProps) => {
	return prevProps.value === nextProps.value && prevProps.locationId === nextProps.locationId
}

export default React.memo(ServiceSelector, areEqual)
