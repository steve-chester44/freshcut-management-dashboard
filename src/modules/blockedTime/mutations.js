import gql from "graphql-tag"

export const closeBlockTimeModal = gql`
	mutation closeBlockTimeModal {
		closeBlockTimeModal @client
	}
`

export const openBlockTimeModal = gql`
	mutation openBlockTimeModal($id: ID, $startTime: String, $endTime: String, $employeeId: ID, $description: String) {
		openBlockTimeModal(
			id: $id
			startTime: $startTime
			endTime: $endTime
			employeeId: $employeeId
			description: $description
		) @client
	}
`

export const upsertBlockedTime = gql`
	mutation upsertBlockedTime($input: BlockedTimeInput!) {
		upsertBlockedTime(input: $input) {
			blockedTime {
				id
				employeeId
				locationId
				startTime
				endTime
				description
			}
		}
	}
`

export const deleteBlockedTime = gql`
	mutation deleteBlockedTime($id: ID!) {
		deleteBlockedTime(id: $id)
	}
`
