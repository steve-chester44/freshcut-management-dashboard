import format from "date-fns/format"

/**
 * TODO: Remvoe the need for this resolver all together
 */
const createBlockedTimeResolve = ({ cache }) => {
	cache.writeData({
		data: {
			blockedTime: {
				__typename: "BlockedTimeModal",
				blockedTimeId: null,
				visible: false,
				startTime: null,
				endTime: null,
				description: null,
				employeeId: null,
				id: null
			}
		}
	})

	return {
		Mutation: {
			closeBlockTimeModal: (root, input, ctx) => {
				ctx.cache.writeData({
					data: {
						blockedTime: {
							id: null,
							blockedTimeId: null,
							description: null,
							visible: false,
							startTime: null,
							endTime: null,
							employeeId: null,
							__typename: "BlockedTimeModal"
						}
					}
				})
				return null
			},
			openBlockTimeModal: (root, input, ctx) => {
				ctx.cache.writeData({
					data: {
						blockedTime: {
							startTime: format(input.startTime),
							endTime: format(input.endTime),
							employeeId: input.employeeId,
							description: input.description,
							id: input.id || null,
							visible: true,
							__typename: "BlockedTimeModal"
						}
					}
				})
				return null
			}
		}
	}
}

export default createBlockedTimeResolve
