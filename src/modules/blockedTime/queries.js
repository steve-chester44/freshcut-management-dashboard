import gql from "graphql-tag"

export const blockedTimeModalQuery = gql`
	query {
        blockedTime @client {
            id
            startTime
            endTime
            employeeId
            visible
            description
        }
	}
`
