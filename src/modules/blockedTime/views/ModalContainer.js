import React from 'react'
import moment from 'moment'
import format from 'date-fns/format'
import { message, Row, Col, Modal, Form, DatePicker, Select, Input, TimePicker } from 'antd'
import { useQuery, useMutation } from '@apollo/react-hooks'
import ModalFooter from '../components/ModalFooter'
import { displayErrors } from '../../../graphql/utils'
import { calendarInfoQuery } from '../../calendar/queries'
import { upsertBlockedTime, deleteBlockedTime } from '../mutations'
import { CALENDAR_ROOT } from '../../calendar/utils/urls'

const BlockedTimeModal = ({ locationId, blockedTimeId, history, ...props }) => {
	const locationState = props.location.state

	const [upsertTime] = useMutation(upsertBlockedTime)

	const {
		data: { location },
		loading
	} = useQuery(calendarInfoQuery, {
		variables: {
			locationId
		}
	})

	// TODO: FINISH IMPLEMENTING
	const [deleteTime] = useMutation(deleteBlockedTime, {
		update: (proxy, mutationResult) => {
			const data = this.props.client.readQuery({
				query: calendarInfoQuery,
				variables: {
					locationId: this.props.locationId
				}
			})

			this.props.client.writeQuery({
				query: calendarInfoQuery,
				variables: {
					locationId: this.props.locationId
				},
				data: {
					...data,
					location: {
						...data.location,
						blockedTime: data.location.blockedTime.filter(time => +time.id !== +this.state.id)
					}
				}
			})
		},
		variables: {
			id: blockedTimeId
		}
	})

	const [state, setState] = React.useState({
		submitting: false,
		id: blockedTimeId,
		startTime: moment(locationState.startTime),
		endTime: moment(locationState.endTime),
		employeeId: locationState.employeeId,
		description: locationState.description || ''
	})

	const handleDescriptionChange = ({ target: { value } }) => {
		setState(prev => ({ ...prev, description: value }))
	}

	const handleSubmit = async () => {
		setState(prev => ({ ...prev, submitting: true }))

		const input = {
			id: blockedTimeId,
			startTime: format(state.startTime),
			endTime: format(state.endTime),
			description: state.description,
			employeeId: state.employeeId,
			locationId
		}

		try {
			await upsertTime({
				update: (proxy, { data: { upsertBlockedTime } }) => {
					if (!upsertBlockedTime) throw new Error({ message: 'Failed to create the blocked time.' })

					// New blocked time
					if (!blockedTimeId) {
						const data = proxy.readQuery({
							query: calendarInfoQuery,
							variables: {
								locationId
							}
						})

						proxy.writeQuery({
							query: calendarInfoQuery,
							variables: {
								locationId
							},
							data: {
								...data,
								location: {
									...data.location,
									blockedTime: data.location.blockedTime.concat([upsertBlockedTime.blockedTime])
								}
							}
						})
					}
				},
				variables: {
					input
				}
			})

			history.push(CALENDAR_ROOT)
		} catch (error) {
			displayErrors(error)
		}
	}

	const handleDelete = async () => {
		try {
			setState(prev => ({ ...prev, submitting: true }))

			await deleteTime()

			message.success('Blocked time deleted')
			history.push(CALENDAR_ROOT)
		} catch (error) {
			setState(prev => ({ ...prev, submitting: false }))
		}
	}

	if (loading) return null

	return (
		<Modal
			visible={true}
			title={blockedTimeId ? `Update Blocked Time` : `Add Blocked Time`}
			onOk={handleSubmit}
			onCancel={() => history.push(CALENDAR_ROOT)}
			footer={
				<ModalFooter
					isNew={!blockedTimeId}
					isDisabled={state.submitting}
					onSubmit={handleSubmit}
					onDelete={handleDelete}
					onCancel={() => history.push(CALENDAR_ROOT)}
				/>
			}
		>
			<Form.Item label="Date" colon={false}>
				<DatePicker
					format="MMMM Do, YYYY"
					value={state.startTime}
					style={{ width: '100%' }}
					size="large"
					onChange={newStartTime => {
						setState(prev => ({
							...prev,
							startTime: newStartTime,
							endTime: newStartTime.isAfter(prev.endTime)
								? newStartTime.clone().add(prev.endTime.diff(prev.startTime, 'minutes'), 'minutes')
								: prev.endTime
						}))
					}}
				/>
			</Form.Item>

			{location.employees.length > 1 && (
				<Form.Item label="Employee" colon={false}>
					<Select
						style={{ width: '100%' }}
						size="large"
						value={parseInt(state.employeeId, 10)}
						onChange={employeeId => setState(prev => ({ ...prev, employeeId }))}
					>
						{location.employees.map(employee => (
							<Select.Option key={`emp-${employee.id}`} value={employee.id}>
								{employee.firstName} {employee.lastName}
							</Select.Option>
						))}
					</Select>
				</Form.Item>
			)}

			<Row gutter={16}>
				<Col xs={24} md={12}>
					<Form.Item label="Start Time" colon={false}>
						<TimePicker
							allowClear={false}
							format="h:mm a"
							use12Hours={true}
							value={state.startTime}
							onChange={newStartTime => {
								setState(prev => ({
									...prev,
									startTime: newStartTime,
									endTime: newStartTime.isAfter(prev.endTime)
										? newStartTime.clone().add(prev.endTime.diff(prev.startTime, 'minutes'), 'minutes')
										: prev.endTime
								}))
							}}
							style={{ width: '100%' }}
							size="large"
						/>
					</Form.Item>
				</Col>

				<Col xs={24} md={12}>
					<Form.Item label="End Time" colon={false}>
						<TimePicker
							allowClear={false}
							format="h:mm a"
							value={state.endTime}
							onChange={newEndTime => {
								setState(prev => ({
									...prev,
									endTime: newEndTime,
									startTime: prev.startTime.isAfter(newEndTime)
										? newEndTime.clone().subtract(prev.startTime.diff(prev.endTime, 'minutes'), 'minutes')
										: prev.startTime
								}))
							}}
							use12Hours={true}
							style={{ width: '100%' }}
							size="large"
						/>
					</Form.Item>
				</Col>
			</Row>

			<Form.Item label="Description" colon={false}>
				<Input.TextArea onChange={handleDescriptionChange} value={state.description} />
			</Form.Item>
		</Modal>
	)
}

export default BlockedTimeModal
