import React from "react"
import { Popconfirm, Button } from "antd"

const ModalFooter = ({ isNew = false, isDisabled, onSubmit, onDelete }) => {
	return (
		<div style={{ display: "flex", justifyContent: "space-between" }}>
			{!isNew ? (
				<Popconfirm
					okText="Delete"
					onConfirm={onDelete}
					placement="topLeft"
					title="Are you sure you want to delete the scheduled block?"
				>
					<Button disabled={isDisabled} type="danger">
						Delete
					</Button>
				</Popconfirm>
			) : (
				<span />
			)}

			<div>
				<Button onClick={onSubmit} disabled={isDisabled} type="primary">
					{isNew ? "Create" : "Update"}
				</Button>
			</div>
		</div>
	)
}

export default React.memo(ModalFooter)
