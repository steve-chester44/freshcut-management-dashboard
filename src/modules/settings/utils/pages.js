import React from 'react'
import { WALKIN_APP, ONLINE_CHECKINS } from './urls'

import { Icon } from 'antd'

export const accountLinks = [
	{
		icon: <Icon type="idcard" />,
		to: '/settings/account',
		title: 'Account & Profile'
	},
	{
		icon: <Icon type="wallet" />,
		to: '/settings/billing',
		title: 'Plans & Billing',
		restrict: user => user.permissionLevel !== 'owner'
	}
]

export const appLinks = [
	{
		icon: <Icon type="shop" />,
		title: 'Walk-ins',
		restrict: user => !user.company.features.includes('WALKIN_APP'),
		to: WALKIN_APP
	},
	{
		icon: <Icon type="check-square" />,
		title: 'Online Check-in',
		restrict: user => !user.company.features.includes('ONLINE_CHECKIN'),
		to: ONLINE_CHECKINS
	}
	// { to: "/settings/online-booking", title: "Online Appointments", access: 1 },
	// { to: "/settings/api", title: "API Access", access: 1 }
]
