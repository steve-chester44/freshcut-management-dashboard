import React from 'react'
import { message, Row, Col, Form, Input, Button, Alert } from 'antd'

import Heading from '../components/Heading'

const OnlineCheckins = ({ user }) => {
	return (
		<div>
			<Heading title="Online Check-in" />

			<h3>
				{/* TODO: Hardcoded location */}
				<a
					target="new"
					href={`https://${process.env.REACT_APP_ONLINE_CHECKIN_URL}/waitlist/l/${user.company.locations[0].uuid}`}
					title="Online Check-ins"
				>
					Click here
				</a>{' '}
				to access your Online Check-in portal.
			</h3>
		</div>
	)
}

export default OnlineCheckins
