import React from "react"
import Heading from "../../components/Heading"

const BillingContainer = () => {
	return (
		<div>
            <Heading title="Billing" />
            
            <p>Please contact support if you need to make changes to your billing info.</p>
		</div>
	)
}

export default BillingContainer
