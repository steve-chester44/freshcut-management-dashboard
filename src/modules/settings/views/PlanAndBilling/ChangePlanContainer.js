import React from 'react'
import { useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import styled from 'styled-components'
import { Button, Popconfirm } from 'antd'

import { useUser } from '../../../../common/hooks'
import PricingTable from '../../components/PricingTable/PricingTable'

const ChangePlanModal = React.lazy(() => import('../../components/ChangePlanModal'))

export const Container = styled('div')`
	.period-switcher {
		margin: 16px 0;

		.ant-switch-checked {
			background: rgba(191, 191, 191, 1);
		}
	}

	.ant-list-item {
		padding: 24px;

		&-extra-wrap {
			width: 100%;
		}

		&-extra {
			padding-top: 6px;
		}

		&-meta-title {
			font-weight: 500;
			font-size: 16px;
		}
	}
`

const cancelSubscriptionMutation = gql`
	mutation cancelSubscription {
		cancelSubscription {
			id
			name
			subscription {
				end
				plan {
					name
					accessLevel
				}
			}
		}
	}
`

const ChangePlanContainer = () => {
	const { user } = useUser()
	const [cancelSubscription, { loading }] = useMutation(cancelSubscriptionMutation)

	return (
		<Container>
			<PricingTable currentPlan={user.company.subscription.plan} />

			{user.company.features.length > 0 && (
				<div style={{ width: '100%', display: 'flex', justifyContent: 'flex-end', marginTop: 48 }}>
					<Popconfirm
						title={
							<p>
								Are you sure you want to cancel your plan?
								<br />
								All access will be revoked and data could potentially be lost.
							</p>
						}
						onConfirm={async () => {
							cancelSubscription()
						}}
					>
						<Button disabled={loading} loading={loading} size="large" style={{ marginLeft: 10 }} type="danger" ghost>
							Cancel plan
						</Button>
					</Popconfirm>
				</div>
			)}
		</Container>
	)
}

export default ChangePlanContainer
