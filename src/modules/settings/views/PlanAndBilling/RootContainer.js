import React from 'react'

import { Tabs } from 'antd'
import LoadingPage from '../../../../components/routes/LoadingPage'

const ChangePlan = React.lazy(() => import('./ChangePlanContainer'))
const Billing = React.lazy(() => import('./BillingContainer'))

export default () => {
	return (
		<Tabs animated={{ inkBar: true, tabPane: false }} defaultActiveKey="plan">
			<Tabs.TabPane tab="Change Plan" key="plan">
				<React.Suspense fallback={<LoadingPage background="#fff" />}>
					<ChangePlan />
				</React.Suspense>
			</Tabs.TabPane>
			<Tabs.TabPane tab="Billing" key="billing">
				<React.Suspense fallback={<LoadingPage background="#fff" />}>
					<Billing />
				</React.Suspense>
			</Tabs.TabPane>
		</Tabs>
	)
}
