import React from 'react'
import styled from 'styled-components'
import { Route, Link, Redirect, Switch } from 'react-router-dom'
import LoadingPage from '../../../components/routes/LoadingPage'
import { appLinks, accountLinks } from '../utils/pages'
import { WALKIN_APP, ONLINE_CHECKINS } from '../utils/urls'

import { Menu } from 'antd'

const Account = React.lazy(() => import('./Account'))
const Billing = React.lazy(() => import('./PlanAndBilling/RootContainer'))
const CheckinCustomizer = React.lazy(() => import('../checkinapp/views/RootContainer'))
const OnlineCheckins = React.lazy(() => import('./OnlineCheckins'))

export const Container = styled('div')`
	display: flex;
	background: #fff;
	margin: 24px 0;
	position: relative;
	min-height: 80vh;
	width: 100%;
	flex: 1;
	flex-direction: column;

	.ant-menu-inline {
		border-right: none !important;
	}

	.sidemenu {
		width: 250px;
		padding: 16px 0;

		&--menu-title {
			padding: 10px;
			opacity: 0.2;
			text-transform: uppercase;
			margin: 0;
			line-height: 1;
			font-size: 12px;
			font-weight: 700;
		}
	}

	.main {
		border-left: 1px solid #f8f8f8;
		flex: 1 1 0%;
		padding: 25px;
	}

	@media (min-width: 900px) {
		flex-direction: row;
		margin: 24px;
		width: calc(100% - 48px);

		.sidemenu {
			min-height: 100%;
			height: 100%;
		}
	}
`

export default ({ user, location }) => {
	const renderLink = (link, idx) => {
		if (link.restrict && link.restrict(user)) return null

		if (!link.to && link.children) {
			return (
				<Menu.SubMenu
					key={idx}
					title={
						<span>
							{link.icon && link.icon}

							<span>{link.title}</span>
						</span>
					}
				>
					{link.children.map(child => {
						if (child.restrict && child.restrict(user)) return null

						return (
							<Menu.Item key={child.to}>
								<Link to={child.to}>{child.title}</Link>
							</Menu.Item>
						)
					})}
				</Menu.SubMenu>
			)
		}

		return (
			<Menu.Item key={link.to}>
				<Link to={link.to}>
					<span>
						{link.icon && link.icon}
						<span>{link.title}</span>
					</span>
				</Link>
			</Menu.Item>
		)
	}

	return (
		<Container>
			<div className="sidemenu">
				<h4 className="sidemenu--menu-title">Settings</h4>
				<Menu selectedKeys={[location.pathname]}>{accountLinks.map(renderLink)}</Menu>
				<h4 className="sidemenu--menu-title">Integrations</h4>
				<Menu selectedKeys={[location.pathname]}>{appLinks.map(renderLink)}</Menu>
			</div>

			<div className="main">
				<React.Suspense fallback={<LoadingPage background="#fff" />}>
					<Switch>
						<Route path="/settings/account" render={props => <Account {...props} user={user} />} />

						{user.permissionLevel === 'owner' && (
							<Route path="/settings/billing" render={props => <Billing {...props} user={user} />} />
						)}

						<Route path={WALKIN_APP} render={props => <CheckinCustomizer {...props} user={user} />} />
						<Route path={ONLINE_CHECKINS} render={props => <OnlineCheckins {...props} user={user} />} />

						<Redirect to={accountLinks[0].to} />
					</Switch>
				</React.Suspense>
			</div>
		</Container>
	)
}
