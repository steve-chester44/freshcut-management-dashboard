import React from 'react'
import { useMutation } from '@apollo/react-hooks'
import { message, Row, Col, Form, Input, Button, Alert } from 'antd'

import { ACCOUNT_MUTATION } from '../mutations'

import Heading from '../components/Heading'

const ChangePasswordModal = React.lazy(() => import('../../authentication/components/ChangePasswordModal'))

const validEmail = str =>
	!/(\.{2}|-{2}|_{2})/.test(str) &&
	/^[a-z0-9][a-z0-9-_.]+@([a-z]|[a-z0-9]?[a-z0-9-]+[a-z0-9]).[a-z0-9]{2,10}(?:.[a-z]{2,10})?$/.test(str)

const Account = ({ user }) => {
	const [updateAccount] = useMutation(ACCOUNT_MUTATION)

	const [state, setState] = React.useState({
		error: undefined,
		changePasswordModalVisible: false,
		fields: {
			firstName: user.firstName,
			lastName: user.lastName,
			email: user.email
		}
	})

	const { firstName, lastName, email } = state.fields

	const handleInputChange = ({ target: { name, value } }) => {
		setState(prev => ({ ...prev, error: undefined, fields: { ...prev.fields, [name]: value } }))
	}

	const handleSubmit = async () => {
		if (!validEmail(email)) {
			return setState(prev => ({ ...prev, error: 'Invalid email address' }))
		}

		await updateAccount({ variables: { input: state.fields } })

		message.success('Account updated')
	}

	return (
		<div>
			<Heading title="Account Settings" />

			<Row>
				<Col xs={24} lg={18} xl={12}>
					<Form>
						{state.error && <Alert message={state.error} type="error" />}
						<Row type="flex" gutter={16}>
							<Col span={12}>
								<Form.Item label="First Name" colon={false}>
									<Input type="text" size="large" name="firstName" value={firstName} onChange={handleInputChange} />
								</Form.Item>
							</Col>
							<Col span={12}>
								<Form.Item label="Last Name" colon={false}>
									<Input type="text" size="large" name="lastName" value={lastName} onChange={handleInputChange} />
								</Form.Item>
							</Col>
						</Row>

						<Form.Item label="Email" colon={false}>
							<Input type="email" size="large" name="email" value={email} onChange={handleInputChange} />
						</Form.Item>

						<Row type="flex" justify="space-between">
							<Button type="primary" disabled={email.length === 0 || firstName.length === 0} onClick={handleSubmit}>
								Update Settings
							</Button>
							<Button onClick={() => setState(prev => ({ ...prev, changePasswordModalVisible: true }))}>
								Change Password
							</Button>

							{state.changePasswordModalVisible && (
								<ChangePasswordModal
									onClose={() => setState(prev => ({ ...prev, changePasswordModalVisible: false }))}
									user={user}
								/>
							)}
						</Row>
					</Form>
				</Col>
			</Row>
		</div>
	)
}

export default Account
