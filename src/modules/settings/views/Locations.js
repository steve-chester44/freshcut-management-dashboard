import React, { PureComponent } from "react"
import { connect } from "react-redux"
import styled from "styled-components"

import { List } from "antd"

import { allLocationsSelector } from "../../../common/selectors/locations"
import { fetchLocations } from "../../../common/actions/locations"

import LocationsView from "../components/LocationsView"

const ContentPadder = styled.div`
	padding: 20px;
`

class LocationSettings extends PureComponent {
	async componentDidMount() {
		this.props.fetchLocations(this.props.user.organization)

	}
	
	render() {
		return (
			<ContentPadder>
				<LocationsView locations={this.props.locations} />
			</ContentPadder>
		)
	}
}

const mapState = state => ({
	user: state.auth.profile,
	// Should probably just get location ID's of the organization here then query the ORM within each LocationRow
	locations: allLocationsSelector(state)
})

const mapDispatch = { fetchLocations }

export default connect(mapState, mapDispatch)(LocationSettings)
