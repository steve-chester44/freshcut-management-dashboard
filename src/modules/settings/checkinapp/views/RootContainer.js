import React from 'react'

import { Button, Tabs } from 'antd'
import LoadingPage from '../../../../components/routes/LoadingPage'

const Theming = React.lazy(() => import('./Theming'))

// TODO: Query for the users colors, if none -- fallback to defaults
export default ({ user }) => {
	return (
		<div>
			<Tabs animated={{ inkBar: true, tabPane: false }} defaultActiveKey="plan">
				<Tabs.TabPane tab="Theme" key="plan">
					<React.Suspense fallback={<LoadingPage background="#fff" />}>
						<Theming />
					</React.Suspense>
				</Tabs.TabPane>
			</Tabs>

			<Button type="primary">Update Settings</Button>
		</div>
	)
}
