import React from 'react'
import { Row, Col } from 'antd'
import ColorPicker from '../components/ColorPicker/ColorPicker'

const colors = {
	bgColor: {
		label: 'Background Color',
		value: 'rgba(32,32,32,1.0)'
	},
	textColor: {
		label: 'Primary Text Color',
		value: 'rgba(255, 255, 255, 1.0)'
	},
	btnBg: {
		label: 'Button Color',
		value: 'rgba(255, 0, 0, 1.0)'
	},
	btnText: {
		label: 'Button Text Color',
		value: 'rgba(32, 32, 32, 1.0)'
	}
}

const Theming = () => {
	const [state, setState] = React.useState({ loading: false, colors, images: [] })

	return (
		<Row style={{ marginBottom: 25 }}>
			{Object.keys(state.colors).map((key, idx) => {
				const color = state.colors[key]

				return (
					<Col style={{ marginBottom: 10 }} xl={24} md={12} key={idx}>
						<ColorPicker
							{...color}
							onChange={value => {
								setState({
									...state,
									colors: { ...state.colors, [key]: { ...state.colors[key], value } }
								})
							}}
						/>
					</Col>
				)
			})}
		</Row>
	)
}

export default Theming
