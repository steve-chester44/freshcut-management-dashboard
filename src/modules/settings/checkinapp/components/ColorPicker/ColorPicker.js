import React, { useState, useRef } from "react"
import styled from "styled-components"
import useOnClickOutside from "use-onclickoutside"

import { SketchPicker } from "react-color"
import Swatch from "./Swatch"

const colorToRGB = color => {
	const { r, g, b, a } = color.rgb

	return `rgba(${r}, ${g}, ${b}, ${a})`
}

const Container = styled("div")`
	display: flex;
	cursor: pointer;
	margin: 5px 0;
	position: relative;
`

const Info = styled("div")`
	margin-left: 10px;

	h4 {
		margin: 0;
	}

	h5 {
		margin: 0;
		padding: 0;
		line-height: 1;
		opacity: 0.4;
		font-weight: 300;
	}
`

const PickerContainer = styled("div")`
	position: absolute;
	top: 45px;
	left: 0;
	z-index: 999;
`

const ColorPicker = ({ label, value, onChange }) => {
	const [pickerVisible, setVisibility] = useState(false)
	const ref = useRef()

	useOnClickOutside(ref, () => setVisibility(false))

	return (
		<Container>
			<Swatch color={value} onClick={() => setVisibility(true)} />
			<Info onClick={() => setVisibility(!pickerVisible)}>
				<h4>{label}</h4>
				<h5>{value}</h5>
			</Info>
			{pickerVisible && (
				<PickerContainer ref={ref}>
					<SketchPicker
						color={value}
						onChangeComplete={color => {
							onChange(colorToRGB(color))
						}}
					/>
				</PickerContainer>
			)}
		</Container>
	)
}

export default ColorPicker
