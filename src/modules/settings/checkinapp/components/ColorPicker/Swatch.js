import React from "react"
import styled from "styled-components"

import { Checkboard } from "react-color/lib/components/common"

const Container = styled("div")`
	position: relative;
	width: 40px;
	height: 40px;
	border-radius: 10px;
	overflow: hidden;
	border: 1px solid rgba(32, 32, 32, 0.1);
	background-color: ${({ color }) => `${color}`};
`

export default ({ color, ...rest }) => {
	return (
		<Container color={color} {...rest}>
			<Checkboard />
		</Container>
	)
}
