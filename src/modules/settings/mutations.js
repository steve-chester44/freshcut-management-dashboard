import gql from "graphql-tag"

export const ACCOUNT_MUTATION = gql`
	mutation updateMyProfile($input: UpdateProfileInput!) {
		updateMyProfile(input: $input)
	}
`

export const changeSubscription = gql`
	mutation ChangeSubscription($input: ChangeSubscriptionInput!) {
		changeSubscription(input: $input) {
			id
			name
			subscription {
				end
				plan {
					name
					accessLevel
				}
			}
		}
	}
`
