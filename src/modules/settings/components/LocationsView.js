import React, { PureComponent } from "react"

import { Card, Row, Button } from "antd"

import LocationsTable from "./LocationsTable/LocationsTable"
import LocationModal from "./LocationModal/LocationModal"

class LocationsView extends PureComponent {
	state = {
		editModalVisible: false,
		createModalVisible: false,
		activeLocation: undefined
	}

	handleLocationClick = activeLocation => {
		this.setState({ activeLocation, editModalVisible: true })
	}

	handleModalClose = () => {
		this.setState({
			editModalVisible: false,
			createModalVisible: false,
			activeLocation: undefined
		})
	}

	render() {
		return (
			<Row>
				<Row style={{ marginBottom: 20 }} type="flex" justify="end">
					<Button type="primary">Create Location</Button>
				</Row>
				<Card title="Locations">
					<LocationsTable onLocationClick={this.handleLocationClick} locations={this.props.locations} />
				</Card>

				{this.state.editModalVisible && (
					<LocationModal onClose={this.handleModalClose} location={this.state.activeLocation} />
				)}
			</Row>
		)
	}
}

export default LocationsView
