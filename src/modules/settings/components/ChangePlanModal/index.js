import React from 'react'
import styled from 'styled-components'
import { Modal, Radio, Button, List } from 'antd'

import { injectStripe, CardElement } from 'react-stripe-elements'
import { StripeProvider, Elements } from 'react-stripe-elements'

import { useRouter, useUser } from '../../../../common/hooks'

import { changeSubscription } from '../../mutations'

import { useMutation } from '@apollo/react-hooks'

export const Contents = styled('div')`
	display: flex;

	.price {
		margin: 0;
		line-height: 1;
		opacity: 0.7;
		margin-bottom: 24px;
	}

	.left {
		flex: 1;
		padding-right: 24px;

		.ant-list-item {
			&-extra-wrap {
				width: 100%;
				align-items: center;
			}

			&-extra {
				p {
					margin: 0;
					line-height: 1;
				}
			}

			&-meta {
				margin-bottom: 0;

				h4 {
					margin: 0;
					line-height: 1;
				}
			}
		}
	}

	.right {
		width: 245px;
		margin-top: 18px;

		ul {
			margin-left: 18px;

			li {
				padding: 5px 0;
			}
		}
	}
`

const ChangePlanModal = ({ isPayingCustomer, plan, onSuccess, onCancel, stripe }) => {
	const { history, location } = useRouter()
	const [interval, setPaymentInterval] = React.useState('month')

	const [ready, setReady] = React.useState(isPayingCustomer)
	const [changeSub, { loading }] = useMutation(changeSubscription)

	const { user } = useUser()

	const handleSubmit = async () => {
		if (!stripe) return

		let input = { plan: plan[interval].id }

		try {
			if (!isPayingCustomer) {
				const { source } = await stripe.createSource({
					type: 'card',
					currency: 'usd',
					owner: {
						email: user.contactEmail,
						name: `${user.firstName} ${user.lastName || ''}`
					}
				})

				input.source = {
					id: source?.id,
					status: source?.status,
					last4: source?.card.last4,
					exp_month: source?.card.exp_month,
					exp_year: source?.card.exp_year
				}
			}

			const { data } = await changeSub({
				variables: { input }
			})

			if (onSuccess) {
				onSuccess(data)
			} else {
				if (location.pathname !== '/settings/billing') {
					history.push('/settings/billing')
				}
			}
		} catch (error) {
			throw error
		}
	}

	const savings = plan.month.price * 2

	return (
		<Modal width="800px" onCancel={onCancel} visible={true} footer={null} title="Confirm plan">
			<Contents>
				<div className="left">
					<h2>{plan.name} Plan</h2>

					<p className="price">
						${plan[interval].price} every {interval}
					</p>
					{plan.accessLevel !== 'free' && (
						<Radio.Group
							style={{ width: '100%', marginBottom: 24 }}
							onChange={({ target: { value } }) => setPaymentInterval(value)}
							value={interval}
						>
							<List itemLayout="vertical" bordered={true} header="Choose how often you'd like to be billed.">
								<List.Item>
									<List.Item.Meta
										title={
											<Radio value={'month'}>
												<span>${plan.month.price} USD every 30 days</span>
											</Radio>
										}
									/>
								</List.Item>
								<List.Item
									extra={savings > 0 ? <p style={{ color: 'rgba(108, 180, 78, 1.0)' }}>Save ${savings}</p> : null}
								>
									<List.Item.Meta
										title={
											<Radio value={'year'}>
												<span>${plan.year.price} USD every year</span>
											</Radio>
										}
									/>
								</List.Item>
							</List>
						</Radio.Group>
					)}

					{!isPayingCustomer && (
						<div>
							<div style={{ margin: '0px auto 18px auto' }}>
								<div
									style={{
										background: '#fff',
										borderRadius: 5,
										border: '1px solid rgba(217, 217, 217, 1.0)',
										padding: 12,
										marginBottom: 10
									}}
								>
									<CardElement
										onChange={({ complete, ...rest }) => {
											if (!ready && complete) {
												setReady(true)
											}
											if (ready && !complete) {
												setReady(false)
											}
										}}
									/>
								</div>
							</div>
						</div>
					)}

					<Button
						disabled={loading || !ready}
						loading={loading}
						size="large"
						type="primary"
						style={{ width: '100%' }}
						onClick={handleSubmit}
					>
						Start Plan
					</Button>

					<div style={{ fontSize: 10, marginTop: 18, opacity: 0.5, textAlign: 'right' }}>
						Payments are securely handled by Stripe
					</div>
				</div>

				<div className="right">
					<h3>Plan Details</h3>
					<ul>
						{plan.features.map((feature, idx) => {
							return <li key={`f-${idx}`}>{feature}</li>
						})}
					</ul>
				</div>
			</Contents>
		</Modal>
	)
}

// TODO: I think this causes some jitter when opening the modal
const wrapWithStripe = WrappedComponent => {
	return props => {
		const [stripe, setStripe] = React.useState(null)

		React.useEffect(() => {
			if (window.Stripe) {
				setStripe(window.Stripe(process.env.REACT_APP_STRIPE_KEY))
			}

			const listener = () => {
				setStripe(window.Stripe(process.env.REACT_APP_STRIPE_KEY))
			}

			document.getElementById('stripe-js').addEventListener('load', listener)

			return () => document.getElementById('stripe-js').removeEventListener('load', listener)
		}, [])

		return (
			<StripeProvider stripe={stripe}>
				<Elements>
					<WrappedComponent {...props} />
				</Elements>
			</StripeProvider>
		)
	}
}

export default wrapWithStripe(injectStripe(ChangePlanModal))
