import React from "react"

import { Table } from "antd"

const LocationsTable = props => {
	const columns = [
		{
			title: "Name",
			dataIndex: "details",
			key: "name",
			render: (details, row, index) => (
				<a href="#" onClick={() => props.onLocationClick(props.locations[index])}>
					{details.name}
				</a>
			)
		},
		{
			title: "Address",
			dataIndex: "details",
			key: "id",
			render: details => {
				const renderAddress = `${details.address.street1}, ${details.address.city} ${
					details.address.state ? `, ${details.address.state}, ${details.address.zip}` : ""
				}`

				return <span>{renderAddress}</span>
			}
		}
	]

	return <Table columns={columns} dataSource={props.locations} />
}

export default LocationsTable
