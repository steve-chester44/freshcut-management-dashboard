import React, { useState, useEffect } from 'react'
import styled from 'styled-components'

import { getAccessLevel } from '../../../../common/utils/get-access-level'

import { Row, Col } from 'antd'
import PricingSlot from './PricingSlot'
import PricingDetail from './PricingDetail'
import ChangePlanModal from '../ChangePlanModal'

const Container = styled(Row)`
	padding: 10px;
	height: 100%;
	width: 100%;
	display: flex;
	justify-content: center;
`

const Card = styled('div')`
	border-radius: 8px;
	background: rgba(249, 249, 249, 1);
	padding: 12px;
	margin: 8px;
	height: 80px;
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100%;

	h2 {
		text-align: center;
		margin: 0;
		font-size: 20px;
		line-height: 1.3;
	}
`

const isProd = process.env.NODE_ENV === 'production'

const colStyle = {
	marginBottom: 16
}

export default ({ currentPlan }) => {
	const [selectedPlan, setSelectedPlan] = useState(undefined)
	const [plans, setPlans] = useState(null)

	const currentLevel = getAccessLevel(currentPlan.accessLevel)

	useEffect(() => {
		fetch(isProd ? `https://api.neverwait.app/plans` : `http://localhost:3443/plans`)
			.then(res => {
				if (res.json) {
					return res.json()
				}
			})
			.then(setPlans)
	}, [])

	return (
		<div>
			<h1
				style={{
					textAlign: 'center',
					marginTop: 24,
					fontSize: 40,
					lineHeight: 1,
					marginBottom: 14,
					fontWeight: 700,
					fontFamily: 'jaf-domus'
				}}
			>
				Great plans for barbers, by barbers.
			</h1>

			<Container type="flex" gutter={8}>
				<div />
				{!plans && <div>Loading available plans...</div>}
				{plans &&
					plans.map((plan, index) => {
						const planLevel = getAccessLevel(plan.accessLevel)

						return (
							<Col xs={24} md={12} style={{ marginBottom: 8 }} xl={8} key={`plan-${index}`}>
								<PricingSlot
									color={index % 2 === 0 ? 'rgba(25, 162, 183, 1.0)' : 'rgba(31, 197, 167, 1.0)'}
									onClick={() => {
										setSelectedPlan(plan)
									}}
									isCurrent={plan.accessLevel === currentPlan.accessLevel}
									isDowngrade={currentLevel > planLevel}
									title={plan.name}
									price={plan.month.price}
								>
									{plan.features.map((feature, idx) => {
										return <PricingDetail key={idx}>{feature}</PricingDetail>
									})}
								</PricingSlot>
							</Col>
						)
					})}
			</Container>

			<p
				style={{
					opacity: 0.3,
					textAlign: 'center'
				}}
			>
				SMS based notifications volume is a monthly limit that resets every month. You will be charged $0.02 per excess
				SMS.
			</p>

			<Row>
				<Col xs={24}>
					<Card>
						<h2>Additional staff members just $10/month.</h2>
					</Card>
				</Col>
			</Row>

			<h1
				style={{
					textAlign: 'center',
					paddingTop: 48,
					fontSize: 36,
					lineHeight: 1,
					marginBottom: 4,
					marginTop: 14,
					fontWeight: 700,
					fontFamily: 'jaf-domus'
				}}
			>
				Plus all of these great features
			</h1>
			<p
				style={{
					opacity: 0.5,
					fontSize: 22,
					textAlign: 'center',
					paddingBottom: 14
				}}
			>
				included for free with all plans
			</p>

			<Row type="flex" justify="space-around" gutter={16} style={{ marginTop: 24, padding: '0 20px' }}>
				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							All-in-One Calendar Management
						</h2>
					</Card>
				</Col>

				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							Email Reminders and Notifications
						</h2>
					</Card>
				</Col>

				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							Staff scheduling, availability and personal time management
						</h2>
					</Card>
				</Col>

				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							Service times and prices per staff member
						</h2>
					</Card>
				</Col>

				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							Client profiles with complete history and notes.
						</h2>
					</Card>
				</Col>

				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							Multiple locations
						</h2>
					</Card>
				</Col>

				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							Mobile Client App
						</h2>
					</Card>
				</Col>

				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							Cancellation / No-Show Policy and Fees
						</h2>
					</Card>
				</Col>

				<Col xs={24} lg={8} style={colStyle}>
					<Card>
						<h2>
							<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
									fill="rgba(46, 196, 167, 1.0)"
									fillRule="nonzero"
								/>
							</svg>{' '}
							Analytics and Reporting*
							<br />
							<span style={{ fontSize: 16, opacity: 0.5 }}>Coming Soon</span>
						</h2>
					</Card>
				</Col>
			</Row>

			{selectedPlan && (
				<ChangePlanModal
					isPayingCustomer={currentPlan.accessLevel !== 'free'}
					plan={selectedPlan}
					onSuccess={() => setSelectedPlan(undefined)}
					onCancel={() => {
						setSelectedPlan(undefined)
					}}
				/>
			)}
		</div>
	)
}
