import React from 'react'
import styled from 'styled-components'
import { Button } from 'antd'

const Container = styled('div')`
	border-top: 0;
	height: 100%;
	display: flex;
	flex-direction: column;
	background: white;
	border-radius: 4px;
	border: 1px solid rgba(232, 232, 232, 1);
`

const Header = styled('div')`
	position: relative;
	padding: 24px 24px 5px 24px;
	margin-bottom: 24px;

	&:after {
		content: '';
		position: absolute;
		bottom: 0;
		left: 0;
		width: 100%;
		display: block;
		height: 1px;
		background: #e3e7eb;
		background: linear-gradient(
			90deg,
			rgba(227, 231, 235, 0.1) 0,
			rgba(227, 231, 235, 0.6) 50%,
			rgba(227, 231, 235, 0.1)
		);
	}

	h2 {
		font-size: 1.2rem;
		color: rgba(31, 197, 167, 1);
		line-height: 1;
		font-size: 24px;
	}
`

const Contents = styled('div')`
	padding: 0 24px 24px 24px;
	flex: 1;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`

const Price = styled('div')`
	font-size: 0.75rem;
	font-weight: 400;
	padding-bottom: 0.75em;

	.amount {
		font-size: 2.5rem;
		font-weight: 700;

		.currency {
			vertical-align: super;
			font-size: 1rem !important;
			font-weight: 400 !important;
			color: #4a4a4a;
		}
	}
`

const Details = styled('div')`
	ul {
		list-style-type: none;
	}
`

const PricingSlot = ({ onClick, isCurrent, isDowngrade, title, price, color, children }) => {
	return (
		<Container>
			<Header color={color}>
				<h2>{title}</h2>

				<Price>
					<span className="amount">
						<span className="currency">$</span>
						{price}
					</span>
					/month
				</Price>
			</Header>
			<Contents>
				<Details>
					<ul>{children}</ul>
				</Details>

				<Button
					type="primary"
					size="large"
					style={{
						marginTop: 14
					}}
					disabled={isCurrent}
					onClick={onClick}
				>
					{isCurrent ? 'Current plan' : isDowngrade ? 'Downgrade' : 'Upgrade'}
				</Button>
			</Contents>
		</Container>
	)
}

export default PricingSlot
