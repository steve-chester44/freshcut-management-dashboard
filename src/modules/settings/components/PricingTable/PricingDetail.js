import React from "react"
import styled from "styled-components"

const Container = styled("li")`
	display: flex;
	align-items: center;
	margin-bottom: 14px;
	color: rgba(146, 162, 176, 1);

	.icon {
		display: inline-flex;
		width: 16px;
		height: 12px;
		margin-right: 12px;
	}
`

export default ({ children }) => {
	return (
		<Container>
			<span className="icon">
				<svg width="16" height="12" xmlns="http://www.w3.org/2000/svg">
					<path
						d="M14.3.3L5 9.6 1.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4-.4-.4-1-.4-1.4 0z"
						fill="rgba(46, 196, 167, 1.0)"
						fillRule="nonzero"
					/>
				</svg>
			</span>
			{children}
		</Container>
	)
}
