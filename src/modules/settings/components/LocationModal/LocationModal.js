/*
Handles the creation and 'updation' of Locations
*/

import React, { PureComponent } from "react"

import { Modal } from "antd"

class LocationModal extends PureComponent {
	constructor(props) {
		super(props)
		this.state = {
			fields: {}
		}
	}
	render() {
		return (
			<Modal
				title="Edit Location"
				visible={this.state.editModalVisible}
				onOk={this.handleUpdate}
				onCancel={this.handleModalClose}
			>
				<p>Some contents...</p>
				<p>Some contents...</p>
				<p>Some contents...</p>
			</Modal>
		)
	}
}

export default LocationModal
