import React from 'react'
import styled from 'styled-components'

export const Container = styled('div')`
	margin-bottom: 12px;
`

const Heading = ({ title }) => {
	return (
		<Container>
			<h1>
				<span>{title}</span>
			</h1>
		</Container>
	)
}

export default Heading
