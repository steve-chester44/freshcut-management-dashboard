import gql from "graphql-tag"

export const setCalendarInsertMode = gql`
	mutation setCalendarInsertMode($mode: String!) {
		setCalendarInsertMode(mode: $mode) @client
	}
`

export const setCalendarDate = gql`
	mutation setCalendarDate($date: String!) {
		setCalendarDate(date: $date) @client
	}
`

export const OPEN_BLOCKED_TIME_MODAL_MUTATION = gql`
	mutation openBlockedTimeModal($id: ID!) {
		openBlockedTimeModal(id: $id) @client
	}
`

export const setScrollTime = gql`
	mutation setScrollTime($date: Date) {
		setScrollTime(date: $date) @client
	}
`

export const setActiveLocationMutation = gql`
	mutation setLocation($location: String!) {
		setLocation(location: $location) @client
	}
`

export const setCalendarResource = gql`
	mutation setCalendarResource($resource: String!) {
		setCalendarResource(resource: $resource) @client
	}
`

export const SET_CALENDAR_VIEW_MUTATION = gql`
	mutation setCalendarView($view: String!) {
		setCalendarView(view: $view) @client
	}
`
