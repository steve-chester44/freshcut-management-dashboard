import { createSelector } from "reselect"

export const generateTimelineIntervals = seconds => {
	var time = new Date()
	time.setHours(0, 0, 0, 0)

	const intervals = []
	const minutes = seconds / 60
	const max = 24 * (60 / minutes)

	for (var i = 0; i <= max - 1; i++) {
		intervals.push(new Date(time))
		time.setMinutes(time.getMinutes() + minutes)
	}

	return intervals
}

export const createEmployeeTimeline = (employee, id) => {
	id = id || Math.random()
	return { id, type: "Employee", key: `emp:${id}`, ...employee }
}

export const createTimelineList = createSelector(
	employees => employees,
	employees => Object.keys(employees).map(empId => createEmployeeTimeline(employees[empId], empId))
)

export const getWeekTimelines = createSelector(
	date => date,
	date => {
		return Array.from({
			length: 7
		}).map((x, index) => {
			let newDay = date.clone().add(index, "day")

			return {
				date: newDay,
				type: "Weekday",
				key: `weekday:${newDay.format("d")}`
			}
		})
	}
)
