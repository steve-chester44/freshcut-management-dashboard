const onSubscriptionData = ({ client, subscriptionData, queryVariables, query }) => {
	console.log('SUBSCRIPTION DATA')
	if (!subscriptionData.data || !subscriptionData.data.AppointmentsChange) return

	const { appointment, isNewRecord, deleted } = subscriptionData.data.AppointmentsChange

	console.log({ appointment })
	const cache = client.readQuery({ query, variables: queryVariables })

	console.log({ cache })
	const isDeleted = deleted || appointment.deleted

	// if we're updating the record then do nothing, let apollo handle it
	if (!isNewRecord && !isDeleted) return

	const location = {
		...cache.location,
		appointments: isDeleted
			? cache.location.appointments.filter(appt => appt.id !== appointment.id)
			: cache.location.appointments.concat([appointment])
	}

	client.writeQuery({
		query,
		variables: queryVariables,
		data: {
			...cache,
			location
		}
	})
}

export default onSubscriptionData
