import startOfDay from "date-fns/start_of_day"
import parse from "date-fns/parse"
import endOfDay from "date-fns/end_of_day"
import addDay from "date-fns/add_days"

const getCalendarTimes = date => {
	return {
		startTime: startOfDay(parse(date)),
		endTime: endOfDay(addDay(parse(date), 1))
	}
}

export default getCalendarTimes
