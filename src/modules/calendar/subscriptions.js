import gql from 'graphql-tag'
import { appointmentFragments } from './queries';

export const appointmentsSubscription = gql`
	subscription onAppointmentsChange($locationId: ID!) {
		AppointmentsChange(locationId: $locationId) {
			isNewRecord
			appointment {
				...appointmentParts
			}
		}
	}

	${appointmentFragments}
`
