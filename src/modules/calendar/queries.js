import gql from 'graphql-tag'

import { baseEmployeeFragment } from '../employees/queries'

export const insertModeQuery = gql`
	query insertMode {
		calendarInsertMode @client
	}
`

export const calendarDate = gql`
	query date {
		calendarDate @client
	}
`

export const calendarRequirements = gql`
	query calendarRequirements {
		activeLocation @client {
			id
			name
		}
		calendarDate @client
	}
`

export const appointmentFragments = gql`
	fragment appointmentParts on Appointment {
		id
		startTime
		endTime
		price
		duration
		status
		locationId
		createdAt
		employee {
			id
			firstName
			lastName
		}
		services {
			id
			name
		}
		customer {
			id
			firstName
			lastName
		}
		source {
			type
		}
	}
`

export const blockedTimeFragments = gql`
	fragment blockedTimeParts on BlockedTime {
		id
		startTime
		endTime
		locationId
		employeeId
		description
	}
`

export const serviceFragments = gql`
	fragment serviceParts on Service {
		id
		name
		sources {
			id
			name
			type
			price
			duration
			serviceId
		}
	}
`

export const locationFragments = gql`
	fragment locationParts on Location {
		id
		name
		employees(input: { where: { bookingEnabled: true } }) {
			...BaseEmployeeParts

			services {
				...serviceParts
			}
		}

		serviceGroups {
			id
			name
			services {
				...serviceParts
			}
		}

		appointments(input: { where: { startTime: { gt: $startTime }, endTime: { lt: $endTime } } }) {
			...appointmentParts
		}

		blockedTime {
			...blockedTimeParts
		}
	}

	${baseEmployeeFragment}
	${appointmentFragments}
	${serviceFragments}
	${blockedTimeFragments}
`

export const calendarResourcesQuery = gql`
	query calendarResources($locationId: ID) {
		activeResource @client {
			...BaseEmployeeParts
		}

		employees(input: { where: { locationId: $locationId, bookingEnabled: true } }) {
			...BaseEmployeeParts
		}
	}

	${baseEmployeeFragment}
`

export const calendarActiveLocationQuery = gql`
	{
		activeLocation @client {
			id
			name
		}
	}
`

export const CALENDAR_VIEW = gql`
	{
		view @client
	}
`

export const calendarInfoQuery = gql`
	query CalendarInfo($locationId: Int!, $startTime: String, $endTime: String) {
		calendarScrollTime @client
		calendarInsertMode @client
		view @client
		timelineInterval @client
		activeAppointment @client
		activeResource @client {
			id
			firstName
		}
		location(locationId: $locationId) {
			...locationParts
		}
	}

	${locationFragments}
`
