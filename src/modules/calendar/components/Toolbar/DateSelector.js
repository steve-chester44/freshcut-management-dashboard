import React from "react"
import moment from "moment"
import styled from "styled-components"
import { Button, Icon, DatePicker } from "antd"
import formatDate from "date-fns/format"

const Container = styled("div")`
	.ant-calendar-picker {
		width: 0;
		height: 0;
		overflow: hidden;
	}

	.ant-calendar-picker-input {
		overflow: hidden;
		width: 0;
		height: 0;
		padding: 0;
		border: 0;
		display: none;
	}

	.ant-calendar-picker-clear,
	.ant-calendar-range-picker-input,
	.ant-calendar-range-picker-separator,
	.ant-calendar-picker-icon {
		display: none !important;
	}
`

const DateSelector = ({ onNavigate, date, format = "MMMM DD" }) => {
	const [datePickerVisible, setVisibility] = React.useState(false)

	return (
		<Container className="date-selector">
			{datePickerVisible && (
				<DatePicker
					open={true}
					onOpenChange={setVisibility}
					value={moment(date)}
					onChange={date => onNavigate("DATE", date.toDate())}
				/>
			)}

			<Button.Group size="large">
				<Button onClick={() => onNavigate("PREV")}>
					<Icon type="left" />
				</Button>
				<Button onClick={() => setVisibility(prev => !prev)}>{formatDate(date, format)}</Button>
				<Button onClick={() => onNavigate("NEXT")}>
					<Icon type="right" />
				</Button>
			</Button.Group>
		</Container>
	)
}

export default React.memo(DateSelector)
