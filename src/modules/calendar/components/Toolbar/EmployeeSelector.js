import React from 'react'

import { Dropdown, Menu, Button, Icon } from 'antd'

import { calendarResourcesQuery } from '../../queries'
import { setCalendarResource } from '../../mutations'
import { useQuery, useMutation } from '@apollo/react-hooks'

const EmployeeSelector = ({ locationId }) => {
	const { loading, data, error } = useQuery(calendarResourcesQuery, {
		// Network-only is a work around for user cache not updating after modifying an employee from the State page.
		fetchPolicy: 'network-only',
		skip: !locationId,
		variables: { locationId }
	})
	const [setResource] = useMutation(setCalendarResource)

	if (loading || error || !data) return null

	if (!data.activeResource) return null
	const selectAllEmployees = () => {
		setResource({
			variables: {
				resource: {
					id: 'all',
					firstName: 'All Employees',
					lastName: null,
					__typename: 'User'
				}
			}
		})
	}

	const selectEmployee = employee => setResource({ variables: { resource: employee } })

	return (
		<Dropdown
			overlay={
				<Menu>
					{data.activeResource.id !== 'all' && <Menu.Item onClick={selectAllEmployees}>All Employees</Menu.Item>}

					{/* TODO: Filter out employees that do not have any services selected. */}
					{data.employees.map(employee => {
						if (employee.id === data.activeResource.id) return null

						return (
							<Menu.Item key={employee.id} onClick={() => selectEmployee(employee)}>
								{employee.firstName} {employee.lastName}
							</Menu.Item>
						)
					})}
				</Menu>
			}
		>
			<Button size="large" style={{ marginLeft: 8 }}>
				{data.activeResource.firstName} {data.activeResource.lastName} <Icon type="down" />
			</Button>
		</Dropdown>
	)
}

export default EmployeeSelector
