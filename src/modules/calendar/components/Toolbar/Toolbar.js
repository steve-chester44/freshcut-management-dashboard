import React from 'react'
import styled from 'styled-components'

import FloatingMenu from '../../../../components/FloatingMenu/FloatingMenu'

import ViewRangeSelector from './ViewRangeSelector'
import EmployeeSelector from './EmployeeSelector'
import LocationSelector from './LocationSelector'

import DateSelector from './DateSelector'

import { SET_CALENDAR_VIEW_MUTATION, setCalendarInsertMode, setActiveLocationMutation } from '../../mutations'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { NEW_APPOINTMENT } from '../../../appointments/urls'
import { companyLocationsQuery } from '../../../../common/queries/Location'
import { Row, Col, Icon } from 'antd'
import { useRouter } from '../../../../common/hooks'

const rootAction = { content: <Icon type="plus" /> }

const StyledRow = styled(Row)`
	padding: 13px 13px 0 13px;

	.middle {
		display: flex;
		justify-content: center;
	}
`

const Toolbar = ({ onNavigate, date, view }) => {
	const { history } = useRouter()
	const [setCalendarView] = useMutation(SET_CALENDAR_VIEW_MUTATION)
	const [setLocation] = useMutation(setActiveLocationMutation)
	const [setInsertMode] = useMutation(setCalendarInsertMode)

	const {
		data: { activeLocation, locations = [] }
	} = useQuery(companyLocationsQuery)

	const actions = [
		// {
		// 	content: <Icon type="clock-circle" />,
		// 	tooltip: "New Blocked Time",
		// 	onClick: () => setInsertMode({ variables: { mode: "blockedTime" } })
		// },
		{
			content: <Icon type="plus" />,
			onClick: () => history.push(NEW_APPOINTMENT),
			tooltip: 'Add Appointment'
		}
	]

	return (
		<StyledRow type="flex" align="middle">
			<Col xs={8}>
				{locations.length > 1 && (
					<LocationSelector
						activeLocation={activeLocation}
						locations={locations}
						onSelect={location => {
							setLocation({ variables: { location } })
						}}
					/>
				)}
				<EmployeeSelector locationId={activeLocation ? activeLocation.id : undefined} />
			</Col>

			<Col xs={8} className="middle">
				<DateSelector onNavigate={onNavigate} date={date} />
			</Col>

			<Col xs={8} style={{ display: 'flex', justifyContent: 'flex-end' }}>
				<ViewRangeSelector
					value={view}
					onChange={view => {
						setCalendarView({ variables: { view } })
					}}
				/>
			</Col>

			<FloatingMenu actions={actions} rootAction={rootAction} />
		</StyledRow>
	)
}

export default Toolbar
