import React from "react"
import { Button } from "antd"

const ViewRangeSelector = ({ value, onChange }) => {
	return (
		<Button.Group size="large">
			<Button type={value === "day" ? "primary" : ""} onClick={() => onChange('day')}>
				Day
			</Button>

			<Button type={value === "week" ? "primary" : ""} onClick={() => onChange('week')}>
				Week
			</Button>
		</Button.Group>
	)
}

export default ViewRangeSelector
