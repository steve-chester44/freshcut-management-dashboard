import React from "react"

import { Dropdown, Menu, Button, Icon } from "antd"

const LocationSelector = ({ locations = [], loading = false, onSelect, activeLocation }) => {
	return (
		<Dropdown
			overlay={
				<Menu>
					{locations.map(location => (
						<Menu.Item key={location.id} onClick={() => onSelect(location)}>
							{location.name}
						</Menu.Item>
					))}
				</Menu>
			}
		>
			<Button disabled={loading} style={{ marginLeft: 8 }}>
				{activeLocation ? activeLocation.name : "Select Location"} <Icon type="down" />
			</Button>
		</Dropdown>
	)
}

export default LocationSelector
