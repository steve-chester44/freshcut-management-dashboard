import React from 'react'
import dates from 'date-arithmetic'
import TimeGrid from 'react-big-calendar/lib/TimeGrid'

const Day = ({ date, ...props }) => {
	const range = Day.range(date)
	return <TimeGrid {...props} range={range} eventOffset={10} />
}

Day.range = date => {
	return [dates.startOf(date, 'day')]
}

Day.navigate = (date, action) => {
	switch (action) {
		case 'PREV':
			return dates.add(date, -1, 'day')

		case 'NEXT':
			return dates.add(date, 1, 'day')

		default:
			return date
	}
}

Day.title = date => date.toLocaleDateString()

export default Day
