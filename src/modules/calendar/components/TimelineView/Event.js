import React from 'react'
import styled from 'styled-components'
import format from 'date-fns/format'
import { Tooltip } from 'antd'

const sourceColor = {
	default: '#E9CF4B',
	onlineappointment: '#66ce89',
	onlinecheckin: '#66ce89',
	walkin: '#CE66AB'
}

const sourceStyles = ({ sourceType }) => `
	.jewel {
		background-color: ${sourceColor[sourceType]};
	}
`

const Container = styled('div')`
	position: relative;
	display: flex;
	justify-content: space-between;
	align-items: center;
	height: 100%;
	padding: 4px;
	overflow: hidden;
	line-height: 17px;

	.details {
		display: flex;
		align-items: center;
	}

	.left {
		display: flex;
		align-items: center;
	}

	.time {
		font-size: 12px;
		margin-right: 4px;
		white-space: nowrap;
	}

	.duration {
		font-size: 12px;
		opacity: 0.5;
	}

	.name {
		font-weight: 700;
		font-size: 14px;
		white-space: nowrap;
	}

	.service {
		font-weight: 100;
		font-size: 12px;
		white-space: nowrap;
	}

	.jewel {
		margin-right: 8px;
		left: 0;
		width: 10px;
		height: 10px;
		border-radius: 50%;
	}

	${sourceStyles};
`

const readable = {
	onlineappointment: 'via Online Appointment',
	onlinecheckin: 'via Online Check-in',
	walkin: 'via Walk-in',
	default: 'manually'
}

const CustomEvent = ({ event }) => {
	const name = event.customer ? `${event.customer.firstName || ''} ${event.customer.lastName || ''}` : 'Walk-In'
	const sourceType = event?.source?.type || 'default'

	return (
		<Container sourceType={sourceType}>
			<div className="left">
				<Tooltip placement="right" title={`booked ${readable[sourceType]}`}>
					<div className="jewel" />
				</Tooltip>

				<div>
					<div className="details">
						<div className="time">
							{format(event.startTime, 'h:mm A')} - {format(event.endTime, 'h:mm A')}
						</div>
						<div className="name">{name}</div>
					</div>
					{event.services?.[0] && (
						<div className="service">{event.services.length > 1 ? 'Multiple Services' : event.services?.[0]?.name}</div>
					)}
				</div>
			</div>

			<div className="duration">{event.duration} mins</div>
		</Container>
	)
}

export default CustomEvent
