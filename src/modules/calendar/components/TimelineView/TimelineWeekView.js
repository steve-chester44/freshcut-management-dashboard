import React from "react"
import dates from "date-arithmetic"
import TimeGrid from "react-big-calendar/lib/TimeGrid"

const TimelineWeekView = ({ date, ...props }) => {
	const range = TimelineWeekView.range(date)

	return <TimeGrid {...props} range={range} eventOffset={10} />
}

TimelineWeekView.range = date => {
	let start = date
	let end = dates.add(start, 5, "day")

	let current = start
	let range = []

	while (dates.lte(current, end, "day")) {
		range.push(current)
		current = dates.add(current, 1, "day")
	}

	return range
}

TimelineWeekView.navigate = (date, action) => {
	switch (action) {
		case "PREV":
			return dates.add(date, -3, "day")

		case "NEXT":
			return dates.add(date, 3, "day")

		default:
			return date
	}
}

TimelineWeekView.title = date => date.toLocaleDateString()

export default TimelineWeekView
