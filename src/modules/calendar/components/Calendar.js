import React from 'react'
import { generatePath } from 'react-router-dom'
import moment from 'moment'

import { Calendar as BigCalendar, momentLocalizer } from 'react-big-calendar'

import TimelineDayView from './TimelineView/TimelineDayView'
import TimelineWeekView from './TimelineView/TimelineWeekView'

import Toolbar from './Toolbar/Toolbar'
import Event from './TimelineView/Event'
import Header from './TimelineView/Header'

import { setCalendarDate as setCalendarDateMutation, setScrollTime as setCalendarScrollTime } from '../mutations'
import { Alert } from 'antd'
import parse from 'date-fns/parse'
import addMinutes from 'date-fns/add_minutes'
import isAfter from 'date-fns/is_after'

import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop'
import 'react-big-calendar/lib/addons/dragAndDrop/styles.css'

import {
	VIEW_APPOINTMENT,
	NEW_APPOINTMENT,
	EDIT_BLOCKED_TIME_ROUTE,
	NEW_BLOCKED_TIME_ROUTE
} from '../../appointments/urls'
import { useMutation } from '@apollo/react-hooks'
import { startOfDay } from 'date-fns'

const DragAndDropCalendar = withDragAndDrop(BigCalendar)

const localizer = momentLocalizer(moment)

const calendarComponents = {
	toolbar: Toolbar,
	event: Event,
	header: Header
}

let DEFAULT_LOCATION = {
	appointments: [],
	blockedTime: []
}

let DEFAULT_RESOURCE = {}

const sourceColors = {
	default: {
		background: '#3F55C2'
	},
	onlinecheckin: {
		background: '#66CE89'
	},
	onlineappointment: {
		background: '#66CE89'
	},
	walkin: {
		background: '#CE66AB'
	}
}

const eventPropGetter = event => {
	return {
		style: {
			opacity: 1,
			background: 'rgba(100, 118, 214, 1.0)'
		}
	}
}

const startAccessor = event => new Date(event.startTime)
const endAccessor = event => new Date(event.endTime)
const resourceAccessor = event => event?.employee?.id
const availableViews = { day: TimelineDayView, week: TimelineWeekView }
const formats = { timeGutterFormat: 'ha' }

const Calendar = ({ loading, history, date, data, handleAppointmentChange }) => {
	console.log('[CALENDAR]')
	// these are needed to prevent errors and to ensure the calendar doesn't "blink" between renders
	let location = data.location || DEFAULT_LOCATION
	let activeResource = data.activeResource || DEFAULT_RESOURCE
	const [setCalendarDate] = useMutation(setCalendarDateMutation)
	const [setScrollTime] = useMutation(setCalendarScrollTime)

	const [{ resources }, setState] = React.useState({ resources: [] })

	// console.log(data)
	// For setting scrollTime -- FIXME: This is buggy in mobile and always fires.
	// React.useEffect(() => {
	// 	let timeout

	// 	if (loading) return

	// 	function listener(event) {
	// 		clearTimeout(timeout)

	// 		timeout = window.setTimeout(() => {
	// 			// Height of the scrollable window divided by 24 hours in a day divided by 60 minutes gives us the pixels per minute.
	// 			const ppm = event.target.scrollHeight / 24 / 60
	// 			const scrollTop = event.target.scrollTop

	// 			const minutes = scrollTop / ppm
	// 			const date = addMinutes(startOfDay(new Date()), minutes)

	// 			setScrollTime({ variables: { date } })
	// 		}, 250)
	// 	}

	// 	document.querySelector('.rbc-time-content').addEventListener('scroll', listener)

	// 	return () => {
	// 		// TODO: This was throwing an error because the element doesn't exist... do we need to remove the above event listener?
	// 		const timeline = document.querySelector('.rbc-time-content')
	// 		if (!timeline) return

	// 		return timeline.removeEventListener('scroll', listener)
	// 	}
	// }, [setScrollTime, loading])

	// Change the visible employees
	// FIXME: Find a way to pass a prop to the Toolbar so we can trigger this via on onResourceChange prop instead of reacting to the change.
	React.useEffect(() => {
		if (!location.id) return

		const resources =
			activeResource.id === 'all'
				? location.employees.filter(employee => employee.bookingEnabled === true)
				: [activeResource]

		setState(prevState => ({ ...prevState, resources }))
	}, [activeResource, location.id, location.employees])

	const onSelectSlot = ({ resourceId, start, end }) => {
		if (data.calendarInsertMode === 'appointment') {
			// within 5 minutes, don't open modal. This is needed to prevent clicks from opening the appointment modal.
			if (isAfter(addMinutes(start, 6), end)) {
				return
			}

			history.push(NEW_APPOINTMENT, { start, end, employeeId: resourceId })
			return
		}

		if (data.calendarInsertMode === 'blockedTime') {
			history.push(NEW_BLOCKED_TIME_ROUTE, {
				startTime: start,
				endTime: end,
				employeeId: resourceId
			})
		}
	}

	const onSelectEvent = appointment => {
		if (appointment.type !== 'blockedTime') {
			history.push(generatePath(VIEW_APPOINTMENT, { id: appointment.id }))

			return
		}

		history.push(generatePath(EDIT_BLOCKED_TIME_ROUTE, { id: appointment.id }))
	}

	// sets the calendar date which is then used to query the next appointments for the given date.
	const onNavigate = date => {
		setCalendarDate({
			variables: { date }
		})
	}

	return (
		<DragAndDropCalendar
			localizer={localizer}
			selectable={true}
			events={[...location.appointments, ...location.blockedTime]}
			startAccessor={startAccessor}
			endAccessor={endAccessor}
			// scrollToTime={parse(data.calendarScrollTime)}
			resources={resources}
			resourceAccessor={resourceAccessor}
			resourceIdAccessor="id"
			resourceTitleAccessor="firstName"
			view={data.view}
			views={availableViews}
			defaultView={'day'}
			date={parse(date)}
			timeslots={12}
			step={5}
			formats={formats}
			eventPropGetter={eventPropGetter}
			components={calendarComponents}
			onNavigate={onNavigate}
			onView={() => null}
			onSelectSlot={onSelectSlot}
			onSelectEvent={onSelectEvent}
			onEventResize={handleAppointmentChange}
			onEventDrop={handleAppointmentChange}
		/>
	)
}

export default Calendar
