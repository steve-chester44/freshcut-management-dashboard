import differenceInMinutes from 'date-fns/difference_in_minutes'
import React, { lazy } from 'react'
import styled from 'styled-components'
import { calendarInfoQuery, calendarRequirements } from '../queries'
import { Route, Switch } from 'react-router-dom'
import { useQuery, useMutation, useSubscription } from '@apollo/react-hooks'

import getCalendarTimes from '../utils/getCalendarTimes'

import { CALENDAR_ROOT } from '../utils/urls'

import Calendar from '../components/Calendar'
import Loading from '../../../components/routes/LoadingPage'

import { upsertAppointmentMutation } from '../../appointments/mutations'
import { appointmentsSubscription } from '../subscriptions'

import {
	NEW_APPOINTMENT,
	EDIT_APPOINTMENT_ROUTE,
	VIEW_APPOINTMENT,
	NEW_BLOCKED_TIME_ROUTE,
	EDIT_BLOCKED_TIME_ROUTE
} from '../../appointments/urls'

import { Layout } from 'antd'

import { setActiveLocationMutation } from '../../../modules/calendar/mutations'
import onSubscriptionData from '../utils/onSubscriptionData'

const BlockedTimeModal = lazy(() => import('../../blockedTime/views/ModalContainer'))

const AppointmentModal = lazy(() => import('../../appointments/views/AppointmentModal'))
const AppointmentSummary = lazy(() => import('../../appointments/views/AppointmentSummary'))

const Wrapper = styled('div')`
	position: relative;
	overflow: hidden;
	height: 100vh;
`

const CalendarRoot = ({ locations, history }) => {
	console.log('[CALENDAR ROOT]')

	const [upsertAppointment] = useMutation(upsertAppointmentMutation)

	const {
		data: { calendarDate, activeLocation }
	} = useQuery(calendarRequirements)

	// Prevent the need to wait on the useEffect to set the location before we can use it
	const locationId = activeLocation ? activeLocation.id : locations[0].id
	const [setLocation] = useMutation(setActiveLocationMutation)

	const queryVariables = React.useMemo(() => {
		return {
			locationId,
			...getCalendarTimes(calendarDate)
		}
	}, [calendarDate, locationId])

	useSubscription(appointmentsSubscription, {
		variables: { locationId },
		onSubscriptionData: ({ client, subscriptionData }) => {
			onSubscriptionData({ client, subscriptionData, queryVariables, query: calendarInfoQuery })
		}
	})

	const { data, loading } = useQuery(calendarInfoQuery, { variables: queryVariables })

	React.useEffect(() => {
		if (!locations || activeLocation) return

		setLocation({ variables: { location: locations[0] } })
	}, [activeLocation, setLocation, locations])

	if (!calendarDate || !data) return <Loading />

	const onAppointmentAdjustment = async ({ start, end, event, resourceId }) => {
		const { id, locationId } = event

		const duration = differenceInMinutes(end, start)

		const input = {
			id,
			locationId,
			customerId: event.customer?.id,
			userId: resourceId,
			startTime: start,
			endTime: end,
			duration
		}


		await upsertAppointment({
			variables: { input },
			optimisticResponse: {
				__typename: 'Mutation',
				upsertAppointment: {
					__typename: 'Appointment',
					status: 'confirmed',
					...event,
					startTime: start,
					endTime: end,
					duration
				}
			}
		})
	}

	return (
		<Layout.Content style={{ flex: 1 }}>
			<React.Suspense fallback={null}>
				<Switch>
					<Route
						path={[NEW_BLOCKED_TIME_ROUTE, EDIT_BLOCKED_TIME_ROUTE]}
						render={props => {
							return (
								<BlockedTimeModal
									blockedTimeId={props.match.params.id}
									locationId={locationId}
									{...props}
									onCancel={() => history.push(CALENDAR_ROOT)}
								/>
							)
						}}
					/>
					<Route
						exact
						path={[NEW_APPOINTMENT, EDIT_APPOINTMENT_ROUTE]}
						render={props => {
							return (
								<AppointmentModal
									defaultEmployeeId={props.location?.state?.employeeId}
									defaultStartTime={props.location?.state?.start}
									defaultEndTime={props.location?.state?.end}
									locationId={locationId}
									appointmentId={props.match.params.id}
									onClose={() => history.push(CALENDAR_ROOT)}
								/>
							)
						}}
					/>

					<Route
						path={VIEW_APPOINTMENT}
						exact
						render={props => {
							console.log('[APPOINTMENT SUMMARY ROUTE]')
							return (
								<AppointmentSummary
									locationId={locationId}
									onClose={() => history.push(CALENDAR_ROOT)}
									appointmentId={props.match.params.id}
									{...props}
								/>
							)
						}}
					/>
				</Switch>
			</React.Suspense>

			<Wrapper>
				<Calendar
					handleAppointmentChange={onAppointmentAdjustment}
					loading={loading}
					date={calendarDate}
					history={history}
					data={data}
				/>
			</Wrapper>
		</Layout.Content>
	)
}

export default CalendarRoot
