import subHours from 'date-fns/sub_hours'
import { calendarResourcesQuery, calendarActiveLocationQuery } from './queries'

const createCalendarResolver = ({ cache }) => {
	// defaults

	cache.writeData({
		data: {
			calendarDate: new Date().toString(),
			calendarInsertMode: 'appointment',
			// use subHours so the line is visible.  If we set scrolltime to current time then the current time indicator isnt visible.
			// this isn't perfect. 2 am and we sub 3 hours, it'll be equal to 11pm which is at the opposite end of the calendar
			calendarScrollTime: subHours(new Date(), 3).toString(),
			activeLocation: null,
			activeAppointment: null,
			activeResource: {
				id: 'all',
				firstName: 'All Employees',
				lastName: null,
				bookingEnabled: true,
				__typename: 'User'
			},
			timelineInterval: 900,
			view: 'day'
		}
	})

	return {
		Mutation: {
			setScrollTime: (root, { date }, ctx) => {
				console.log('setting calendar scroll time', date)
				if (!date) return null
				ctx.cache.writeData({ data: { calendarScrollTime: date.toString() } })
				return null
			},
			setCalendarInsertMode: (root, { mode = 'appointment' }, ctx) => {
				console.log('setting calendar insert mode', mode)
				ctx.cache.writeData({ data: { calendarInsertMode: mode } })
				return null
			},
			setCalendarResource: (root, { resource }, ctx) => {
				const showAll = resource.id === 'all'

				const data = { data: { activeResource: resource } }

				if (showAll) {
					data.data.view = 'day'
				}

				ctx.cache.writeData(data)
				return null
			},
			setLocation: (root, { location }, ctx) => {
				ctx.cache.writeData({ data: { activeLocation: location } })
				return null
			},
			setCalendarDate: (root, { date }, ctx) => {
				console.log(root);
				ctx.cache.writeData({ data: { calendarDate: date.toString() } })
				return null
			},
			setCalendarView: (root, { view }, ctx) => {
				const { activeLocation } = ctx.cache.readQuery({ query: calendarActiveLocationQuery })

				const { activeResource, employees } = ctx.cache.readQuery({
					query: calendarResourcesQuery,
					variables: {
						locationId: activeLocation.id
					}
				})

				const data = { data: { view } }

				// Don't show all employees in 'week' view
				if (activeResource.id === 'all' && view === 'week') {
					data.data.activeResource = employees[0]
				}

				ctx.cache.writeData(data)
				return null
			}
		}
	}
}

export default createCalendarResolver
