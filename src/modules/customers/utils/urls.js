export const CUSTOMERS_ROOT = "/customers"
export const CREATE_CUSTOMER_ROUTE = "/customer/new"
export const EDIT_CUSTOMER_ROUTE = "/customers/:id"
