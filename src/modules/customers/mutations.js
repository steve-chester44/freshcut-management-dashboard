import gql from 'graphql-tag'
import { fragments } from './queries'

export const createCustomerMutation = gql`
	mutation CreateCustomer($input: CreateCustomerInput!) {
		createCustomer(input: $input) {
			...customer
		}
	}

	${fragments.customer}
`

export const updateCustomerMutation = gql`
	mutation UpdateCustomer($input: UpdateCustomerInput!) {
		updateCustomer(input: $input) {
			...customer
		}
	}

	${fragments.customer}
`
