import React from 'react'

import { Form, Input, Row, Col, Select } from 'antd'

const CustomerForm = ({ fields, onInputChange, onNotificationChange }) => {
	return (
		<div>
			<Row type="flex" gutter={16}>
				<Col xs={24} md={12}>
					<Form.Item label="First Name" colon={false}>
						<Input size="large" type="text" name="firstName" value={fields.firstName} onChange={onInputChange} />
					</Form.Item>
				</Col>
				<Col xs={24} md={12}>
					<Form.Item label="Last Name" colon={false}>
						<Input size="large" type="text" name="lastName" value={fields.lastName} onChange={onInputChange} />
					</Form.Item>
				</Col>
				<Col xs={24} md={12}>
					<Form.Item label="Phone" colon={false}>
						<Input size="large" type="text" name="phoneNumber" value={fields.phoneNumber} onChange={onInputChange} />
					</Form.Item>
				</Col>
				<Col xs={24} md={12}>
					<Form.Item label="Email" colon={false}>
						<Input size="large" type="email" name="email" value={fields.email} onChange={onInputChange} />
					</Form.Item>
				</Col>
			</Row>

			<Row>
				<Col span={24}>
					<Form.Item label="Appointment Notifications">
						<Select
							size="large"
							style={{ width: '100%' }}
							defaultValue="sms"
							value={fields.appointmentNotifications}
							name="appointmentNotifications"
							onChange={onNotificationChange}
						>
							<Select.Option value="none">Don't send notifications</Select.Option>
							<Select.Option value="email">Email</Select.Option>
							<Select.Option value="sms">SMS</Select.Option>
						</Select>
					</Form.Item>
				</Col>
			</Row>

			<Row>
				<Col span={24}>
					<Form.Item label="Customer Notes" colon={false}>
						<Input.TextArea
							value={fields.notes}
							autosize={{ minRows: 3, maxRows: 3 }}
							name="notes"
							onChange={onInputChange}
						/>
					</Form.Item>
				</Col>
			</Row>
		</div>
	)
}

export default CustomerForm
