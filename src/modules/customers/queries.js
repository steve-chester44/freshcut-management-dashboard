import gql from 'graphql-tag'

export const fragments = {
	customer: gql`
		fragment customer on Customer {
			id
			firstName
			lastName
			phoneNumber
			email
			notes
			appointmentNotifications
			createdAt
		}
	`
}

export const customerQuery = gql`
	query customer($id: Int!) {
		customer(id: $id) {
			...customer
		}
	}

	${fragments.customer}
`

export const customersQuery = gql`
	query customers($input: CustomerListInput!) {
		customers(input: $input) {
			...customer
		}
	}

	${fragments.customer}
`

export const customerSearchQuery = gql`
	query SearchCustomers($input: CustomerSearchInput!) {
		searchCustomers(input: $input) {
			...customer
		}
	}

	${fragments.customer}
`
