import React from 'react'
import { Link, generatePath } from 'react-router-dom'

import { Table, Button, Input } from 'antd'
import PageWithActionBar from '../../../components/PageWithActionBar'

import { customersQuery } from '../queries'
import { customerSearchQuery } from '../queries'
import { EDIT_CUSTOMER_ROUTE } from '../utils/urls'
import { useQuery, useLazyQuery } from '@apollo/react-hooks'

const columns = [
	{
		title: 'Name',
		dataIndex: 'name',
		key: 'name',
		render: (text, { firstName = '', lastName = '' }) => {
			return (
				<div>
					<span>{firstName} </span>
					<span>{lastName}</span>
				</div>
			)
		}
	},
	{
		title: 'Phone Number',
		dataIndex: 'phoneNumber',
		key: 'phoneNumber'
	},
	{
		align: 'right',
		render: customer => {
			return (
				<Link to={generatePath(EDIT_CUSTOMER_ROUTE, { id: customer.id })}>
					<Button>View</Button>
				</Link>
			)
		}
	}
]

// TODO: Implement ability to paginate list
const listState = { input: { first: 500, offset: 0 } }

const paginationConfig = {
	hideOnSinglePage: true,
	showSizeChanger: true
}

const ListContainer = () => {
	const [search, { loading, data }] = useLazyQuery(customerSearchQuery)

	const {
		data: { customers }
	} = useQuery(customersQuery, { variables: listState })

	let searchDebouce

	const handleSearch = ({ target: { value } }) => {
		window.clearTimeout(searchDebouce)

		searchDebouce = window.setTimeout(async () => {
			search({ variables: { input: { term: value.trim() } } })
		}, 250)
	}

	return (
		<PageWithActionBar
			className="page-load-fade"
			padding="20px 30px"
			leftContent={<h1>Clients & Customers</h1>}
			rightContent={
				<Input.Search
					placeholder="Search by name or phone number"
					onChange={handleSearch}
					size="large"
					style={{ width: 350 }}
				/>
			}
		>
			<Table
				size="small"
				pagination={paginationConfig}
				loading={loading || !customers}
				rowKey={record => record.id}
				dataSource={data && data.searchCustomers ? data.searchCustomers : customers}
				columns={columns}
			/>
		</PageWithActionBar>
	)
}

export default ListContainer
