import React from 'react'
import styled from 'styled-components'

import { Row, Col, Icon, Button, message } from 'antd'
import CustomerForm from '../components/CustomerForm'

import { CUSTOMERS_ROOT } from '../utils/urls'
import { createCustomerMutation, updateCustomerMutation } from '../mutations'
import { customerSearchQuery, customerQuery } from '../queries'

import { useQuery, useMutation } from '@apollo/react-hooks'

const Wrapper = styled('div')`
	position: fixed;
	background: white;
	top: 0;
	left: 0;
	width: 100vw;
	height: 100vh;
	overflow: auto;
	z-index: 99;
	display: flex;
	flex-direction: column;

	.header {
		padding: 10px;
		display: flex;
		justify-content: space-between;
		align-items: center;
		border-bottom: 1px solid rgba(32, 32, 32, 0.1);

		h1 {
			padding: 0;
			margin: 0;
		}

		.close-btn {
			cursor: pointer;
			font-size: 40px;
		}
	}

	.customer-form {
		padding: 5vh 5vw;
		height: 100%;
	}
`

const defaultFields = {
	firstName: '',
	lastName: '',
	email: '',
	phoneNumber: '',
	gender: 'unknown',
	notes: '',
	appointmentNotifications: 'sms'
}

const CustomerModal = ({ customerId, history, children, onCreate }) => {
	const [createCustomer] = useMutation(createCustomerMutation)
	const [updateCustomer] = useMutation(updateCustomerMutation)

	const [state, setState] = React.useState({
		isOpen: !children,
		isSubmitting: false,
		isNewCustomer: true,
		fields: defaultFields
	})

	useQuery(customerQuery, {
		skip: !customerId || customerId === 'new',
		variables: { id: customerId },
		onCompleted: ({ customer }) => {
			if (customer) {
				setState(prev => ({ ...prev, isNewCustomer: false, fields: customer }))
			}
		}
	})

	const handleInputChange = ({ target: { name, value } }) => {
		setState({
			...state,
			fields: {
				...state.fields,
				[name]: value
			}
		})
	}

	const handleOpenClick = (options, event) => {
		setState({ ...state, isOpen: true, fields: options && options.customer ? options.customer : defaultFields })
	}

	const handleCancelClick = event => {
		if (children) {
			setState({
				...state,
				fields: defaultFields,
				isOpen: false
			})
		} else {
			history.goBack()
		}
	}

	const handleNotificationChange = appointmentNotifications => {
		setState({
			...state,
			fields: {
				...state.fields,
				appointmentNotifications
			}
		})
	}

	const handleCreate = async input => {
		console.log(input)
		const { data } = await createCustomer({
			variables: { input },
			update: (cache, { data: { createCustomer: customer } }) => {
				// TODO: Could we use read/writeFragment here
				// We're attempting to add the customer to a customerSearchQuery that doesn't have a search term... i'm not sure this is the best way to go about that.
				const { searchCustomers } = cache.readQuery({
					query: customerSearchQuery,
					variables: { input: { term: '' } }
				})

				cache.writeQuery({
					query: customerSearchQuery,
					variables: { input: { term: '' } },
					data: { searchCustomers: searchCustomers.concat([customer]) }
				})
			}
		})

		// TODO: This assumes we're always headed back to the Customer list but its possible we want to go somewhere else.. could we re-purpose the onCreate function to handle this logic?.
		if (!children) {
			history.push(CUSTOMERS_ROOT)
			return
		}

		if (onCreate) {
			onCreate(data.createCustomer)
		}

		setState({
			...state,
			fields: defaultFields,
			isOpen: false
		})

		message.success('Customer created')
	}

	const handleUpdate = async input => {
		let { __typename, createdAt, ...cleanInput } = input

		await updateCustomer({
			variables: { input: cleanInput }
		})

		message.success('Customer updated')
	}

	const handleSubmit = () => {
		if (state.isNewCustomer) {
			handleCreate(state.fields)
		} else {
			handleUpdate(state.fields)
		}
	}

	return (
		<React.Fragment>
			{state.isOpen && (
				<Wrapper>
					<Row>
						<Col className="header">
							<span />
							<h1>{state.isNewCustomer ? 'New' : 'Update'} Customer</h1>
							<Icon className="close-btn" type="close" onClick={handleCancelClick} />
						</Col>
					</Row>
					<Row type="flex" justify="center">
						<Col className="customer-form" xs={24} xl={12}>
							<CustomerForm
								onInputChange={handleInputChange}
								onNotificationChange={handleNotificationChange}
								fields={state.fields}
								onSubmit={x => console.log(x)}
							/>

							<Row type="flex" justify="end">
								<Button size="large" onClick={handleCancelClick}>
									Cancel
								</Button>

								<Button
									style={{ marginLeft: 20 }}
									type="primary"
									disabled={state.fields.firstName.length === 0}
									size="large"
									icon="save"
									onClick={handleSubmit}
									loading={state.isSubmitting}
								>
									{state.isNewCustomer ? 'Save' : 'Update'}
								</Button>
							</Row>
						</Col>
					</Row>
				</Wrapper>
			)}
			{children && children({ open: handleOpenClick })}
		</React.Fragment>
	)
}

export default CustomerModal
