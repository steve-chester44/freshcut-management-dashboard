import { message } from 'antd'

const debug = require('debug')('graphql')

export function getCookieValue(a) {
	const b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)')
	return b ? b.pop() : undefined
}

export function displayErrors(error) {
	if (!error.graphQLErrors) {
		return debug('error thrown but was not a gqlerror', error)
	}

	error.graphQLErrors.forEach(({ message: errMsg }) => {
		message.error(errMsg)
	})
}
