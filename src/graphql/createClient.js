import { ApolloClient } from 'apollo-client'
import { InMemoryCache, defaultDataIdFromObject } from 'apollo-cache-inmemory'
import { onError } from 'apollo-link-error'
import { ApolloLink } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'

import { HttpLink } from 'apollo-link-http'
import { split } from 'apollo-link'
import { getMainDefinition } from 'apollo-utilities'

import createCalendarResolver from '../modules/calendar/resolvers'
import createBlockedTimeResolver from '../modules/blockedTime/resolvers'
import { getCookieValue } from '../graphql/utils'

import { logError } from '../common/utils/logging'
import { message } from 'antd'

const cache = new InMemoryCache({
	dataIdFromObject: object => {
		switch (object.__typename) {
			case 'Source':
				// Needed to make sources unique.
				return `Source:${object.id}-${object.type}`
			default:
				return defaultDataIdFromObject(object) // fall back to default handling
		}
	}
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors && graphQLErrors.length > 0) {
		graphQLErrors.forEach(error => {
			logError(error)
			message.error(error.message)
		})
	}

	if (networkError && networkError.result && networkError.result.errors) {
		networkError.result.errors.forEach(logError)
	}
})

const httpLink = ApolloLink.from([
	errorLink,
	new HttpLink({
		credentials: 'include',
		uri: process.env.REACT_APP_API_ENDPOINT
	})
])

export const wsLink = new WebSocketLink({
	uri: process.env.REACT_APP_WSS_ENDPOINT,
	credentials: 'include',
	options: {
		reconnect: true,
		lazy: true,
		connectionParams: {
			token: getCookieValue('access-token')
		}
	}
})

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
	// split based on operation type
	({ query }) => {
		const { kind, operation } = getMainDefinition(query)
		return kind === 'OperationDefinition' && operation === 'subscription'
	},
	wsLink,
	httpLink
)

const client = new ApolloClient({
	link,
	cache,
	resolvers: {
		...createBlockedTimeResolver({ cache }),
		...createCalendarResolver({ cache })
	}
})

export default client
