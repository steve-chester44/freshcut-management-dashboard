const { override, fixBabelImports, addLessLoader, addBabelPlugins } = require('customize-cra')

module.exports = override(
	fixBabelImports('antd', { libraryDirectory: 'es', style: true }),
	addBabelPlugins('@babel/plugin-proposal-optional-chaining'),
	addLessLoader({
		javascriptEnabled: true,
		modifyVars: {
			'@primary-color': 'rgba(97, 116, 221, 1.0)',
			'@layout-body-background': 'rgba(249, 249, 249, 1.0)',
			'@layout-header-background': 'rgba(39, 39, 39, 1.0)',
			'@menu-dark-submenu-bg': 'rgba(49, 49, 49, 1.0)',
			'@btn-default-color': 'rgba(52, 68, 150, 1.0)',
			'@btn-default-bg': 'rgba(255, 255, 255, 1.0)',
			'@btn-default-border': 'rgba(189, 195, 225, 1.0)',
			'@text-color': 'rgba(25, 25, 25, 1.0)',
			'@text-color-secondary': 'rgba(140, 140, 140, 1.0)',
			'@text-color-secondary-dark': 'rgba(172, 180, 216, 1.0)',
			'@btn-font-weight': '600',
			'@btn-font-size-sm': '12px',
			'@btn-shadow': 'none',
			'@label-color': '@text-color-secondary',
			'@font-family': "system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif"
		}
	})
)
